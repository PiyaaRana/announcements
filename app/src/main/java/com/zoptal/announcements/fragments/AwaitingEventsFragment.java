package com.zoptal.announcements.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.adapter.ContactsAdapter;
import com.zoptal.announcements.adapter.EventAdapter;
import com.zoptal.announcements.base.DataHelperClass;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.LoginSignUpActivity;
import com.zoptal.announcements.modal.ContactModel;
import com.zoptal.announcements.modal.ContactNameModel;
import com.zoptal.announcements.modal.ContactNameResponseModal;
import com.zoptal.announcements.modal.EventContactsModal;
import com.zoptal.announcements.modal.EventModal;
import com.zoptal.announcements.responseClasses.CalendarEventResponse;
import com.zoptal.announcements.responseClasses.ContactsResponse;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.DateConvertor;
import com.zoptal.announcements.utils.NestedRecyclerLinearLayoutManager;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.Utils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AwaitingEventsFragment extends Fragment {

    View view;
    private RecyclerView recyclerView_awaitingEvents;
    private LinearLayoutManager layoutManager;
    private TextView txtview_empty,txtview_save;
    private EventAdapter eventAdapter;
    private List<EventModal>eventModalList;
    private ArrayList<ContactNameResponseModal>eventContactsModalList;
    private String eventId="";
    private RelativeLayout mainLayout;
    private ProgressBar progressBar;
    private String appToken = "f6fa0fa5041479b76322145vgh1g410975af7766",contact_no_values="",eventdateRecieved,timeZone,inviteId;
    private Boolean isFragmentVisible=false;
    private MyActivity myActivity;
    private ArrayList<CalendarEventResponse.CalendarEventDataModal> awaitingListingResponseArrayList = new ArrayList<>();
    private ArrayList<ContactsResponse.ContactsDataModal> contactListingResponseArrayList = new ArrayList<>();
    private Calendar cDate;
    EventAdapter.ContactsAdapter eventListChildAdapter;
    AlphaAnimation buttonClick;
    private String shareEventName,shareEventDesc,shareEventLocation,shareEventStartTime, shareEventDate,markCheck="1",shareeventstarttime;
    private BasicActivity basicActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.awaiting_events_fragment,container,false);
        isFragmentVisible=true;
        eventModalList= new ArrayList<>();
        eventContactsModalList= new ArrayList<>();
        buttonClick = new AlphaAnimation(1F, 0.5F);
        findViews();
        basicActivity=(BasicActivity)getActivity();
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        return view;
    }
        private void findViews(){
        txtview_empty=(TextView)view.findViewById(R.id.empty_data);
        txtview_save= (TextView) view.findViewById(R.id.tv_save_event);

        mainLayout = (RelativeLayout) view.findViewById(R.id.content_frame);
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        recyclerView_awaitingEvents=(RecyclerView)view.findViewById(R.id.recycler_awaiting_events);
        recyclerView_awaitingEvents.setHasFixedSize(true);
        layoutManager= new LinearLayoutManager(getActivity());
        recyclerView_awaitingEvents.setLayoutManager(layoutManager);
        recyclerView_awaitingEvents.setItemAnimator(new DefaultItemAnimator());
          cDate= Calendar.getInstance();
        TimeZone tz = cDate.getTimeZone();
        timeZone= String.valueOf(tz.getDisplayName(false, TimeZone.SHORT));
        getPhoneContacts();
        txtview_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                basicActivity.replaceFragment(new InboxFragment());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible=false;
    }

    private void getPhoneContacts() {
        ArrayList<String> contacts = new ArrayList<>();
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.e("phonenumber", phoneNumber);
            if (phoneNumber.length() >= 10) {
                //String  result = phoneNumber.replaceAll("[^\\w\\s\\p{Ps}\\p{Pe}]","");
                String result = getOnlyDigits(phoneNumber);
                contacts.add(result);
                Log.e("phonenumberfinal", result);
                HashSet hs = new HashSet();
                hs.addAll(contacts); // demoArrayList= name of arrayList from which u want to remove duplicates
                contacts.clear();
                contacts.addAll(hs);
                for (int i = 0; i < contacts.size(); i++) {
                    if (i == 0) {
                        contact_no_values = contacts.get(i);
                        Log.e("selectedparentsss", contact_no_values);
                    } else {
                        contact_no_values = contact_no_values + "," + contacts.get(i);
                        Log.e("selectedparent", contact_no_values);
                    }
                }

            }
        }
        phones.close();
        getAwaitingEventsAPI();
    }
    public static String getOnlyDigits(String s) {
        String number = "";
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        number = matcher.replaceAll("");
        if (number.length() > 10) {
            number = number.substring(number.length() - 10);
        }
        return number;
    }
    private void getAwaitingEventsAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();
        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<CalendarEventResponse> userCall = apiInterface.received_event_invitations(appToken, access_token,"awaiting",contact_no_values,"");
        userCall.enqueue(new Callback<CalendarEventResponse>() {
            @Override
            public void onResponse(Call<CalendarEventResponse> call, Response<CalendarEventResponse> response) {
                DataHelperClass.getInstance().setCalendarEventDataModal(response.body().getCalendarArrayList());
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        Log.e("response",""+response.body());
                        awaitingListingResponseArrayList.clear();
                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
                            itemsListingResponseArrayList.add(itemsListingModal);
                        }*/
                        awaitingListingResponseArrayList = response.body().getCalendarArrayList();
                        if (awaitingListingResponseArrayList.size() > 0) {
                            eventAdapter = new EventAdapter(getActivity());
                            recyclerView_awaitingEvents.setAdapter(eventAdapter);

                        } else {
                            recyclerView_awaitingEvents.setVisibility(View.GONE);
                            txtview_empty.setVisibility(View.VISIBLE);
                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    } else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
                        recyclerView_awaitingEvents.setVisibility(View.GONE);
                        txtview_empty.setVisibility(View.VISIBLE);
                    } else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<CalendarEventResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }

    private void getContactsAPI() {
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<ContactsResponse> userCall = apiInterface.my_contacts(appToken, access_token, contact_no_values,eventId);
        userCall.enqueue(new Callback<ContactsResponse>() {
            @Override
            public void onResponse(Call<ContactsResponse> call, Response<ContactsResponse> response) {
                if (isFragmentVisible) {
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        Log.e("response",""+response.body());
                        contactListingResponseArrayList.clear();
//                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
//                            itemsListingResponseArrayList.add(itemsListingModal);
//                        }*/
                        contactListingResponseArrayList = response.body().getArrayList();
                        eventListChildAdapter.notifyDataSetChanged();
                        eventAdapter.notifyDataSetChanged();
                        //recyclerView_awaitingEvents.setAdapter(eventAdapter);
                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }
//                    else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
//                        recyclerView.setVisibility(View.GONE);
//                        empty_data.setVisibility(View.VISIBLE);
//                    }
                    else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactsResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }
    public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {
        public Button contacts;
        private Context context;
        String  event_loc,event_date;
        ProgressDialog pd;
        Dialog eventDialog;
        private int i;

        public EventAdapter(Context context){
            super();
            this.context = context;
        }

        @Override
        public EventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.awaiting_event_list, parent, false);
           EventAdapter.ViewHolder viewHolder = new EventAdapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final EventAdapter.ViewHolder holder, final int position) {

            holder.tv_eventName.setText(awaitingListingResponseArrayList.get(position).getEventName());
            String eventType= awaitingListingResponseArrayList.get(position).getEventType();
            if(eventType.equalsIgnoreCase("Public")){
                holder.tv_eventType.setText("Public Event");
            }
            else {
                holder.tv_eventType.setText("Private Event");
            }
            holder.tv_eventLoc.setText(awaitingListingResponseArrayList.get(position).getEventLocation());
            holder.tv_eventDesc.setText(awaitingListingResponseArrayList.get(position).getDescription());
            holder.tv_eventSize.setText(awaitingListingResponseArrayList.get(position).getPartySize());
            eventdateRecieved=awaitingListingResponseArrayList.get(position).getEventDate();
            shareEventStartTime=awaitingListingResponseArrayList.get(position).getStartTime();
            DateConvertor obj = new DateConvertor();
            final String eventstarttime = obj.changeTimeFormatRecieve(shareEventStartTime);
            Log.e("datevalue", eventstarttime);

            holder.tv_eventTime.setText(eventstarttime+" "+timeZone);
            eventId=awaitingListingResponseArrayList.get(position).getId();
            if(awaitingListingResponseArrayList.get(position).getContactDetails().size()>0) {
                for (int k = 0; k < awaitingListingResponseArrayList.get(position).getContactDetails().size(); k++) {
                    Log.e("size",""+k+" "+awaitingListingResponseArrayList.get(position).getContactDetails().size());
                    ContactNameResponseModal modal = new ContactNameResponseModal();
                    modal.setId(awaitingListingResponseArrayList.get(position).getContactDetails().get(k).getContact_id());
                    modal.setPicture(awaitingListingResponseArrayList.get(position).getContactDetails().get(k).getReceiverPic());
                    eventContactsModalList.add(modal);
                }

                eventListChildAdapter = new EventAdapter.ContactsAdapter(context, eventContactsModalList);
                holder.recyclerView_EventContacts.setAdapter(eventListChildAdapter);
                i = eventContactsModalList.size();
                if (i > 0 && i <= 4) {
                    holder.tv_contactNumberText.setVisibility(View.GONE);
                    if (i == 1) {
                        holder.tv_dummyText.setText("" + i + " " + "is going");
                    } else {
                        holder.tv_dummyText.setText("" + i + " " + "are going");
                    }

                    holder.tv_dummyText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                }
                if (i > 4) {
                    holder.tv_contactNumberText.setText(String.valueOf(i - 3));
                }

            }
            else {
                holder.contactCountLayout.setVisibility(View.GONE);
            }
                //getContactsAPI();

            holder.btn_addCalendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.startAnimation(buttonClick);
                    addCalendarAPI();
                    view.setBackgroundResource(R.drawable.button_back_disable);
                    view.setEnabled(false);

                }
            });

            holder.btn_shareEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shareEventName=awaitingListingResponseArrayList.get(position).getEventName();
                    shareEventLocation=awaitingListingResponseArrayList.get(position).getEventLocation();
                    shareEventDesc=awaitingListingResponseArrayList.get(position).getDescription();
                    shareEventStartTime=awaitingListingResponseArrayList.get(position).getStartTime();
                    eventdateRecieved=awaitingListingResponseArrayList.get(position).getEventDate();
                    DateConvertor obj = new DateConvertor();
                    shareeventstarttime = obj.changeTimeFormatRecieve(shareEventStartTime);
                    Log.e("datevalue", shareeventstarttime);
                    DateConvertor obj1 = new DateConvertor();
                    final String eventdate = obj1.changeDateFormatRecieved(eventdateRecieved);
                    Log.e("datevalues", eventdate);
                    shareEventDate=eventdate;
                    shareData();
                }
            });
            holder.ll_MarkGoing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    inviteId=awaitingListingResponseArrayList.get(position).getInviteId();
                    markEventsAPI();
                    holder.img_tick.setImageResource(R.mipmap.tick);
                    view.setEnabled(false);
                    removeAt(position);
//                    Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            removeAt(position);// Execute a comethode in the intervel 3sec
//                        }
//                    },1000);
//
                }
            });
        }

        @Override
        public int getItemCount() {
            return awaitingListingResponseArrayList.size();
        }
        public void removeAt(int position){
            awaitingListingResponseArrayList.remove(position);
            notifyDataSetChanged();
        }
        class ViewHolder extends RecyclerView.ViewHolder{
            public TextView tv_eventName,tv_eventType,tv_eventLoc;
            public TextView tv_eventDesc,tv_eventSize,tv_eventTime,tv_contactNumberText,tv_dummyText;
            public RecyclerView recyclerView_EventContacts;
            public RelativeLayout contactCountLayout;
            public Button btn_addCalendar,btn_shareEvent;
            public RelativeLayout ll_MarkGoing;
            public ImageView img_tick;
            public ViewHolder(View itemView) {
                super(itemView);
                tv_eventName = (TextView)itemView.findViewById(R.id.list_event_name);
                tv_eventType = (TextView)itemView.findViewById(R.id.list_event_type);
                tv_eventLoc = (TextView)itemView.findViewById(R.id.list_event_loc);
                tv_eventSize = (TextView)itemView.findViewById(R.id.size_value);
                tv_eventTime = (TextView)itemView.findViewById(R.id.time_value);
                tv_eventDesc = (TextView)itemView.findViewById(R.id.list_event_desc);
                ll_MarkGoing = (RelativeLayout) itemView.findViewById(R.id.tick_layout);
                tv_contactNumberText = (TextView)itemView.findViewById(R.id.contact_numbr_txt);
                 btn_addCalendar= (Button)itemView.findViewById(R.id.btn_cal_evnt);
                 btn_shareEvent = (Button)itemView.findViewById(R.id.btn_tell_frnd);
                 img_tick=(ImageView)itemView.findViewById(R.id.list_event_tick);
                recyclerView_EventContacts=(RecyclerView)itemView.findViewById(R.id.recycler_contact_event);
                contactCountLayout=(RelativeLayout)itemView.findViewById(R.id.contact_count_layout);
                tv_dummyText= (TextView)itemView.findViewById(R.id.contact_dummy_txt);
                    LinearLayoutManager hs_linearLayout = new NestedRecyclerLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false){
                @Override
                public boolean canScrollHorizontally() {
                    return false;
                }
            };
                recyclerView_EventContacts.setLayoutManager(hs_linearLayout);
                recyclerView_EventContacts.setHasFixedSize(true);
                recyclerView_EventContacts.setItemAnimator(new DefaultItemAnimator());
//                for(int i=0;i<awaitingListingResponseArrayList.size();i++){
//                    for (int j=0;j<awaitingListingResponseArrayList.get(i).getContactDetails().size();j++){
//                        ContactNameResponseModal modal=new ContactNameResponseModal();
//                        modal.setId(awaitingListingResponseArrayList.get(i).getContactDetails().get(j).getContact_id());
//                        modal.setPicture(awaitingListingResponseArrayList.get(i).getContactDetails().get(j).getReceiverPic());
//                       eventContactsModalList.add(modal);
//
//                    }
//                }
            }
        }

        //    public void changeText(ViewHolder holder,int position) {
//
//        if (checkcaltext.equals("add")) {
//
//            holder.addCalendar.setText("Add to Calendar");
//
//
//        } else {
//            holder.addCalendar.setText("Remove from Calendar");
//        }
//    }
        public class ContactsAdapter extends RecyclerView.Adapter<EventAdapter.ContactsAdapter.MyViewHolder> {
             private Context context;
             private String imageUrl;
             private ArrayList<ContactNameResponseModal>recyclerList;

            public class MyViewHolder extends RecyclerView.ViewHolder{
                public ImageView image;
                public TextView tv_imageName;
                public String imageName;
                public MyViewHolder(View view){
                    super(view);
                    image = (ImageView) view.findViewById(R.id.imageViewName);
                    tv_imageName=(TextView)view.findViewById(R.id.image_name);
                    tv_imageName.setVisibility(View.GONE);
                }
            }

            public ContactsAdapter(Context context,ArrayList<ContactNameResponseModal>recyclerList){
                 this.context= context;
                 this.recyclerList=recyclerList;
            }

            @Override
            public EventAdapter.ContactsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_contact_list,parent,false);
                return new EventAdapter.ContactsAdapter.MyViewHolder(itemView);
            }

            @Override
            public void onBindViewHolder(EventAdapter.ContactsAdapter.MyViewHolder holder, int position) {
                ContactNameResponseModal contactNameResponseModal= recyclerList.get(position);
                if (recyclerList.size() >= 4) {
                    for (int k = 0; k < 4; k++) {
                        imageUrl = contactNameResponseModal.getPicture();
                        if (!(imageUrl.isEmpty())) {
                            Picasso.with(context).load(imageUrl).resize(215, 215).into(holder.image);
                        } else {
                            Picasso.with(context).load(R.drawable.no_profile_grey_circle_bg).resize(215, 215).into(holder.image);
                        }
                    }

                }
                else {
                    imageUrl = contactNameResponseModal.getPicture();
                    if (!(imageUrl.isEmpty())) {
                        Picasso.with(context).load(imageUrl).resize(215, 215).into(holder.image);
                    } else {
                        Picasso.with(context).load(R.drawable.no_profile_grey_circle_bg).resize(215, 215).into(holder.image);
                    }

                }
            }

            public void removeAt(int position){
                recyclerList.remove(position);
                notifyItemRemoved(position);
            }
            @Override
            public int getItemCount() {
                return recyclerList.size();
            }
        }

    }

    private void addCalendarAPI() {

        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<LoginResponse> userCall = apiInterface.add_to_calendar(appToken, access_token, eventId);
        userCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (isFragmentVisible) {
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());

                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }
//                    else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
//                        recyclerView.setVisibility(View.GONE);
//                        empty_data.setVisibility(View.VISIBLE);
//                    }
                    else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (isFragmentVisible) {

                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }
    private void shareData() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String VolEvent = shareEventName + " , " + shareEventDate + " , " + shareEventLocation
                + " , " + shareeventstarttime;
        intent.putExtra(Intent.EXTRA_TEXT, VolEvent);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        getActivity().startActivity(Intent.createChooser(intent, "Send Invite..."));
    }

    private void markEventsAPI() {

        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<LoginResponse> userCall = apiInterface.mark_event_invitation(appToken, access_token, inviteId,markCheck);
        userCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (isFragmentVisible) {
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());

                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }
//                    else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
//                        recyclerView.setVisibility(View.GONE);
//                        empty_data.setVisibility(View.VISIBLE);
//                    }
                    else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (isFragmentVisible) {

                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }
}
