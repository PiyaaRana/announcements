package com.zoptal.announcements.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.adapter.ContactAdapter;
import com.zoptal.announcements.adapter.ContactsAdapter;
import com.zoptal.announcements.adapter.EventAdapter;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.ForgotPasswordActivity;
import com.zoptal.announcements.mainController.LoginSignUpActivity;
import com.zoptal.announcements.modal.ContactModel;
import com.zoptal.announcements.modal.ContactNameModel;
import com.zoptal.announcements.responseClasses.ContactsResponse;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.Utils;
import com.zoptal.announcements.utils.WebApiUrls;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Abc on 10/24/2017.
 */

public class ContactsFragment extends Fragment {

    BasicActivity activity;
    View view;
    private RecyclerView.LayoutManager layoutManager;
    private ContactAdapter madapter;
    private List<ContactNameModel> ShiftList;
    ProgressDialog pd;
    private RecyclerView recyclerView;
    String name, usertype, s;
    TextView empty_data;
    public boolean isFragmentVisible = false;
    private String buttonCheck = "false",otherUserId;
    private RelativeLayout mainLayout;
    private ProgressBar progressBar;
    private String appToken = "f6fa0fa5041479b76322145vgh1g410975af7766";
    String contact_no_values = "";
    private ArrayList<ContactsResponse.ContactsDataModal> contactListingResponseArrayList = new ArrayList<>();
    private MyActivity myActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isFragmentVisible = true;
        view = inflater.inflate(R.layout.contact_fragment, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ShiftList = new ArrayList<>();
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        findViews();
        getPhoneContacts();
        return view;
    }

    private void findViews() {
        empty_data = (TextView) view.findViewById(R.id.empty_data);
        mainLayout = (RelativeLayout) view.findViewById(R.id.content_frame);
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_contact);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible = false;
    }

    private void getData() {

        ContactNameModel contact = new ContactNameModel(R.mipmap.profile, "userName");
        ShiftList.add(contact);
        contact = new ContactNameModel(R.mipmap.profile, "userName");
        ShiftList.add(contact);
        contact = new ContactNameModel(R.mipmap.profile, "userName");
        ShiftList.add(contact);
        contact = new ContactNameModel(R.mipmap.profile, "userName");
        ShiftList.add(contact);
        contact = new ContactNameModel(R.mipmap.profile, "userName");
        ShiftList.add(contact);
        contact = new ContactNameModel(R.mipmap.profile, "userName");
        ShiftList.add(contact);
        contact = new ContactNameModel(R.mipmap.profile, "userName");
        ShiftList.add(contact);
        contact = new ContactNameModel(R.mipmap.profile, "userName");
        ShiftList.add(contact);
        madapter.notifyDataSetChanged();
    }


    private void getPhoneContacts() {
        ArrayList<String> contacts = new ArrayList<>();
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.e("phonenumber", phoneNumber);
            if (phoneNumber.length() >= 10) {
                //String  result = phoneNumber.replaceAll("[^\\w\\s\\p{Ps}\\p{Pe}]","");
                String result = getOnlyDigits(phoneNumber);
                contacts.add(result);
                Log.e("phonenumberfinal", result);
                HashSet hs = new HashSet();
                hs.addAll(contacts); // demoArrayList= name of arrayList from which u want to remove duplicates
                contacts.clear();
                contacts.addAll(hs);
                for (int i = 0; i < contacts.size(); i++) {
                    if (i == 0) {
                        contact_no_values = contacts.get(i);
                        Log.e("selectedparentsss", contact_no_values);
                    } else {
                        contact_no_values = contact_no_values + "," + contacts.get(i);
                        Log.e("selectedparent", contact_no_values);
                    }
                }

            }
        }
        phones.close();
        getContactsAPI();
    }

    public static String getOnlyDigits(String s) {
        String number = "";
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        number = matcher.replaceAll("");
        if (number.length() > 10) {
            number = number.substring(number.length() - 10);
        }
        return number;
    }

    private void getContactsAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<ContactsResponse> userCall = apiInterface.my_contacts(appToken, access_token, contact_no_values,"");
        userCall.enqueue(new Callback<ContactsResponse>() {
            @Override
            public void onResponse(Call<ContactsResponse> call, Response<ContactsResponse> response) {
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);

                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {

                        contactListingResponseArrayList.clear();
                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
                            itemsListingResponseArrayList.add(itemsListingModal);
                        }*/
                        contactListingResponseArrayList = response.body().getArrayList();
                        if (contactListingResponseArrayList.size() > 0) {
                            recyclerView.setVisibility(View.VISIBLE);
                            empty_data.setVisibility(View.GONE);
                            madapter = new ContactAdapter(getActivity());
                            recyclerView.setAdapter(madapter);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            empty_data.setVisibility(View.VISIBLE);
                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    } else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
                        recyclerView.setVisibility(View.GONE);
                        empty_data.setVisibility(View.VISIBLE);
                    } else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactsResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }


    public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {
        private Context context;
        String name, invite;
        String personid, notifyevent_id, imageurl, buttoncheck;
        ProgressDialog pd;


        public ContactAdapter(Context context) {
            super();
            this.context = context;
        }

        @Override
        public ContactAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.contact_item, parent, false);
            ContactAdapter.ViewHolder viewHolder = new ContactAdapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ContactAdapter.ViewHolder holder, final int position) {
           holder.person_name.setText(contactListingResponseArrayList.get(position).getUsername());
           imageurl= contactListingResponseArrayList.get(position).getPicture();
           if(!(imageurl.isEmpty())){
               Picasso.with(context).load(imageurl).resize(215,215).into(holder.person_pic);
           }
           else {
               Picasso.with(context).load(R.drawable.no_profile_grey_circle_bg).resize(215,215).into(holder.person_pic);
           }

           holder.clickLayout.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   otherUserId=contactListingResponseArrayList.get(position).getId();
                   if (context instanceof BasicActivity) {
                       ProfileFragment fragment = new ProfileFragment();
                       Bundle bundle = new Bundle();
                       bundle.putString("myprofile", "false"); //key and value
                       bundle.putString("otheruserid", otherUserId);
                       fragment.setArguments(bundle);
                       ((BasicActivity) context).replaceFragment(fragment);
                   }
               }
           });

        }

        @Override
        public int getItemCount() {
            return contactListingResponseArrayList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            public TextView person_name;
            public ImageView person_pic;
            public RelativeLayout clickLayout;


            public ViewHolder(View itemView) {
                super(itemView);
                person_name = (TextView) itemView.findViewById(R.id.contact_name);
                person_pic = (ImageView) itemView.findViewById(R.id.profile_icon_inbox);
               clickLayout = (RelativeLayout) itemView.findViewById(R.id.profile_click_layout);
                            }
        }


    }
}





