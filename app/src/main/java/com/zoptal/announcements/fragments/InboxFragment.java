package com.zoptal.announcements.fragments;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.adapter.EventAdapter;
import com.zoptal.announcements.adapter.InboxAdapter;
import com.zoptal.announcements.base.DataHelperClass;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.LoginSignUpActivity;
import com.zoptal.announcements.modal.EventContactsModal;
import com.zoptal.announcements.modal.EventModal;
import com.zoptal.announcements.modal.InboxModal;
import com.zoptal.announcements.responseClasses.CalendarEventResponse;
import com.zoptal.announcements.responseClasses.ContactsResponse;
import com.zoptal.announcements.responseClasses.EventResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.DateConvertor;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.Utils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class InboxFragment extends Fragment {
    View view;
    private RecyclerView recyclerView_inbox;
    private LinearLayoutManager layoutManager;
    private TextView txtview_empty,txtview_cal;
    private InboxAdapter inboxAdapter;
    private List<InboxModal> inboxModalList;
    BasicActivity basicActivity;
    private String appToken = "f6fa0fa5041479b76322145vgh1g410975af7766",contact_no_values="",eventId,searchValue="";
    private Boolean isFragmentVisible=false;
    private RelativeLayout mainLayout;
    private ProgressBar progressBar;
    private MyActivity myActivity;
    private EditText edt_search;
    private ImageView img_SearchIcon;
    private ArrayList<CalendarEventResponse.CalendarEventDataModal> inboxListingResponseArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.inbox_fragment,container,false);
        isFragmentVisible=true;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        inboxModalList= new ArrayList<>();
        basicActivity=(BasicActivity)getActivity();
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        findViews();
        return view;
    }

    private void findViews(){
        txtview_empty=(TextView)view.findViewById(R.id.empty_data);
        txtview_cal=(TextView)view.findViewById(R.id.tv_save_event);
        edt_search = (EditText) view.findViewById(R.id.edt_search);
        img_SearchIcon= (ImageView) view.findViewById(R.id.srchicon);
        mainLayout = (RelativeLayout) view.findViewById(R.id.content_frame);
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        recyclerView_inbox=(RecyclerView)view.findViewById(R.id.recycler_inbox);
        recyclerView_inbox.setHasFixedSize(true);
        layoutManager= new LinearLayoutManager(getActivity()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView_inbox.setLayoutManager(layoutManager);

        txtview_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                basicActivity.replaceFragment(new CalendarFragment());
            }
        });
        img_SearchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchValue=edt_search.getText().toString().trim();
                if (edt_search.getText().length()==0){
                    myActivity.showOneErrorDialog("Enter any search keyword.", "OK", getActivity());
                }
                else {
                    searchValue= edt_search.getText().toString().trim();
                    edt_search.setText("");
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
                    SearchOutputFragment fragment= new SearchOutputFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("searchstring", searchValue); //key and value
                    bundle.putString("screentype", "inbox");
                     fragment.setArguments(bundle);
                    ((BasicActivity)getActivity()).replaceFragment(fragment);
                }
            }

        });

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchValue= edt_search.getText().toString().trim();
                    edt_search.setText("");
                    SearchOutputFragment fragment= new SearchOutputFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("searchstring", searchValue); //key and value
                    bundle.putString("screentype", "inbox");
                    fragment.setArguments(bundle);
                    ((BasicActivity)getActivity()).replaceFragment(fragment);
                    return true;
                }
                return false;
            }
        });

        getPhoneContacts();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible=false;
    }

    private void getPhoneContacts() {
        ArrayList<String> contacts = new ArrayList<>();
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.e("phonenumber", phoneNumber);
            if (phoneNumber.length() >= 10) {
                //String  result = phoneNumber.replaceAll("[^\\w\\s\\p{Ps}\\p{Pe}]","");
                String result = getOnlyDigits(phoneNumber);
                contacts.add(result);
                Log.e("phonenumberfinal", result);
                HashSet hs = new HashSet();
                hs.addAll(contacts); // demoArrayList= name of arrayList from which u want to remove duplicates
                contacts.clear();
                contacts.addAll(hs);
                for (int i = 0; i < contacts.size(); i++) {
                    if (i == 0) {
                        contact_no_values = contacts.get(i);
                        Log.e("selectedparentsss", contact_no_values);
                    } else {
                        contact_no_values = contact_no_values + "," + contacts.get(i);
                        Log.e("selectedparent", contact_no_values);
                    }
                }

            }
        }
        phones.close();
        getInboxAPI();
    }
    public static String getOnlyDigits(String s) {
        String number = "";
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        number = matcher.replaceAll("");
        if (number.length() > 10) {
            number = number.substring(number.length() - 10);
        }
        return number;
    }

    private void loadEvents(){
        InboxModal contact = new InboxModal("Jesse Williams","Funeral for John Doe","10.00 am");
        inboxModalList.add(contact);
        contact = new InboxModal("Jesse Williams","Funeral for John Doe","10.00 am");
        inboxModalList.add(contact);
        contact = new InboxModal("Jesse Williams","Funeral for John Doe","10.00 am");
        inboxModalList.add(contact);
        contact = new InboxModal("Jesse Williams","Funeral for John Doe","10.00 am");
        inboxModalList.add(contact);
        inboxAdapter.notifyDataSetChanged();
    }

    private void getInboxAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<CalendarEventResponse> userCall = apiInterface.inbox(appToken, access_token,contact_no_values,"");
        userCall.enqueue(new Callback<CalendarEventResponse>() {
            @Override
            public void onResponse(Call<CalendarEventResponse> call, Response<CalendarEventResponse> response) {
                DataHelperClass.getInstance().setCalendarEventDataModal(response.body().getCalendarArrayList());
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);

                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {

                        inboxListingResponseArrayList.clear();
                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
                            itemsListingResponseArrayList.add(itemsListingModal);
                        }*/
                        inboxListingResponseArrayList = response.body().getCalendarArrayList();
                        if (inboxListingResponseArrayList.size() > 0) {
                            inboxAdapter = new InboxAdapter(getActivity());
                            recyclerView_inbox.setAdapter(inboxAdapter);

                        } else {
                            recyclerView_inbox.setVisibility(View.GONE);
                            txtview_empty.setVisibility(View.VISIBLE);
                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    } else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
                        recyclerView_inbox.setVisibility(View.GONE);
                        txtview_empty.setVisibility(View.VISIBLE);
                    } else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<CalendarEventResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }
    public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.MyViewHolder> {

        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder{
            public ImageView userImage;
            public TextView tv_userName,tv_eventName,tv_inboxTime,tv_inboxDate;
            public String imageName;
            private RelativeLayout click_layout;
            public MyViewHolder(View view){
                super(view);
                tv_userName=(TextView)view.findViewById(R.id.inbox_username);
                tv_eventName =(TextView)view.findViewById(R.id.inbox_event_name);
                tv_inboxTime =(TextView)view.findViewById(R.id.inbox_event_time);
                tv_inboxDate =(TextView)view.findViewById(R.id.inbox_event_date);
                click_layout=(RelativeLayout)view.findViewById(R.id.shift_items);
                userImage=(ImageView) view.findViewById(R.id.volEvent_img);

//                click_layout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent myactivity = new Intent(context.getApplicationContext(), EventDetailScreen.class);
//                        myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
//                        myactivity.putExtra("eventid",)
//                        context.getApplicationContext().startActivity(myactivity);
//                    }
//                });

            }
        }

        public InboxAdapter(Context context){

            this.context= context;
        }

        @Override
        public InboxAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inbox_list,parent,false);
            return new InboxAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(InboxAdapter.MyViewHolder holder, final int position) {
            holder.tv_userName.setText(inboxListingResponseArrayList.get(position).getContactName());
            holder.tv_eventName.setText(inboxListingResponseArrayList.get(position).getEventName());
            String inviteStatus=inboxListingResponseArrayList.get(position).getGoingStatus();
            Log.e("inviteStatus",inviteStatus);
            String dateString= inboxListingResponseArrayList.get(position).getEventDate();
            DateConvertor obj = new DateConvertor();
            final String eventdate = obj.changeDateFormatRecieved(dateString);
            Log.e("datevalue", eventdate);
            holder.tv_inboxDate.setText(eventdate);
            String startTime= inboxListingResponseArrayList.get(position).getStartTime();
            DateConvertor objstartdate = new DateConvertor();
            final String eventstarttime = obj.changeTimeFormatRecieve(startTime);
            Log.e("datevalues", eventstarttime);
            String endTime= inboxListingResponseArrayList.get(position).getEndTime();
            DateConvertor objendtime = new DateConvertor();
            final String eventendtime = obj.changeTimeFormatRecieve(endTime);
            Log.e("datevaluess", eventdate);
            holder.tv_inboxTime.setText(eventstarttime+" - "+eventendtime);
            String imageUrl=inboxListingResponseArrayList.get(position).getUserPic();
            if(!(imageUrl.isEmpty())){
                Picasso.with(context).load(imageUrl).resize(215,215).into(holder.userImage);
            }
            else {
                Picasso.with(context).load(R.drawable.no_profile_grey_circle_bg).resize(215,215).into(holder.userImage);
            }
            holder.click_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    eventId= inboxListingResponseArrayList.get(position).getId();
                    if (context instanceof BasicActivity) {
                    EventDetailScreen fragment = new EventDetailScreen();
                    Bundle bundle=new Bundle();
                    bundle.putString("eventid", eventId); //key and value
                        bundle.putString("screen", "false");
                    fragment.setArguments(bundle);
                    ((BasicActivity) context).replaceFragment(fragment);
                    }
                }
            });
        }

        public void removeAt(int position){
            inboxListingResponseArrayList.remove(position);
            notifyItemRemoved(position);
        }
        @Override
        public int getItemCount() {
            return inboxListingResponseArrayList.size();
        }
    }


}
