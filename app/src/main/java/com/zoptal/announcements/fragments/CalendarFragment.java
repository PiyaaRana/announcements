package com.zoptal.announcements.fragments;

/**
 * Created by Abc on 10/24/2017.
 */

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.arch.lifecycle.Lifecycle;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.adapter.CalendarAdapter;
import com.zoptal.announcements.adapter.InboxAdapter;
import com.zoptal.announcements.base.DataHelperClass;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.base.MyApp;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.LoginSignUpActivity;
import com.zoptal.announcements.modal.CalendarEventsModalClass;
import com.zoptal.announcements.modal.CalendarModalClass;
import com.zoptal.announcements.modal.InboxModal;
import com.zoptal.announcements.responseClasses.CalendarEventResponse;
import com.zoptal.announcements.responseClasses.EventResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.DateConvertor;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.Utils;
import com.zoptal.announcements.utils.WebApiUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


@TargetApi(3)

public class CalendarFragment extends Fragment implements View.OnClickListener {

    private static final String tag = "MyCalendarActivity";
    String date_month_year, s, cal_fragment = "false", calendar_response, calendarCheck;
    JSONArray jsonArraynew;
    private TextView currentMonth;
    private Button selectedDayMonthYearButton;
    private ImageView prevMonth;
    private ImageView nextMonth;
    private GridView calendarView;
    private GridCellAdapter adapter;
    private java.util.Calendar _calendar;
    String accesstoken, theday;
    Boolean flag = false;
    ProgressDialog pd;
    BasicActivity activity;
    @SuppressLint("NewApi")
    private int month, year;
    @SuppressWarnings("unused")
    @SuppressLint({"NewApi", "NewApi", "NewApi", "NewApi"})
    private final DateFormat dateFormatter = new DateFormat();
    private static final String dateTemplate = "MMMM , yyyy";
    private static final String CurrentDateTemplate = "yyyy-MM-dd";
    View view;
    Calendar cDate;
    private int pHr, pMin, cHr, cMin;
    private RecyclerView recyclerView_calendar;
    private LinearLayoutManager layoutManager;
    private String filePath, format_from, format_to, eventtimeFrom, eventtimeTo, eventId, screenEventDisplay = "otherevents", userId;
    private TextView txtview_startTime, txtview_endTime, txtview_myEvents, txtview_emptyData, txtview_otherEvents;
    private MyActivity myActivity;
    private CalendarAdapter calendarAdapter;
    private ArrayList<CalendarEventsModalClass> calendarModalClassList;
    public ProgressBar progressBar;
    private String appToken = "f6fa0fa5041479b76322145vgh1g410975af7766", userData = "", eventUserId;
    private RelativeLayout mainLayout;
    private String contact_no_values = "", dateString, eventCheck = "false";
    private Boolean isFragmentVisible = false;
    private ArrayList<CalendarEventResponse.CalendarEventDataModal> calendarListingResponseArrayList = new ArrayList<>();
    private ArrayList<CalendarEventsModalClass> calendarEventListingResponseArrayList= new ArrayList<>() ;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isFragmentVisible = true;
        view = inflater.inflate(R.layout.calendar_fragment, container, false);
        isFragmentVisible = true;
        activity = (BasicActivity) getActivity();
        calendarModalClassList = new ArrayList<>();
        calendarEventListingResponseArrayList= new ArrayList<>();
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        userId = SharedPrefsUtils.getStringPreference(getActivity(), "user_id");
        _calendar = java.util.Calendar.getInstance(Locale.getDefault());
        cDate = Calendar.getInstance();
        dateString = String.valueOf(DateFormat.format(CurrentDateTemplate, _calendar.getTime()));
        pHr = cHr = _calendar.get(Calendar.HOUR_OF_DAY);
        pMin = cMin = _calendar.get(Calendar.MINUTE);
        month = _calendar.get(java.util.Calendar.MONTH) + 1;
        year = _calendar.get(java.util.Calendar.YEAR);
        Log.d(tag, "Calendar Instance:= " + "Month: " + month + " " + "Year: "
                + year);
        /*selectedDayMonthYearButton = (Button) this
                .findViewById(R.id.selectedDayMonthYear);
        selectedDayMonthYearButton.setText("Selected: ");*/
        prevMonth = (ImageView) view.findViewById(R.id.prevMonth);
        prevMonth.setOnClickListener(this);
        mainLayout = (RelativeLayout) view.findViewById(R.id.f1_content_frame);
        txtview_myEvents = (TextView) view.findViewById(R.id.tv_my_event);
        txtview_myEvents.setOnClickListener(this);
        txtview_emptyData = (TextView) view.findViewById(R.id.empty_data);

        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        currentMonth = (TextView) view.findViewById(R.id.currentMonth);
        currentMonth.setText(DateFormat.format(dateTemplate,
                _calendar.getTime()));
        nextMonth = (ImageView) view.findViewById(R.id.nextMonth);
        nextMonth.setOnClickListener(this);
        recyclerView_calendar = (RecyclerView) view.findViewById(R.id.recycler_calendar_events);
        recyclerView_calendar.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView_calendar.setLayoutManager(layoutManager);
        calendarView = (GridView) view.findViewById(R.id.calendar);
        setGridCellAdapterToDate(month, year, eventCheck);
        getPhoneContacts();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible = false;
    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;
        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();
        float x = 1;
        if (items > columns) {
            x = items / columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }
        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);
    }

    /**
     * @param month
     * @param year
     */
    private void setGridCellAdapterToDate(int month, int year, String buttonValue) {
        adapter = new GridCellAdapter(getActivity().getApplicationContext(),
                R.id.calendar_day_gridcell, month, year);
        _calendar.set(year, month - 1, _calendar.get(java.util.Calendar.DAY_OF_MONTH));
        currentMonth.setText(DateFormat.format(dateTemplate,
                _calendar.getTime()));
        adapter.notifyDataSetChanged();
        calendarView.setAdapter(adapter);
        setGridViewHeightBasedOnChildren(calendarView, 7);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevMonth:
                if (month <= 1) {
                    month = 12;
                    year--;
                } else {
                    month--;
                }
                Log.d(tag, "Setting Prev Month in GridCellAdapter: " + "Month: "
                        + month + " Year: " + year);
                setGridCellAdapterToDate(month, year, eventCheck);
                _calendar.add(Calendar.DATE, -1);
                dateString = String.valueOf(DateFormat.format(CurrentDateTemplate, _calendar.getTime()));
                calendarEventAPI();
                break;
            case R.id.nextMonth:
                if (month > 11) {
                    month = 1;
                    year++;
                } else {
                    month++;
                }
                Log.d(tag, "Setting Next Month in GridCellAdapter: " + "Month: "
                        + month + " Year: " + year);
                setGridCellAdapterToDate(month, year, eventCheck);
                _calendar.add(Calendar.DATE, 1);
                dateString = String.valueOf(DateFormat.format(CurrentDateTemplate, _calendar.getTime()));
                calendarEventAPI();
                break;
            case R.id.tv_my_event:
                if (screenEventDisplay.equalsIgnoreCase("otherevents")) {
                    txtview_myEvents.setText("Events added to calendar");
                    screenEventDisplay = "myevents";
                } else if (screenEventDisplay.equalsIgnoreCase("myevents")) {
                    txtview_myEvents.setText("Events I'm hosting");
                    screenEventDisplay = "otherevents";
                }

                //getPhoneContacts();
                calendarEventAPI();
                break;
        }
    }

    @Override
    public void onDestroy() {
        Log.d(tag, "Destroying View …");
        super.onDestroy();
    }

    // Inner Class
    public class GridCellAdapter extends BaseAdapter {
        private static final String tag = "GridCellAdapter";
        private final Context _context;
        private final List<String> list;
        private static final int DAY_OFFSET = 1;
        private final String[] weekdays = new String[]{"Sun", "Mon", "Tue",
                "Wed", "Thu", "Fri", "Sat"};
        private final String[] months = {"January", "February", "March",
                "April", "May", "June", "July", "August", "September",
                "October", "November", "December"};
        private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30,
                31, 30, 31};
        private int daysInMonth;
        private int currentDayOfMonth;
        private int currentWeekDay;
        private Button gridcell;
        private TextView num_events_per_day;
        private final HashMap<String, Integer> eventsPerMonthMap;
        private final SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "dd-MMM-yyyy");
        private ArrayList<String> datesarray = new ArrayList<String>();
        private String button_type;
        private RelativeLayout datelayout;
        private ImageView iv_shedule;
        private String firstdate;

        // Days in Current Month
        public GridCellAdapter(Context context, int textViewResourceId,
                               int month, int year) {
            super();
            this._context = context;
            this.list = new ArrayList<String>();
            Log.d(tag, "==> Passed in Date FOR Month: " + month + " "
                    + "Year: " + year);
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            setCurrentDayOfMonth(calendar.get(java.util.Calendar.DAY_OF_MONTH));
            setCurrentWeekDay(calendar.get(java.util.Calendar.DAY_OF_WEEK));
            Log.d(tag, "New Calendar:= " + calendar.getTime().toString());
            Log.d(tag, "CurrentDayOfWeek :" + getCurrentWeekDay());
            Log.d(tag, "CurrentDayOfMonth :" + getCurrentDayOfMonth());
// Print Month
            printMonth(month, year);
// Find Number of Events
            eventsPerMonthMap = findNumberOfEventsPerMonth(year, month);
        }

        private String getMonthAsString(int i) {
            return months[i];
        }

        private String getWeekDayAsString(int i) {
            return weekdays[i];
        }

        private int getNumberOfDaysOfMonth(int i) {
            return daysOfMonth[i];
        }

        public String getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        /**
         * Prints Month
         *
         * @param mm
         * @param yy
         */
        private void printMonth(int mm, int yy) {
            Log.d(tag, "==> printMonth: mm: " + mm + " " + "yy: " + yy);
            int trailingSpaces = 0;
            int daysInPrevMonth = 0;
            int prevMonth = 0;
            int prevYear = 0;
            int nextMonth = 0;
            int nextYear = 0;
            int currentMonth = mm - 1;
            String currentMonthName = getMonthAsString(currentMonth);
            daysInMonth = getNumberOfDaysOfMonth(currentMonth);

            Log.d(tag, "Current Month: " + " " + currentMonthName + " having "
                    + daysInMonth + " days.");

            GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);
            Log.d(tag, "Gregorian Calendar:= " + cal.getTime().toString());

            if (currentMonth == 11) {
                prevMonth = currentMonth - 1;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 0;
                prevYear = yy;
                nextYear = yy + 1;
                Log.d(tag, "*->PrevYear: " + prevYear + " PrevMonth:"
                        + prevMonth + " NextMonth: " + nextMonth
                        + " NextYear: " + nextYear);
            } else if (currentMonth == 0) {
                prevMonth = 11;
                prevYear = yy - 1;
                nextYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 1;
                Log.d(tag, "**-> PrevYear: " + prevYear + " PrevMonth:"
                        + prevMonth + " NextMonth: " + nextMonth
                        + " NextYear: " + nextYear);
            } else {
                prevMonth = currentMonth - 1;
                nextMonth = currentMonth + 1;
                nextYear = yy;
                prevYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                Log.d(tag, "***—> PrevYear: " + prevYear + " PrevMonth:"
                        + prevMonth + " NextMonth: " + nextMonth
                        + " NextYear: " + nextYear);
            }

            int currentWeekDay = cal.get(java.util.Calendar.DAY_OF_WEEK) - 1;
            trailingSpaces = currentWeekDay;
            Log.d(tag, "Week Day:" + currentWeekDay + " is "
                    + getWeekDayAsString(currentWeekDay));
            Log.d(tag, "No. Trailing space to Add: " + trailingSpaces);
            Log.d(tag, "No. of Days in Previous Month: " + daysInPrevMonth);

            if (cal.isLeapYear(cal.get(java.util.Calendar.YEAR)))
                if (mm == 2)
                    ++daysInMonth;
                else if (mm == 3)
                    ++daysInPrevMonth;
// Trailing Month days
            for (int i = 0; i < trailingSpaces; i++) {
                Log.d(tag,
                        "PREV MONTH:= "
                                + prevMonth
                                + " => "
                                + getMonthAsString(prevMonth)
                                + " "
                                + String.valueOf((daysInPrevMonth
                                - trailingSpaces + DAY_OFFSET)
                                + i));
                list.add(String
                        .valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET)
                                + i)
                        + "-GREY"
                        + "-"
                        + getMonthAsString(prevMonth)
                        + "-"
                        + prevYear);
            }

// Current Month Days
            for (int i = 1; i <= daysInMonth; i++) {
                Log.d(currentMonthName, String.valueOf(i) + " "
                        + getMonthAsString(currentMonth) + " " + yy);
                if (i == getCurrentDayOfMonth()) {
                    list.add(String.valueOf(i) + "-BLUE" + "-"
                            + getMonthAsString(currentMonth) + "-" + yy);
                } else {
                    list.add(String.valueOf(i) + "-WHITE" + "-"
                            + getMonthAsString(currentMonth) + "-" + yy);
                }
            }

// Leading Month days
            for (int i = 0; i < list.size() % 7; i++) {
                Log.d(tag, "NEXT MONTH:= " + getMonthAsString(nextMonth));
                list.add(String.valueOf(i + 1) + "-GREY" + "-"
                        + getMonthAsString(nextMonth) + "-" + nextYear);
            }
        }

        /**
         * NOTE: YOU NEED TO IMPLEMENT THIS PART Given the YEAR, MONTH, retrieve
         * ALL entries from a SQLite database for that month. Iterate over the
         * List of All entries, and get the dateCreated, which is converted into
         * day.
         *
         * @param year
         * @param month
         * @return
         */
        private HashMap<String, Integer> findNumberOfEventsPerMonth(int year,
                                                                    int month) {
            HashMap<String, Integer> map = new HashMap<String, Integer>();
            return map;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) _context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.screen_gridcell, parent, false);
            }

// Get a reference to the Day gridcell
            gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
            iv_shedule = (ImageView) row.findViewById(R.id.iv_shedule);
            datelayout = (RelativeLayout) row.findViewById(R.id.date_layout);

            gridcell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    date_month_year = (String) view.getTag();
                    //selectedDayMonthYearButton.setText("Selected: " + date_month_year);
                    ((MyApp) getActivity().getApplicationContext()).setDateValue(date_month_year);
                    String[] datearray = date_month_year.split("-");
                    firstdate = datearray[0];
                    String monthvalue = datearray[1];
                    adapter.notifyDataSetChanged();
                    calendarView.setAdapter(adapter);
                    //iv_shedule.setVisibility(View.VISIBLE);
                    //adapter.notifyDataSetChanged();
                    Log.e("Selected date", date_month_year);
                    try {
                        Date parsedDate = dateFormatter.parse(date_month_year);
                        Log.d(tag, "Parsed Date: " + parsedDate.toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            });
// ACCOUNT FOR SPACING
            Log.d(tag, "Current Day: " + getCurrentDayOfMonth());
            String[] day_color = list.get(position).split("-");
            theday = day_color[0];
            String themonth = day_color[2];
            String theyear = day_color[3];
            if ((!eventsPerMonthMap.isEmpty()) && (eventsPerMonthMap != null)) {
                if (eventsPerMonthMap.containsKey(theday)) {
                    num_events_per_day = (TextView) row
                            .findViewById(R.id.num_events_per_day);
                    Integer numEvents = (Integer) eventsPerMonthMap.get(theday);
                    num_events_per_day.setText(numEvents.toString());
                }
            }
// Set the Day GridCell
            gridcell.setText(theday);
            datesarray.add(theday);
            gridcell.setTag(theday + "-" + themonth + "-" + theyear);
            Log.d(tag, "Setting GridCell " + theday + "-" + themonth + "-"
                    + theyear);

            if (calendarEventListingResponseArrayList.size() > 0 ) {
                for (int i = 0; i < calendarEventListingResponseArrayList.size(); i++) {
                    String dateInString = calendarEventListingResponseArrayList.get(i).getCalendarEventDate();
                    String[] dateArray = dateInString.split("-");
                    String monthString = getMonth(Integer.parseInt(dateArray[1]));
                    String dayinstring = dateArray[2];
                    if (monthString.equalsIgnoreCase(themonth)) {
                        if (dayinstring.substring(0, 1).equalsIgnoreCase("0")) {
                            dayinstring = dayinstring.replace("0", "");
                        }
                        if (theday.equalsIgnoreCase(dayinstring)) {
                            iv_shedule.setVisibility(View.VISIBLE);

                        } else {
                            System.out.println("date not matched");
                            //break;
                        }
                    } else {
                        System.out.println("month not matched");
                        // break;
                    }
                }
            }
             else {
                iv_shedule.setVisibility(View.GONE);
            }

//            if (datesarray.get(position).equals("5") || datesarray.get(position).equals("8") || datesarray.get(position).equals("9")
//                    || datesarray.get(position).equals("11") || datesarray.get(position).equals("17") || datesarray.get(position).equals("19")
//                    || datesarray.get(position).equals("21")
//                    || datesarray.get(position).equals("22") || datesarray.get(position).equals("23")) {
//                iv_shedule.setVisibility(View.VISIBLE);
//                //gridcell.setTextColor(Color.parseColor("#ff0000"));
//
//            }
            if (day_color[1].equals("GREY")) {
                gridcell.setTextColor(Color.parseColor("#ffffff"));
                iv_shedule.setVisibility(View.GONE);
            }
            if (day_color[1].equals("WHITE")) {
                gridcell.setTextColor(getResources().getColor(
                        R.color.grey));
            }
            if (day_color[1].equals("BLUE")) {
                gridcell.setTextColor(getResources()
                        .getColor(R.color.grey));
            }
            // theday.equals(datesarray.get(position).)
            return row;
        }

        // @Override
        // public void onClick(View view) {
//            date_month_year = (String) view.getTag();
//            //selectedDayMonthYearButton.setText("Selected: " + date_month_year);
//            String[] datearray = date_month_year.split("-");
//            String firstdate=datearray[0];
//            String monthvalue=datearray[1];
//
//            //iv_shedule.setVisibility(View.VISIBLE);
//            //adapter.notifyDataSetChanged();
//            Log.e("Selected date", date_month_year);
//            try {
//                Date parsedDate = dateFormatter.parse(date_month_year);
//                Log.d(tag, "Parsed Date: " + parsedDate.toString());
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }

        //  }
        public int getCurrentDayOfMonth() {
            return currentDayOfMonth;
        }

        private void setCurrentDayOfMonth(int currentDayOfMonth) {
            this.currentDayOfMonth = currentDayOfMonth;
        }

        public void setCurrentWeekDay(int currentWeekDay) {
            this.currentWeekDay = currentWeekDay;
        }

        public int getCurrentWeekDay() {
            return currentWeekDay;
        }
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    private void getPhoneContacts() {
        ArrayList<String> contacts = new ArrayList<>();
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.e("phonenumber", phoneNumber);
            if (phoneNumber.length() >= 10) {
                //String  result = phoneNumber.replaceAll("[^\\w\\s\\p{Ps}\\p{Pe}]","");
                String result = getOnlyDigits(phoneNumber);
                contacts.add(result);
                Log.e("phonenumberfinal", result);
                HashSet hs = new HashSet();
                hs.addAll(contacts); // demoArrayList= name of arrayList from which u want to remove duplicates
                contacts.clear();
                contacts.addAll(hs);
                for (int i = 0; i < contacts.size(); i++) {
                    if (i == 0) {
                        contact_no_values = contacts.get(i);
                        Log.e("selectedparentsss", contact_no_values);
                    } else {
                        contact_no_values = contact_no_values + "," + contacts.get(i);
                        Log.e("selectedparent", contact_no_values);
                    }
                }

            }
        }
        phones.close();
        calendarEventAPI();
//        if(screenEventDisplay.equalsIgnoreCase("otherevents")){
//            txtview_emptyData.setVisibility(View.VISIBLE);
//            recyclerView_calendar.setVisibility(View.GONE);
//        }
//        else if(screenEventDisplay.equalsIgnoreCase("myevents")){
//            txtview_emptyData.setVisibility(View.GONE);
//            calendarEventAPI();
        //}

    }

    public static String getOnlyDigits(String s) {
        String number = "";
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        number = matcher.replaceAll("");
        if (number.length() > 10) {
            number = number.substring(number.length() - 10);
        }
        return number;
    }

    private void calendarEventAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<CalendarEventResponse> userCall = apiInterface.my_calendars(appToken, access_token, dateString, "month", contact_no_values, "");
        userCall.enqueue(new Callback<CalendarEventResponse>() {
            @Override
            public void onResponse(Call<CalendarEventResponse> call, Response<CalendarEventResponse> response) {
                DataHelperClass.getInstance().setCalendarEventDataModal(response.body().getCalendarArrayList());
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        calendarListingResponseArrayList.clear();
                        calendarEventListingResponseArrayList.clear();

                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
                            itemsListingResponseArrayList.add(itemsListingModal);
                        }*/
                        calendarListingResponseArrayList = response.body().getCalendarArrayList();
                        if (calendarListingResponseArrayList.size() > 0) {
                            if (screenEventDisplay.equalsIgnoreCase("otherevents")) {
                                for (int k = 0; k < calendarListingResponseArrayList.size(); k++) {

                                    if (!(userId.equalsIgnoreCase(calendarListingResponseArrayList.get(k).getUserId()))) {
                                        CalendarEventsModalClass modalClass = new CalendarEventsModalClass();
                                        modalClass.setUserId(calendarListingResponseArrayList.get(k).getId());
                                        modalClass.setHostName(calendarListingResponseArrayList.get(k).getContactName());
                                        modalClass.setCalendarEventName(calendarListingResponseArrayList.get(k).getEventName());
                                        modalClass.setCalendarEventDate(calendarListingResponseArrayList.get(k).getEventDate());
                                        modalClass.setEventStartTime(calendarListingResponseArrayList.get(k).getStartTime());
                                        modalClass.setEventEndTime(calendarListingResponseArrayList.get(k).getEndTime());
                                        modalClass.setCalendarEventLocation(calendarListingResponseArrayList.get(k).getEventLocation());
                                        modalClass.setUserImage(calendarListingResponseArrayList.get(k).getUserPic());
                                        calendarEventListingResponseArrayList.add(modalClass);
                                    }

                                }

                            } else if(screenEventDisplay.equalsIgnoreCase("myevents")) {
                                for (int k = 0; k < calendarListingResponseArrayList.size(); k++) {
                                    if (userId.equalsIgnoreCase(calendarListingResponseArrayList.get(k).getUserId())) {
                                        CalendarEventsModalClass modalClass = new CalendarEventsModalClass();
                                        modalClass.setUserId(calendarListingResponseArrayList.get(k).getId());
                                        modalClass.setHostName(calendarListingResponseArrayList.get(k).getContactName());
                                        modalClass.setCalendarEventName(calendarListingResponseArrayList.get(k).getEventName());
                                        modalClass.setCalendarEventDate(calendarListingResponseArrayList.get(k).getEventDate());
                                        modalClass.setEventStartTime(calendarListingResponseArrayList.get(k).getStartTime());
                                        modalClass.setEventEndTime(calendarListingResponseArrayList.get(k).getEndTime());
                                        modalClass.setCalendarEventLocation(calendarListingResponseArrayList.get(k).getEventLocation());
                                        modalClass.setUserImage(calendarListingResponseArrayList.get(k).getUserPic());
                                        calendarEventListingResponseArrayList.add(modalClass);
                                    }

                                }
                            }
                            else {

                            }
                                if(calendarEventListingResponseArrayList.size()>0) {
                                    txtview_emptyData.setVisibility(View.GONE);
                                    recyclerView_calendar.setVisibility(View.VISIBLE);
                                    calendarAdapter = new CalendarAdapter(getActivity(),calendarEventListingResponseArrayList);
                                    recyclerView_calendar.setAdapter(calendarAdapter);
                                    adapter = new GridCellAdapter(getActivity().getApplicationContext(),
                                            R.id.calendar_day_gridcell, month, year);
                                    adapter.notifyDataSetChanged();
                                    calendarView.setAdapter(adapter);
                                    setGridViewHeightBasedOnChildren(calendarView, 7);
                                }
                                else {
                                    txtview_emptyData.setVisibility(View.VISIBLE);
                                    recyclerView_calendar.setVisibility(View.GONE);
                                    adapter = new GridCellAdapter(getActivity().getApplicationContext(),
                                            R.id.calendar_day_gridcell, month, year);
                                    adapter.notifyDataSetChanged();
                                    calendarView.setAdapter(adapter);
                                    setGridViewHeightBasedOnChildren(calendarView, 7);
                                }

                            }
                        else {
                            txtview_emptyData.setVisibility(View.VISIBLE);
                            recyclerView_calendar.setVisibility(View.GONE);
                            adapter = new GridCellAdapter(getActivity().getApplicationContext(),
                                    R.id.calendar_day_gridcell, month, year);
                            adapter.notifyDataSetChanged();
                            calendarView.setAdapter(adapter);
                            setGridViewHeightBasedOnChildren(calendarView, 7);
                        }

//                        contactListingResponseArrayList.clear();
//                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
//                            itemsListingResponseArrayList.add(itemsListingModal);
//                        }*/
//                        contactListingResponseArrayList = response.body().getArrayList();
//                        if (contactListingResponseArrayList.size() > 0) {
//                            recyclerView.setVisibility(View.VISIBLE);
//                            tv_emptyData.setVisibility(View.GONE);
//                            cadapter = new ParentAdapter(getActivity());
//                            recyclerView.setAdapter(cadapter);
//                        } else {
//                            recyclerView.setVisibility(View.GONE);
//                            tv_emptyData.setVisibility(View.VISIBLE);
//                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    } else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {

                    } else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<CalendarEventResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }

    public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder> {
        private Context context;
        private ArrayList<CalendarEventsModalClass> calendarList;
        private String eventdateRecieved, startTime, endTime, imageUrl;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView userImage;
            public TextView tv_userName, tv_eventName, tv_inboxTime, tv_inboxDate, tv_locName;
            public String imageName;
            private RelativeLayout click_layout, rowLayout;
            private LinearLayout locLayout;

            public MyViewHolder(View view) {
                super(view);
                tv_userName = (TextView) view.findViewById(R.id.inbox_username);
                tv_eventName = (TextView) view.findViewById(R.id.inbox_event_name);
                tv_inboxTime = (TextView) view.findViewById(R.id.inbox_event_time);
                tv_inboxDate = (TextView) view.findViewById(R.id.inbox_date);
                click_layout = (RelativeLayout) view.findViewById(R.id.shift_items);
                rowLayout = (RelativeLayout) view.findViewById(R.id.row_layout);
                locLayout = (LinearLayout) view.findViewById(R.id.loc_layout);
                tv_locName = (TextView) view.findViewById(R.id.event_loc);
                userImage = (ImageView) view.findViewById(R.id.volEvent_img);
            }
        }

        public CalendarAdapter(Context context,ArrayList<CalendarEventsModalClass>calendarList) {
            this.context = context;
            this.calendarList=calendarList;
        }

        @Override
        public CalendarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_event_list, parent, false);
            return new CalendarAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(CalendarAdapter.MyViewHolder holder, final int position) {
            holder.tv_userName.setText(calendarList.get(position).getHostName());
            holder.tv_eventName.setText(calendarList.get(position).getCalendarEventName());
            eventdateRecieved = calendarList.get(position).getCalendarEventDate();
            startTime = calendarList.get(position).getEventStartTime();
            endTime = calendarList.get(position).getEventEndTime();
            holder.tv_locName.setText(calendarList.get(position).getCalendarEventLocation());
            imageUrl = calendarList.get(position).getUserImage();

            if (imageUrl != null && !imageUrl.isEmpty()) {
                Picasso.with(context).load(imageUrl).resize(215, 215).into(holder.userImage);
            } else {
                Picasso.with(context).load(R.drawable.no_profile_grey_circle_bg).resize(215, 215).into(holder.userImage);
            }
            if (eventdateRecieved != null && !eventdateRecieved.isEmpty()) {
                DateConvertor obj = new DateConvertor();
                final String eventdate = obj.changeDateFormatRecieved(eventdateRecieved);
                Log.e("datevalue", eventdate);
                holder.tv_inboxDate.setText(eventdate);
            }
            if (startTime != null && !startTime.isEmpty() && endTime != null && !endTime.isEmpty()) {
                DateConvertor objstartdate = new DateConvertor();
                final String eventstarttime = objstartdate.changeTimeFormatRecieve(startTime);
                Log.e("datevalues", eventstarttime);
                DateConvertor objendtime = new DateConvertor();
                final String eventendtime = objendtime.changeTimeFormatRecieve(endTime);
                Log.e("datevaluess", eventendtime);
                holder.tv_inboxTime.setText(eventstarttime + " - " + eventendtime);

            }
           holder.click_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    eventId = calendarList.get(position).getUserId();
                    if (context instanceof BasicActivity) {
                        EventDetailScreen fragment = new EventDetailScreen();
                        Bundle bundle = new Bundle();
                        bundle.putString("eventid", eventId); //key and value
                        bundle.putString("screen", "false");
                        fragment.setArguments(bundle);
                        ((BasicActivity) context).replaceFragment(fragment);
                    }
                }

            });

        }


        public void removeAt(int position) {
            calendarList.remove(position);
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return calendarList.size();
        }
    }


}




