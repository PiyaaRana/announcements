package com.zoptal.announcements.fragments;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.adapter.PastEventAdapter;
import com.zoptal.announcements.base.DataHelperClass;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.LoginSignUpActivity;
import com.zoptal.announcements.modal.CalendarEventsModalClass;
import com.zoptal.announcements.modal.MyEventsModalClass;
import com.zoptal.announcements.modal.PastEventModal;
import com.zoptal.announcements.responseClasses.CalendarEventResponse;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.DateConvertor;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.Utils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyEventsFragment  extends Fragment{
    View view;
    private TextView txtview_screenHeading,txtview_emptyData,txtview_save;
    private RecyclerView recyclerView_events;
    private RecyclerView.LayoutManager layoutManager;
    private PastEventAdapter pastEventAdapter;
    private List<PastEventModal> pastEventModalList;
    private String appToken = "f6fa0fa5041479b76322145vgh1g410975af7766",dateString,contact_no_values = "",eventId,inviteId,markCheck="1",screenEventDisplay = "otherevents";
    private RelativeLayout mainLayout;
    private ProgressBar progressBar;
    private Boolean isFragmentVisible=false;
    private MyActivity myActivity;
    private ArrayList<CalendarEventResponse.CalendarEventDataModal> calendarListingResponseArrayList = new ArrayList<>();
    private ArrayList<MyEventsModalClass> EventListingResponseArrayList= new ArrayList<>() ;
    private static final String CurrentDateTemplate = "yyyy-MM-dd";
    private Calendar _calendar;
    private String userId,eventUserId,searchValue="", eventdateRecieved, shareEventStartTime, shareEventName, shareEventLocation,shareEventDesc, shareEventDate,shareeventstarttime;
    private EditText edt_search;
    private ImageView img_SearchIcon;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.inbox_fragment,container,false);
        isFragmentVisible=true;
        pastEventModalList= new ArrayList<>();
        _calendar= Calendar.getInstance();
        dateString=String.valueOf(DateFormat.format(CurrentDateTemplate, _calendar.getTime()));
        userId = SharedPrefsUtils.getStringPreference(getActivity(), "user_id");
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        findViews();
        return view;
    }

    private void findViews(){
        txtview_screenHeading=(TextView) view.findViewById(R.id.tv_screen_heading);
        txtview_screenHeading.setText("MY EVENTS");
        txtview_save= (TextView) view.findViewById(R.id.tv_save_event);
        txtview_save.setText("Events I'm hosting");
        edt_search = (EditText) view.findViewById(R.id.edt_search);
        img_SearchIcon= (ImageView) view.findViewById(R.id.srchicon);
        mainLayout = (RelativeLayout) view.findViewById(R.id.content_frame);
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        txtview_emptyData=(TextView)view.findViewById(R.id.empty_data);
        recyclerView_events=(RecyclerView)view.findViewById(R.id.recycler_inbox);
        recyclerView_events.setHasFixedSize(true);
        layoutManager= new LinearLayoutManager(getActivity()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView_events.setLayoutManager(layoutManager);
        img_SearchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchValue=edt_search.getText().toString().trim();
                if (edt_search.getText().length()==0){
                    myActivity.showOneErrorDialog("Enter any search keyword.", "OK", getActivity());
                }
                else {
                    searchValue= edt_search.getText().toString().trim();
                    edt_search.setText("");
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
                    SearchOutputFragment fragment= new SearchOutputFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("searchstring", searchValue); //key and value
                    bundle.putString("screentype", "myevents");
                    fragment.setArguments(bundle);
                    ((BasicActivity)getActivity()).replaceFragment(fragment);
                }
            }

        });

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchValue= edt_search.getText().toString().trim();
                    edt_search.setText("");
                    SearchOutputFragment fragment= new SearchOutputFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("searchstring", searchValue); //key and value
                    bundle.putString("screentype", "myevents");
                    fragment.setArguments(bundle);
                    ((BasicActivity)getActivity()).replaceFragment(fragment);
                    return true;
                }
                return false;
            }
        });

        txtview_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (screenEventDisplay.equalsIgnoreCase("otherevents")) {
                    txtview_save.setText("Events I'm attending");
                    screenEventDisplay = "myevents";
                } else if (screenEventDisplay.equalsIgnoreCase("myevents")) {
                    txtview_save.setText("Events I'm hosting");
                    screenEventDisplay = "otherevents";
                }
                myEventAPI();
            }
        });

        getPhoneContacts();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible=false;
    }

    private void getPhoneContacts() {
        ArrayList<String> contacts = new ArrayList<>();
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.e("phonenumber", phoneNumber);
            if (phoneNumber.length() >= 10) {
                //String  result = phoneNumber.replaceAll("[^\\w\\s\\p{Ps}\\p{Pe}]","");
                String result = getOnlyDigits(phoneNumber);
                contacts.add(result);
                Log.e("phonenumberfinal", result);
                HashSet hs = new HashSet();
                hs.addAll(contacts); // demoArrayList= name of arrayList from which u want to remove duplicates
                contacts.clear();
                contacts.addAll(hs);
                for (int i = 0; i < contacts.size(); i++) {
                    if (i == 0) {
                        contact_no_values = contacts.get(i);
                        Log.e("selectedparentsss", contact_no_values);
                    } else {
                        contact_no_values = contact_no_values + "," + contacts.get(i);
                        Log.e("selectedparent", contact_no_values);
                    }
                }

            }
        }
        phones.close();
        myEventAPI();
    }
    public static String getOnlyDigits(String s) {
        String number = "";
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        number = matcher.replaceAll("");
        if (number.length() > 10) {
            number = number.substring(number.length() - 10);
        }
        return number;
    }


    private void myEventAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<CalendarEventResponse> userCall = apiInterface.my_events(appToken, access_token, dateString,"year",contact_no_values,"");
        userCall.enqueue(new Callback<CalendarEventResponse>() {
            @Override
            public void onResponse(Call<CalendarEventResponse> call, Response<CalendarEventResponse> response) {
                DataHelperClass.getInstance().setCalendarEventDataModal(response.body().getCalendarArrayList());
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        calendarListingResponseArrayList.clear();
                        EventListingResponseArrayList.clear();

                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
                            itemsListingResponseArrayList.add(itemsListingModal);
                        }*/
                        calendarListingResponseArrayList = response.body().getCalendarArrayList();
                        if (calendarListingResponseArrayList.size() > 0) {
                            if (screenEventDisplay.equalsIgnoreCase("otherevents")) {
                                for (int k = 0; k < calendarListingResponseArrayList.size(); k++) {

                                    if (!(userId.equalsIgnoreCase(calendarListingResponseArrayList.get(k).getUserId()))) {
                                        MyEventsModalClass modalClass = new MyEventsModalClass();
                                        modalClass.setEventId(calendarListingResponseArrayList.get(k).getId());
                                        modalClass.setHostName(calendarListingResponseArrayList.get(k).getContactName());
                                        modalClass.setEventName(calendarListingResponseArrayList.get(k).getEventName());
                                        modalClass.setEventDate(calendarListingResponseArrayList.get(k).getEventDate());
                                        modalClass.setEventStartTime(calendarListingResponseArrayList.get(k).getStartTime());
                                        modalClass.setEventEndTime(calendarListingResponseArrayList.get(k).getEndTime());
                                        modalClass.setEventLocation(calendarListingResponseArrayList.get(k).getEventLocation());
                                        modalClass.setUserId(calendarListingResponseArrayList.get(k).getUserId());
                                        modalClass.setEventDescription(calendarListingResponseArrayList.get(k).getDescription());
                                        modalClass.setCalCheck(calendarListingResponseArrayList.get(k).getCalCheck());
                                        EventListingResponseArrayList.add(modalClass);
                                    }

                                }

                            } else if(screenEventDisplay.equalsIgnoreCase("myevents")) {
                                for (int k = 0; k < calendarListingResponseArrayList.size(); k++) {
                                    if (userId.equalsIgnoreCase(calendarListingResponseArrayList.get(k).getUserId())) {
                                        MyEventsModalClass modalClass = new MyEventsModalClass();
                                        modalClass.setEventId(calendarListingResponseArrayList.get(k).getId());
                                        modalClass.setHostName(calendarListingResponseArrayList.get(k).getContactName());
                                        modalClass.setEventName(calendarListingResponseArrayList.get(k).getEventName());
                                        modalClass.setEventDate(calendarListingResponseArrayList.get(k).getEventDate());
                                        modalClass.setEventStartTime(calendarListingResponseArrayList.get(k).getStartTime());
                                        modalClass.setEventEndTime(calendarListingResponseArrayList.get(k).getEndTime());
                                        modalClass.setEventLocation(calendarListingResponseArrayList.get(k).getEventLocation());
                                        modalClass.setUserImage(calendarListingResponseArrayList.get(k).getUserPic());
                                        modalClass.setUserId(calendarListingResponseArrayList.get(k).getUserId());
                                        modalClass.setEventDescription(calendarListingResponseArrayList.get(k).getDescription());
                                        modalClass.setCalCheck(calendarListingResponseArrayList.get(k).getCalCheck());
                                        EventListingResponseArrayList.add(modalClass);
                                    }

                                }
                            }
                            else {

                            }
                            if(EventListingResponseArrayList.size()>0) {
                                txtview_emptyData.setVisibility(View.GONE);
                                recyclerView_events.setVisibility(View.VISIBLE);
                                pastEventAdapter = new PastEventAdapter(getActivity(),EventListingResponseArrayList);
                                recyclerView_events.setAdapter(pastEventAdapter);

                            }
                            else {
                                txtview_emptyData.setVisibility(View.VISIBLE);
                                txtview_emptyData.setText("No events found.");
                                recyclerView_events.setVisibility(View.GONE);

                            }

                        }
                        else {
                            txtview_emptyData.setVisibility(View.VISIBLE);
                            txtview_emptyData.setText("No events found.");
                            recyclerView_events.setVisibility(View.GONE);

                        }

                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    } else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {

                    } else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }


            @Override
            public void onFailure(Call<CalendarEventResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }

    public class PastEventAdapter extends RecyclerView.Adapter<PastEventAdapter.ViewHolder> {
        private Context context;
        String name, calcheck = "true", add_cal;
        String  goingStatus, checkcaltext = "add", pdf_link;
        private ArrayList<MyEventsModalClass> eventList;

         public PastEventAdapter(Context context,ArrayList<MyEventsModalClass>eventList) {
            super();
            this.context = context;
            this.eventList=eventList;
        }

        @Override
        public PastEventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.main_event_list, parent, false);
            PastEventAdapter.ViewHolder viewHolder = new PastEventAdapter.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final PastEventAdapter.ViewHolder holder, final int position) {
                eventId = eventList.get(position).getEventId();
                holder.txtview_hostName.setText(eventList.get(position).getHostName());
                holder.txtview_eventName.setText(eventList.get(position).getEventName());
                eventUserId= eventList.get(position).getUserId();
                 eventdateRecieved = eventList.get(position).getEventDate();
                DateConvertor obj = new DateConvertor();
                final String eventdate = obj.changeDateFormatRecieved(eventdateRecieved);
                Log.e("datevalue", eventdate);
                holder.txtview_eventDate.setText(eventdate);
                String startTime = eventList.get(position).getEventStartTime();
                DateConvertor objstartdate = new DateConvertor();
                final String eventstarttime = obj.changeTimeFormatRecieve(startTime);
                Log.e("datevalues", eventstarttime);
                String endTime = eventList.get(position).getEventEndTime();
                DateConvertor objendtime = new DateConvertor();
                final String eventendtime = objendtime.changeTimeFormatRecieve(endTime);
                Log.e("datevaluess", eventendtime);
                holder.txtview_eventTime.setText(eventstarttime + " - " + eventendtime);
//                goingStatus=calendarListingResponseArrayList.get(position).getGoingStatus();
//            if (goingStatus.equalsIgnoreCase("1")) {
//                holder.btn_shareEvent.setBackgroundResource(R.drawable.button_back_disable);
//                holder.btn_shareEvent.setEnabled(false);
//            }
            add_cal=eventList.get(position).getCalCheck();

            if(add_cal.equalsIgnoreCase("1")){
                holder.btn_addCalendar.setText("Remove from Calendar");
            }
            else {
                holder.btn_addCalendar.setText("Add to Calendar");
            }
                holder.txtview_eventLoc.setText(eventList.get(position).getEventLocation());
                holder.event_detail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        eventId = eventList.get(position).getEventId();
                        if (context instanceof BasicActivity) {
                            EventDetailScreen fragment = new EventDetailScreen();
                            Bundle bundle = new Bundle();
                            bundle.putString("eventid", eventId); //key and value
                            bundle.putString("screen", "true");
                            fragment.setArguments(bundle);
                            ((BasicActivity) context).replaceFragment(fragment);
                        }
                    }

                });

                holder.btn_shareEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        shareEventName=eventList.get(position).getEventName();
                        shareEventLocation=eventList.get(position).getEventLocation();
                        shareEventDesc=eventList.get(position).getEventDescription();
                        shareEventStartTime=eventList.get(position).getEventStartTime();
                        eventdateRecieved=eventList.get(position).getEventDate();
                        DateConvertor obj = new DateConvertor();
                        shareeventstarttime = obj.changeTimeFormatRecieve(shareEventStartTime);
                        Log.e("datevalue", shareeventstarttime);

                        DateConvertor obj1 = new DateConvertor();
                        final String eventdate = obj1.changeDateFormatRecieved(eventdateRecieved);
                        Log.e("datevalues", eventdate);

                        shareEventDate=eventdate;
                        shareData();                    }
                });

                holder.btn_addCalendar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        eventId = eventList.get(position).getEventId();
                        if(holder.btn_addCalendar.getText().toString().trim().equalsIgnoreCase("Remove from calendar")){
                            removeCalendarAPI();
                            holder.btn_addCalendar.setText("Add to Calendar");

                        }
                        else {
                            addCalendarAPI();
                            holder.btn_addCalendar.setText("Remove from Calendar");
                        }


                    }
                });
            }
        @Override
        public int getItemCount() {
            return eventList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public TextView txtview_hostName,txtview_eventName,txtview_eventDate,txtview_eventLoc,txtview_eventTime;
            RelativeLayout event_detail;
            public Button btn_addCalendar,btn_shareEvent;

            public ViewHolder(View itemView) {
                super(itemView);
                txtview_hostName = (TextView) itemView.findViewById(R.id.tv_host_name);
                txtview_eventName = (TextView) itemView.findViewById(R.id.tv_event_name);
                txtview_eventDate = (TextView) itemView.findViewById(R.id.tv_event_date);
                txtview_eventLoc = (TextView) itemView.findViewById(R.id.tv_event_loc);
                txtview_eventTime = (TextView) itemView.findViewById(R.id.time_data_event);
                event_detail = (RelativeLayout) itemView.findViewById(R.id.click_layout);
                btn_addCalendar= (Button)itemView.findViewById(R.id.btn_cal_evnt);
                btn_shareEvent = (Button)itemView.findViewById(R.id.btn_tell_frnd);
            }
        }

    }

    private void shareData() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String VolEvent = shareEventName + " , " + shareEventDate + " , " + shareEventLocation
                + " , " + shareeventstarttime;
        intent.putExtra(Intent.EXTRA_TEXT, VolEvent);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        getActivity().startActivity(Intent.createChooser(intent, "Send Invite..."));
    }

    private void addCalendarAPI() {

        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<LoginResponse> userCall = apiInterface.add_to_calendar(appToken, access_token, eventId);
        userCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (isFragmentVisible) {
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());

                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }
//                    else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
//                        recyclerView.setVisibility(View.GONE);
//                        empty_data.setVisibility(View.VISIBLE);
//                    }
                    else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (isFragmentVisible) {

                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }

    private void removeCalendarAPI() {
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        String userid = SharedPrefsUtils.getStringPreference(getActivity(), "user_id");
        Log.e("userid",userid);
        Call<LoginResponse> userCall = apiInterface.remove_from_calendar(appToken, access_token, eventId);
        userCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (isFragmentVisible) {
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());

                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }
//                    else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
//                        recyclerView.setVisibility(View.GONE);
//                        empty_data.setVisibility(View.VISIBLE);
//                    }
                    else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (isFragmentVisible) {

                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }
}




