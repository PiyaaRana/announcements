package com.zoptal.announcements.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zoptal.announcements.R;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.base.MyApp;
import com.zoptal.announcements.eventBusClasses.CalendarDateSelectedEvent;
import com.zoptal.announcements.mainController.LoginActivity;
import com.zoptal.announcements.utils.CustomAutoCompleteTextView;
import com.zoptal.announcements.utils.PlaceJSONParser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;

public class MapFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    double latitude;
    double longitude;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker,mEventLocationMarker;
    LocationRequest mLocationRequest;
    private GoogleMap mMap;
    private int PROXIMITY_RADIUS = 10000;
    View view;
    private LatLng latLng1,latLng2,latLng3,latLng4,latLng5,latLng;
    private ArrayList<LatLng> getLocationMarker;
    private PlacesTask placesTask;
    private ParserTask parserTask;
    private CustomAutoCompleteTextView txtview_location;
    private TextView txtview_changeLocation,txtview_addToEvent;
    private MarkerOptions markerOptions1;
    private String EditCheck="true",locationAddressString;
    AlphaAnimation buttonClick;
    private Boolean isFragmentVisible=false;
    private MyActivity myActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view=inflater.inflate(R.layout.location_fragment,container,false);
            isFragmentVisible=true;

        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        buttonClick = new AlphaAnimation(1F, 0.5F);
        EventBus.getDefault().register(this);
//Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            getActivity().finish();
        } else {
        }
// Obtain the SupportMapFragment and get notified when the map is ready to be used.
        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
               mapFragment.getMapAsync(this);
        txtview_location=(CustomAutoCompleteTextView) view.findViewById(R.id.loc_myevent);
        txtview_location.setEnabled(false);
        txtview_changeLocation=(TextView)view.findViewById(R.id.tv_change_loc);
        txtview_addToEvent=(TextView)view.findViewById(R.id.tv_add_event);
        Bundle bundle= getArguments();
        if(bundle!=null){
            EditCheck= bundle.getString("editCheck");
        }
        if(EditCheck.equalsIgnoreCase("false")){
            txtview_location.setEnabled(false);
            txtview_location.setBackgroundColor(Color.TRANSPARENT);
            txtview_changeLocation.setVisibility(View.INVISIBLE);
            txtview_addToEvent.setVisibility(View.INVISIBLE);
            locationAddressString=((MyApp)getActivity().getApplicationContext()).getEventPlace();
            //txtview_location.setText(locationAddressString);
           latLng1=((MyApp)getActivity().getApplicationContext()).getEventLatlng();
           latitude=latLng1.latitude;
           longitude=latLng1.longitude;
        }
        if(EditCheck.equalsIgnoreCase("update")){
            txtview_changeLocation.setVisibility(View.INVISIBLE);
            txtview_addToEvent.setVisibility(View.VISIBLE);
            txtview_addToEvent.setText("Update Location");
            locationAddressString=((MyApp)getActivity().getApplicationContext()).getEventPlace();
            //txtview_location.setText(locationAddressString);
            latLng1=((MyApp)getActivity().getApplicationContext()).getEventLatlng();
            latitude=latLng1.latitude;
            longitude=latLng1.longitude;
        }

        txtview_location.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                txtview_location.showDropDown();
                return false;
            }
        });
        txtview_changeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(buttonClick);
                txtview_location.setText("");
                txtview_location.setEnabled(true);
                latLng1=null;
                if(mEventLocationMarker!=null) {
                    mEventLocationMarker.remove();
                }
              txtview_addToEvent.setVisibility(View.VISIBLE);
                txtview_changeLocation.setVisibility(View.INVISIBLE);
            }
        });
        txtview_addToEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(buttonClick);
                txtview_location.setEnabled(true);
            latLng1=  getLocationFromAddress(getActivity(),txtview_location.getText().toString().trim());
            //Log.e("addressss",""+latLng1);
            if(latLng1!=null){
                ((MyApp)getActivity().getApplicationContext()).setEventLatlng(latLng1);
                ((MyApp)getActivity().getApplicationContext()).setEventPlace(txtview_location.getText().toString().trim());
                 markerOptions1 = new MarkerOptions();

                     markerOptions1.position(latLng1);
                     markerOptions1.title("You are Here!");
                     markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.mipmap.location1));
                     mEventLocationMarker = mMap.addMarker(markerOptions1);
                     mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));
                     mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
                myActivity.showOneErrorDialog("Location updated successfully.", "OK", getActivity());
                 }
                 else {
                myActivity.showOneErrorDialog("Please choose the location again.", "OK", getActivity());
                txtview_location.setText("");
                ((MyApp)getActivity().getApplicationContext()).setEventLatlng(null);
                ((MyApp)getActivity().getApplicationContext()).setEventPlace("");
            }


            }
        });
        txtview_location.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        return view;
    }
    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(getActivity());
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        0).show();
            }
            return false;
        }
        return true;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }
    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }
    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
//Place current location marker
        if(EditCheck.equalsIgnoreCase("true")) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            latLng = new LatLng(location.getLatitude(), location.getLongitude());
            if(latLng!=null) {
                ((MyApp) getActivity().getApplicationContext()).setEventLatlng(latLng);
            }
        }else {
             latLng = latLng1;
        }
        MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("You are Here!");
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.location1));
            mCurrLocationMarker = mMap.addMarker(markerOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
      if(latitude !=0 && longitude !=0) {
          getCurrentAddressString(latitude, longitude);
      }
//        Toast.makeText(getActivity(), "Your Current Location",
//                Toast.LENGTH_LONG).show();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                    this);
        }

    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
        isFragmentVisible=false;

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    public void onEvent(CalendarDateSelectedEvent event){
        String currentDate = event.getCurrentDate();
        String showButtons= event.getShowButtons();
        if(currentDate.equalsIgnoreCase("true")){

        }
        else {
            txtview_location.setText("");


        }
        if(showButtons.equalsIgnoreCase("false")){
            txtview_changeLocation.setVisibility(View.GONE);
            txtview_addToEvent.setVisibility(View.GONE);
            txtview_location.setEnabled(false);
        }
        else if(showButtons.equalsIgnoreCase("true")){
            txtview_changeLocation.setVisibility(View.VISIBLE);
            txtview_addToEvent.setVisibility(View.GONE);
            txtview_location.setEnabled(true);

        }
        else {

        }
        //you can now set this date to view.
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(getActivity(), "permission denied",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuilder sb = new StringBuilder();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";
            // Obtain browser key from https://code.google.com/apis/console
            String key = "key=AIzaSyAJkq0m-PvkFWRyMs8NXEIdmkvfv0Bv3uY";
            String input = "";
            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            // place type to be searched
            String types = "types=geocode";
            // Sensor enabled
            String sensor = "sensor=false";
            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&" + key;
            // Output format
            String output = "json";
            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;
            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Creating ParserTask
            parserTask = new ParserTask();
            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }
    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {
                jObject = new JSONObject(jsonData[0]);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

                } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            String[] from = new String[]{"description","place_id"};
            int[] to = new int[]{android.R.id.text1};

            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);
            // Setting the adapter
            if(!(adapter.isEmpty()))
                txtview_location.setAdapter(adapter);
        }
    }
    @SuppressLint("LongLogTag")
    private void getCurrentAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                Log.e("postal",""+returnedAddress);
               StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                txtview_location.setText(strAdd);
                ((MyApp)getActivity().getApplicationContext()).setEventPlace(txtview_location.getText().toString().trim());

                Log.e("My Current loction address", strReturnedAddress.toString());
            } else {
                Log.e("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }

    }
    public void updateTextField(String newText){
        txtview_location.setText(newText);

    }
private void getLatLngValue(String placeId){
    Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId)
            .setResultCallback(new ResultCallback<PlaceBuffer>() {
                @Override
                public void onResult(PlaceBuffer places) {
                    if (places.getStatus().isSuccess()) {
                        final Place myPlace = places.get(0);
                        LatLng queriedLocation = myPlace.getLatLng();
                        Log.v("Latitude is", "" + queriedLocation.latitude);
                        Log.v("Longitude is", "" + queriedLocation.longitude);
                    }
                    places.release();
                }
            });
}

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null|| address.size()==0) {
                return null;
            }
else {
                Log.e("address",""+address);
                Address location = address.get(0);
                Log.e("addresss",""+address.get(0));
                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }

}


