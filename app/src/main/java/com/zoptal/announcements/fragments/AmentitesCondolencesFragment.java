package com.zoptal.announcements.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.zoptal.announcements.R;
import com.zoptal.announcements.activities.SearchAmenitiesActivity;
import com.zoptal.announcements.activities.SearchAmenitiesAndCondolencesActivity;
import com.zoptal.announcements.mainController.BasicActivity;

public class AmentitesCondolencesFragment extends Fragment implements View.OnClickListener {

    View view;
    private Boolean isFragmentVisible = false;
    private Double latitude, longitude;
    private String eventLocation;
    private TextView tv_flowers, tv_cars, tv_gifts, tv_hotels, tv_restaurants, tv_flights;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.amenities, container, false);
        isFragmentVisible=true;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        isFragmentVisible = true;
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        Bundle bundle = getArguments();
        if (bundle != null) {
            latitude = bundle.getDouble("lat", 0.0);
            longitude = bundle.getDouble("lon", 0.0);
            //Log.e("eventssssss", ""+latitude+"  "+""+longitude );
            eventLocation = bundle.getString("eventplace");
        }
        findViews();
        return view;
    }

    private void findViews() {
        tv_flowers = (TextView) view.findViewById(R.id.tv_flower);
        tv_gifts = (TextView) view.findViewById(R.id.tv_gift);
        tv_hotels = (TextView) view.findViewById(R.id.tv_hotels);
        tv_restaurants = (TextView) view.findViewById(R.id.tv_restaurant);
        tv_cars = (TextView) view.findViewById(R.id.tv_cars);
        tv_flights = (TextView) view.findViewById(R.id.tv_flights);

        tv_flowers.setOnClickListener(this);
        tv_gifts.setOnClickListener(this);
        tv_hotels.setOnClickListener(this);
        tv_flights.setOnClickListener(this);
        tv_restaurants.setOnClickListener(this);
        tv_cars.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_flower:
                Intent intent = new Intent(getActivity(), SearchAmenitiesAndCondolencesActivity.class);
                intent.putExtra("searchtype", "flowers");
                intent.putExtra("lat",latitude);
                intent.putExtra("lon",longitude); //key and value
                intent.putExtra("eventplace",eventLocation);
                startActivity(intent);
                break;
            case R.id.tv_gift:
                Intent intent1 = new Intent(getActivity(), SearchAmenitiesAndCondolencesActivity.class);
                intent1.putExtra("searchtype", "gifts");
                intent1.putExtra("lat",latitude);
                intent1.putExtra("lon",longitude); //key and value
                intent1.putExtra("eventplace",eventLocation);
                startActivity(intent1);
                break;
            case R.id.tv_hotels:
                Intent intent2 = new Intent(getActivity(), SearchAmenitiesAndCondolencesActivity.class);
                intent2.putExtra("searchtype", "hotels");
                intent2.putExtra("lat",latitude);
                intent2.putExtra("lon",longitude); //key and value
                intent2.putExtra("eventplace",eventLocation);
                startActivity(intent2);
                break;
            case R.id.tv_restaurant:
                Intent intent3 = new Intent(getActivity(), SearchAmenitiesAndCondolencesActivity.class);
                intent3.putExtra("searchtype", "restaurants");
                intent3.putExtra("lat",latitude);
                intent3.putExtra("lon",longitude); //key and value
                intent3.putExtra("eventplace",eventLocation);
                startActivity(intent3);
                break;
            case R.id.tv_cars:
                Intent intent4 = new Intent(getActivity(), SearchAmenitiesAndCondolencesActivity.class);
                intent4.putExtra("searchtype", "rental cars");
                intent4.putExtra("lat",latitude);
                intent4.putExtra("lon",longitude); //key and value
                intent4.putExtra("eventplace",eventLocation);
                startActivity(intent4);
                break;
            case R.id.tv_flights:
                Intent intent5 = new Intent(getActivity(), SearchAmenitiesActivity.class);
                intent5.putExtra("searchtype", "flights");
                startActivity(intent5);
                break;

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible=false;
    }
}
