package com.zoptal.announcements.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zoptal.announcements.R;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;

public class AnnouncementTypeFragment extends Fragment implements View.OnClickListener {

    View view;
    private TextView txtview_funeral, txtview_birth, txtview_wedding, txtview_retire, txtview_graduation, txtview_anniv, txtview_other;
    private EditText edt_otherEventName;
    private BasicActivity basicActivity;
    private LinearLayout layout_otherValue;
    private ImageView img_submittxt;
    private MyActivity myActivity;
    private String eventTypeValue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.announcement_type_fragment, container, false);
        basicActivity = (BasicActivity) getActivity();
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        findViews();

        return view;
    }

    private void findViews() {
        txtview_funeral = (TextView) view.findViewById(R.id.tv_funeral);
        txtview_birth = (TextView) view.findViewById(R.id.tv_birth);
        txtview_wedding = (TextView) view.findViewById(R.id.tv_wedding);
        txtview_retire = (TextView) view.findViewById(R.id.tv_retirement);
        txtview_graduation = (TextView) view.findViewById(R.id.tv_graduation);
        txtview_anniv = (TextView) view.findViewById(R.id.tv_anniversary);
        txtview_other = (TextView) view.findViewById(R.id.tv_other);
        edt_otherEventName = (EditText) view.findViewById(R.id.other_txt);
        layout_otherValue = (LinearLayout) view.findViewById(R.id.layout_other);
        img_submittxt = (ImageView) view.findViewById(R.id.sbmt_other_txt);
       edt_otherEventName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    img_submittxt.setVisibility(View.VISIBLE);
                }
                else {
                    img_submittxt.setVisibility(View.INVISIBLE);
                }

            }
        });

        txtview_funeral.setOnClickListener(this);
        txtview_birth.setOnClickListener(this);
        txtview_wedding.setOnClickListener(this);
        txtview_retire.setOnClickListener(this);
        txtview_graduation.setOnClickListener(this);
        txtview_anniv.setOnClickListener(this);
        txtview_other.setOnClickListener(this);
        img_submittxt.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_funeral:
                CreateEventFragment fragment = new CreateEventFragment();
                Bundle bundle = new Bundle();
                bundle.putString("eventtype", "Funeral"); //key and value
                fragment.setArguments(bundle);
                ((BasicActivity) getActivity()).replaceFragment(fragment);
                break;
            case R.id.tv_birth:
                CreateEventFragment fragment1 = new CreateEventFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putString("eventtype", "Birth"); //key and value
                fragment1.setArguments(bundle1);
                ((BasicActivity) getActivity()).replaceFragment(fragment1);
                break;
            case R.id.tv_wedding:
                CreateEventFragment fragment2 = new CreateEventFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putString("eventtype", "Wedding"); //key and value
                fragment2.setArguments(bundle2);
                ((BasicActivity) getActivity()).replaceFragment(fragment2);
                break;
            case R.id.tv_retirement:
                CreateEventFragment fragment3 = new CreateEventFragment();
                Bundle bundle3 = new Bundle();
                bundle3.putString("eventtype", "Retirement"); //key and value
                fragment3.setArguments(bundle3);
                ((BasicActivity) getActivity()).replaceFragment(fragment3);
                break;
            case R.id.tv_graduation:
                CreateEventFragment fragment4 = new CreateEventFragment();
                Bundle bundle4 = new Bundle();
                bundle4.putString("eventtype", "Graduation"); //key and value
                fragment4.setArguments(bundle4);
                ((BasicActivity) getActivity()).replaceFragment(fragment4);
                break;
            case R.id.tv_anniversary:
                CreateEventFragment fragment5 = new CreateEventFragment();
                Bundle bundle5 = new Bundle();
                bundle5.putString("eventtype", "Anniversary"); //key and value
                fragment5.setArguments(bundle5);
                ((BasicActivity) getActivity()).replaceFragment(fragment5);
                break;
            case R.id.tv_other:
                txtview_other.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more, 0);
                layout_otherValue.setVisibility(View.VISIBLE);
                break;
            case R.id.sbmt_other_txt:
                if (edt_otherEventName.getText().length() == 0) {
                    myActivity.showOneErrorDialog("Enter type of event.", "OK", getActivity());
                } else {
                    eventTypeValue = edt_otherEventName.getText().toString().trim();
                    layout_otherValue.setVisibility(View.GONE);
                    txtview_other.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_right, 0);
                    CreateEventFragment fragment6 = new CreateEventFragment();
                    Bundle bundle6 = new Bundle();
                    bundle6.putString("eventtype", eventTypeValue); //key and value
                    fragment6.setArguments(bundle6);
                    ((BasicActivity) getActivity()).replaceFragment(fragment6);
                }
                break;

        }

    }
}

