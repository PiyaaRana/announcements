package com.zoptal.announcements.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.base.DataHelperClass;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.LoginSignUpActivity;
import com.zoptal.announcements.modal.InboxModal;
import com.zoptal.announcements.responseClasses.CalendarEventResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.DateConvertor;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.Utils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchOutputFragment extends Fragment{
    View view;
    private RecyclerView recyclerView_search;
    private LinearLayoutManager layoutManager;
    private TextView txtview_empty,txtview_cal;
    private SearchAdapter searchAdapter;
      BasicActivity basicActivity;
    private String appToken = "f6fa0fa5041479b76322145vgh1g410975af7766",contact_no_values="",eventId,searchValue="",screenType,
    dateString;
    private Boolean isFragmentVisible=false;
    private RelativeLayout mainLayout;
    private ProgressBar progressBar;
    private MyActivity myActivity;
    private EditText edt_search;
    private ImageView img_SearchIcon;
    private ArrayList<CalendarEventResponse.CalendarEventDataModal> searchListingResponseArrayList = new ArrayList<>();

    private static final String CurrentDateTemplate = "yyyy-MM-dd";
    private Calendar _calendar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.search_fragment,container,false);
        isFragmentVisible=true;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        basicActivity=(BasicActivity)getActivity();
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        Bundle bundle= getArguments();
        if(bundle!=null){
            searchValue= bundle.getString("searchstring");
            screenType=bundle.getString("screentype");
        }
        _calendar= Calendar.getInstance();
        dateString=String.valueOf(DateFormat.format(CurrentDateTemplate, _calendar.getTime()));
        findViews();
        return view;
    }

    private void findViews(){
        txtview_empty=(TextView)view.findViewById(R.id.empty_data);
        mainLayout = (RelativeLayout) view.findViewById(R.id.content_frame);
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        recyclerView_search=(RecyclerView)view.findViewById(R.id.recycler_inbox);
        recyclerView_search.setHasFixedSize(true);
        layoutManager= new LinearLayoutManager(getActivity());
        recyclerView_search.setLayoutManager(layoutManager);
         getPhoneContacts();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible=false;
    }

    private void getPhoneContacts() {
        ArrayList<String> contacts = new ArrayList<>();
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.e("phonenumber", phoneNumber);
            if (phoneNumber.length() >= 10) {
                //String  result = phoneNumber.replaceAll("[^\\w\\s\\p{Ps}\\p{Pe}]","");
                String result = getOnlyDigits(phoneNumber);
                contacts.add(result);
                Log.e("phonenumberfinal", result);
                HashSet hs = new HashSet();
                hs.addAll(contacts); // demoArrayList= name of arrayList from which u want to remove duplicates
                contacts.clear();
                contacts.addAll(hs);
                for (int i = 0; i < contacts.size(); i++) {
                    if (i == 0) {
                        contact_no_values = contacts.get(i);
                        Log.e("selectedparentsss", contact_no_values);
                    } else {
                        contact_no_values = contact_no_values + "," + contacts.get(i);
                        Log.e("selectedparent", contact_no_values);
                    }
                }

            }
        }
        phones.close();
        if(screenType.equalsIgnoreCase("inbox")) {
            getInboxAPI();
        }
        else if(screenType.equalsIgnoreCase("myevents")){
         myEventAPI();
        }
        else {
            getPastEventsAPI();
        }
    }
    public static String getOnlyDigits(String s) {
        String number = "";
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        number = matcher.replaceAll("");
        if (number.length() > 10) {
            number = number.substring(number.length() - 10);
        }
        return number;
    }

    private void getInboxAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<CalendarEventResponse> userCall = apiInterface.inbox(appToken, access_token,contact_no_values,searchValue);
        userCall.enqueue(new Callback<CalendarEventResponse>() {
            @Override
            public void onResponse(Call<CalendarEventResponse> call, Response<CalendarEventResponse> response) {
                DataHelperClass.getInstance().setCalendarEventDataModal(response.body().getCalendarArrayList());
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);

                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {

                        searchListingResponseArrayList.clear();
                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
                            itemsListingResponseArrayList.add(itemsListingModal);
                        }*/
                        searchListingResponseArrayList = response.body().getCalendarArrayList();
                        if (searchListingResponseArrayList.size() > 0) {
                            searchAdapter = new SearchAdapter(getActivity());
                            recyclerView_search.setAdapter(searchAdapter);

                        } else {
                            recyclerView_search.setVisibility(View.GONE);
                            txtview_empty.setVisibility(View.VISIBLE);
                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    } else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
                        recyclerView_search.setVisibility(View.GONE);
                        txtview_empty.setVisibility(View.VISIBLE);
                    } else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<CalendarEventResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }

    private void myEventAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<CalendarEventResponse> userCall = apiInterface.my_events(appToken, access_token, dateString,"year",contact_no_values,searchValue);
        userCall.enqueue(new Callback<CalendarEventResponse>() {
            @Override
            public void onResponse(Call<CalendarEventResponse> call, Response<CalendarEventResponse> response) {
                DataHelperClass.getInstance().setCalendarEventDataModal(response.body().getCalendarArrayList());
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        searchListingResponseArrayList.clear();

                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
                            itemsListingResponseArrayList.add(itemsListingModal);
                        }*/
                        searchListingResponseArrayList = response.body().getCalendarArrayList();
                        if(searchListingResponseArrayList.size()>0){
                            searchAdapter = new SearchAdapter(getActivity());
                            recyclerView_search.setAdapter(searchAdapter);
                        }else {
                            txtview_empty.setVisibility(View.VISIBLE);
                            recyclerView_search.setVisibility(View.GONE);
                        }

//                        contactListingResponseArrayList.clear();
//                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
//                            itemsListingResponseArrayList.add(itemsListingModal);
//                        }*/
//                        contactListingResponseArrayList = response.body().getArrayList();
//                        if (contactListingResponseArrayList.size() > 0) {
//                            recyclerView.setVisibility(View.VISIBLE);
//                            tv_emptyData.setVisibility(View.GONE);
//                            cadapter = new ParentAdapter(getActivity());
//                            recyclerView.setAdapter(cadapter);
//                        } else {
//                            recyclerView.setVisibility(View.GONE);
//                            tv_emptyData.setVisibility(View.VISIBLE);
//                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    } else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {

                    } else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<CalendarEventResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }

    private void getPastEventsAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();
        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<CalendarEventResponse> userCall = apiInterface.received_event_invitations(appToken, access_token,"past",contact_no_values,searchValue);
        userCall.enqueue(new Callback<CalendarEventResponse>() {
            @Override
            public void onResponse(Call<CalendarEventResponse> call, Response<CalendarEventResponse> response) {
                DataHelperClass.getInstance().setCalendarEventDataModal(response.body().getCalendarArrayList());
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        searchListingResponseArrayList.clear();
                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
                            itemsListingResponseArrayList.add(itemsListingModal);
                        }*/
                        searchListingResponseArrayList = response.body().getCalendarArrayList();
                        if (searchListingResponseArrayList.size() > 0) {
                            searchAdapter = new SearchAdapter(getActivity());
                            recyclerView_search.setAdapter(searchAdapter);

                        } else {
                            recyclerView_search.setVisibility(View.GONE);
                            txtview_empty.setVisibility(View.VISIBLE);
                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    } else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
                        recyclerView_search.setVisibility(View.GONE);
                        txtview_empty.setVisibility(View.VISIBLE);
                    } else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<CalendarEventResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }


    public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder{
            public ImageView userImage;
            public TextView tv_userName,tv_eventName,tv_inboxTime,tv_inboxDate;
            public String imageName;
            private RelativeLayout click_layout;
            public MyViewHolder(View view){
                super(view);
                tv_userName=(TextView)view.findViewById(R.id.inbox_username);
                tv_eventName =(TextView)view.findViewById(R.id.inbox_event_name);
                tv_inboxTime =(TextView)view.findViewById(R.id.inbox_event_time);
                tv_inboxDate =(TextView)view.findViewById(R.id.inbox_event_date);
                click_layout=(RelativeLayout)view.findViewById(R.id.shift_items);
                userImage=(ImageView) view.findViewById(R.id.volEvent_img);

//                click_layout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent myactivity = new Intent(context.getApplicationContext(), EventDetailScreen.class);
//                        myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
//                        myactivity.putExtra("eventid",)
//                        context.getApplicationContext().startActivity(myactivity);
//                    }
//                });

            }
        }

        public SearchAdapter(Context context){

            this.context= context;
        }

        @Override
        public SearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inbox_list,parent,false);
            return new SearchAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(SearchAdapter.MyViewHolder holder, final int position) {
            holder.tv_userName.setText(searchListingResponseArrayList.get(position).getContactName());
            holder.tv_eventName.setText(searchListingResponseArrayList.get(position).getEventName());
            String dateString= searchListingResponseArrayList.get(position).getEventDate();
            DateConvertor obj = new DateConvertor();
            final String eventdate = obj.changeDateFormatRecieved(dateString);
            Log.e("datevalue", eventdate);
            holder.tv_inboxDate.setText(eventdate);
            String startTime= searchListingResponseArrayList.get(position).getStartTime();
            DateConvertor objstartdate = new DateConvertor();
            final String eventstarttime = obj.changeTimeFormatRecieve(startTime);
            Log.e("datevalues", eventstarttime);
            String endTime= searchListingResponseArrayList.get(position).getEndTime();
            DateConvertor objendtime = new DateConvertor();
            final String eventendtime = obj.changeTimeFormatRecieve(endTime);
            Log.e("datevaluess", eventdate);
            holder.tv_inboxTime.setText(eventstarttime+" - "+eventendtime);
            String imageUrl=searchListingResponseArrayList.get(position).getUserPic();
            if(imageUrl!=null && !(imageUrl.isEmpty())){
                Picasso.with(context).load(imageUrl).resize(215,215).into(holder.userImage);
            }
            else {
                Picasso.with(context).load(R.drawable.no_profile_grey_circle_bg).resize(215,215).into(holder.userImage);
            }
            holder.click_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    eventId= searchListingResponseArrayList.get(position).getId();
                    if (context instanceof BasicActivity) {
                        EventDetailScreen fragment = new EventDetailScreen();
                        Bundle bundle=new Bundle();
                        bundle.putString("eventid", eventId); //key and value
                        bundle.putString("screen", "false");
                        fragment.setArguments(bundle);
                        ((BasicActivity) context).replaceFragment(fragment);
                    }
                }
            });
        }

        public void removeAt(int position){
            searchListingResponseArrayList.remove(position);
            notifyItemRemoved(position);
        }
        @Override
        public int getItemCount() {
            return searchListingResponseArrayList.size();
        }
    }



}
