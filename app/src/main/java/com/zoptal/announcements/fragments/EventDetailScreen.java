package com.zoptal.announcements.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.activities.SearchAmenitiesActivity;
import com.zoptal.announcements.base.DataHelperClass;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.base.MyApp;
import com.zoptal.announcements.eventBusClasses.CalendarDateSelectedEvent;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.LoginSignUpActivity;
import com.zoptal.announcements.modal.ContactNameResponseModal;
import com.zoptal.announcements.responseClasses.CalendarEventResponse;
import com.zoptal.announcements.responseClasses.ContactsResponse;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.DateConvertor;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EventDetailScreen extends Fragment implements View.OnClickListener {
    View view;
    private View view_locOff, view_locOn, view_eventtypeOff, view_eventtypeOn, view_partysizeOff, view_partysizeOn, view_calendarOff, view_calendarOn;
    private ToggleButton tBtn_loc, tBtn_eventType, tBtn_partySize, tBtn_calendar;
    private RelativeLayout layout_location, layout_eventType, layout_partySize, layout_calendar,mainLayout;
    private RecyclerView recyclerView_contacts;
    private ContactsAdapter mAdapter;
    private ArrayList<ContactNameResponseModal> contactList;
    private LinearLayout ll_mapFrame, ll_calendarFrame;
    CalendarEventResponse calEventDataModal = new CalendarEventResponse();
    private ArrayList<CalendarEventResponse.CalendarEventDataModal> calendarListingResponseArrayList = new ArrayList<>();
    private String eventId,selectedEventId,eventName, eventLocation, eventDate, eventStartTime, eventEndTime,contactName, contactPhone, partySize, eventDesc,
            eventCategory, eventType,contact_no_values = "",selecttid,selectname,imageUrl,eventTypeCheck,inviteId,
            appToken="f6fa0fa5041479b76322145vgh1g410975af7766",markCheck="",userId,eventUserId;
    private Boolean isFragmentVisible=false;
    private TextView txtview_hostName,txtview_hostContact,txtview_eventDesc,txtview_screenHeading,txtview_loctext,txtview_caltext,
    txtview_eventType,txtview_partysize,txtview_going,txtview_notGoing,txtview_amenities;
    private ImageView hostPic,img_editIcon,img_delIcon;
    private LatLng latLng;
    double latitude,longitude;
    private EditText edt_qntynumber;
    private CheckBox chk_eventPublic,chk_eventPrivate;
    AlphaAnimation buttonClick;
    private MyActivity myActivity;
    private ArrayList<ContactsResponse.ContactsDataModal> contactListingResponseArrayList = new ArrayList<>();
    private BasicActivity basicActivity;
    private Calendar cDate;
    private Date dateValue;
    private static final String CurrentDateTemplate = "yyyy-MM-dd";
    private String currentDate,hostImage,screencheck;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.event_detail_screen, container, false);
        isFragmentVisible=true;
        contactList= new ArrayList<>();
        userId = SharedPrefsUtils.getStringPreference(getActivity(), "user_id");
        calEventDataModal.setCalendarArrayList(DataHelperClass.getInstance().getCalendarEventDataModal());
        calendarListingResponseArrayList= calEventDataModal.getCalendarArrayList();
        buttonClick = new AlphaAnimation(1F, 0.5F);
        basicActivity=(BasicActivity)getActivity();
        Bundle bundle = getArguments();
        if (bundle != null) {
            eventId = bundle.getString("eventid", "");
            screencheck = bundle.getString("screen", "");
            Log.e("eventssssss", eventId );
        }
        cDate = Calendar.getInstance();
        currentDate=String.valueOf(DateFormat.format(CurrentDateTemplate, cDate.getTime()));
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        findViews();
        recyclerInit();
        return view;
    }

    private void findViews() {
        view_locOff = (View) view.findViewById(R.id.view_loc_off);
        view_locOn = (View) view.findViewById(R.id.view_loc_on);
        view_partysizeOff = (View) view.findViewById(R.id.view_party_off);
        view_partysizeOn = (View) view.findViewById(R.id.view_party_on);
        view_calendarOff = (View) view.findViewById(R.id.view_cal_off);
        view_calendarOn = (View) view.findViewById(R.id.view_cal_on);
        tBtn_loc = (ToggleButton) view.findViewById(R.id.loc_toggle);
        img_editIcon=(ImageView)view.findViewById(R.id.edit_icon);
        tBtn_partySize = (ToggleButton) view.findViewById(R.id.party_toggle);
        tBtn_calendar = (ToggleButton) view.findViewById(R.id.calendar_toggle);
        layout_location = (RelativeLayout) view.findViewById(R.id.rl_location);
        layout_partySize = (RelativeLayout) view.findViewById(R.id.rl_party_size);
        layout_calendar = (RelativeLayout) view.findViewById(R.id.rl_calendar);
        ll_mapFrame = (LinearLayout) view.findViewById(R.id.map_content_frame);
        txtview_hostName=(TextView)view.findViewById(R.id.tv_host_name);
        txtview_hostContact=(TextView)view.findViewById(R.id.tv_contact_no);
        txtview_eventDesc=(TextView)view.findViewById(R.id.tv_event_desc);
        hostPic=(ImageView)view.findViewById(R.id.host_image);
        img_delIcon =(ImageView)view.findViewById(R.id.delete_icon);
        txtview_screenHeading=(TextView)view.findViewById(R.id.tv_screen_heading);
        txtview_loctext = (TextView) view.findViewById(R.id.loc_txt);
        txtview_caltext = (TextView) view.findViewById(R.id.cal_txt);
        txtview_going= (TextView) view.findViewById(R.id.tv_goingto_event);
        txtview_notGoing= (TextView) view.findViewById(R.id.tv_notgoing_event);
        txtview_eventType= (TextView) view.findViewById(R.id.event_txt);
        txtview_partysize = (TextView) view.findViewById(R.id.party_txt);
        edt_qntynumber = (EditText) view.findViewById(R.id.qty_text);
        txtview_amenities=(TextView) view.findViewById(R.id.tv_amenities_event);
        ll_calendarFrame = (LinearLayout) view.findViewById(R.id.calendar_content_frame);
        mainLayout= (RelativeLayout) view.findViewById(R.id.content_frame);
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        //getPhoneContacts();
        for (int i=0;i<calendarListingResponseArrayList.size();i++) {
            if (eventId.equalsIgnoreCase(calendarListingResponseArrayList.get(i).getId())) {
                eventUserId= calendarListingResponseArrayList.get(i).getUserId();
                Log.e("userid",userId+" "+eventUserId);
                if(userId.equalsIgnoreCase(eventUserId)){
                    img_editIcon.setVisibility(View.VISIBLE);
                }
                eventName = calendarListingResponseArrayList.get(i).getEventName();
                contactName = calendarListingResponseArrayList.get(i).getContactName();
                contactPhone = calendarListingResponseArrayList.get(i).getContactPhone();
                eventDesc = calendarListingResponseArrayList.get(i).getDescription();

                eventStartTime = calendarListingResponseArrayList.get(i).getStartTime();
                eventEndTime = calendarListingResponseArrayList.get(i).getEndTime();
                eventDate = calendarListingResponseArrayList.get(i).getEventDate();
                eventLocation = calendarListingResponseArrayList.get(i).getEventLocation();
                partySize = calendarListingResponseArrayList.get(i).getPartySize();
                eventTypeCheck = calendarListingResponseArrayList.get(i).getEventType();

                if(screencheck!=null && !screencheck.isEmpty()){
                    if(screencheck.equalsIgnoreCase("true")){
                        imageUrl=calendarListingResponseArrayList.get(i).getCreator_pic();
                        if (imageUrl != null && !imageUrl.isEmpty()) {
                            Picasso.with(getActivity()).load(imageUrl).resize(215, 215).into(hostPic);
                        } else {
                            Picasso.with(getActivity()).load(R.drawable.no_profile_grey_circle_bg).resize(215, 215).into(hostPic);
                        }
                    }
                    else {
                        imageUrl = calendarListingResponseArrayList.get(i).getUserPic();
                        if (imageUrl != null && !imageUrl.isEmpty()) {
                            Picasso.with(getActivity()).load(imageUrl).resize(215, 215).into(hostPic);
                        } else {
                            Picasso.with(getActivity()).load(R.drawable.no_profile_grey_circle_bg).resize(215, 215).into(hostPic);
                        }
                    }
                }

                markCheck= calendarListingResponseArrayList.get(i).getGoingStatus();
                if(markCheck.equalsIgnoreCase("1")){
                    txtview_going.setEnabled(false);
                    txtview_going.setBackgroundResource(R.drawable.button_back_disable);
                }
                else if(markCheck.equalsIgnoreCase("2")){
                    txtview_notGoing.setEnabled(false);
                    txtview_notGoing.setBackgroundResource(R.drawable.button_back_disable);
                }
                edt_qntynumber.setText(partySize);
                inviteId=calendarListingResponseArrayList.get(i).getInviteId();
                for (int l=0;l<calendarListingResponseArrayList.get(i).getContactDetails().size();l++) {
                    ContactNameResponseModal contactNameModel = new ContactNameResponseModal();
                    contactNameModel.setId(calendarListingResponseArrayList.get(i).getContactDetails().get(l).getContact_id());
                    contactNameModel.setUsername(calendarListingResponseArrayList.get(i).getContactDetails().get(l).getReceiverName());

                    contactNameModel.setPicture(calendarListingResponseArrayList.get(i).getContactDetails().get(l).getReceiverPic());
                    contactList.add(contactNameModel);
                }
                Boolean dateCheck=isEventExpired(eventDate);
                if(dateCheck){
                    txtview_amenities.setVisibility(View.GONE);
                    txtview_going.setVisibility(View.GONE);
                    txtview_notGoing.setVisibility(View.GONE);
                    if(userId.equalsIgnoreCase(eventUserId)) {
                        img_delIcon.setVisibility(View.VISIBLE);
                        img_editIcon.setVisibility(View.GONE);
                    }
                }
                DateConvertor obj = new DateConvertor();
                String eventdate = obj.changeDateFormatRecieved(eventDate);
                Log.e("datevalue", eventdate);
                eventDate = eventdate + "-" + " ";
                DateConvertor obj1 = new DateConvertor();
                String eventstarttime = obj1.changeTimeFormatRecieve(eventStartTime);
                Log.e("datevalues", eventstarttime);
                DateConvertor obj2 = new DateConvertor();
                String eventendtime = obj2.changeTimeFormatRecieve(eventEndTime);
                Log.e("datevaluess", eventendtime);
                latitude = Double.valueOf(calendarListingResponseArrayList.get(i).getEventLatitude());
                longitude = Double.valueOf(calendarListingResponseArrayList.get(i).getEventLongitude());
                latLng = new LatLng(latitude, longitude);
                ((MyApp) getActivity().getApplicationContext()).setEventLatlng(latLng);
                ((MyApp) getActivity().getApplicationContext()).setStartTime(eventStartTime);
                ((MyApp) getActivity().getApplicationContext()).setEndTime(eventEndTime);
                ((MyApp) getActivity().getApplicationContext()).setDateValue(eventdate);
                if (isFragmentVisible) {
                    txtview_hostName.setText(contactName);
                    txtview_hostContact.setText(contactPhone);
                    txtview_eventDesc.setText(eventDesc);
                    txtview_screenHeading.setText(eventName);
                    txtview_caltext.setText(eventdate + " " + eventstarttime + "-" + eventendtime);
                    txtview_loctext.setText(eventLocation);
                    txtview_partysize.setText(partySize);
                    txtview_eventType.setText(eventTypeCheck);

                }

            }
        }
        txtview_going.setOnClickListener(this);
        txtview_notGoing.setOnClickListener(this);
        tBtn_loc.setOnClickListener(this);
        tBtn_partySize.setOnClickListener(this);
        tBtn_calendar.setOnClickListener(this);
        txtview_amenities.setOnClickListener(this);
        img_editIcon.setOnClickListener(this);
        img_delIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loc_toggle:
                if (tBtn_loc.isChecked()) {
                    layout_location.setVisibility(View.VISIBLE);
                    view_locOn.setVisibility(View.VISIBLE);
                    view_locOff.setVisibility(View.GONE);
                    Fragment childFragment = new MapFragment();
                    Bundle args = new Bundle();
                    args.putString("editCheck", "false");
                    childFragment.setArguments(args);
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(R.id.map_content_frame, childFragment).commit();
                    onSelectedDayChanged("true","false");

                } else {
                    layout_location.setVisibility(View.GONE);
                    view_locOn.setVisibility(View.GONE);
                    view_locOff.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.party_toggle:
                if (tBtn_partySize.isChecked()) {
                    layout_partySize.setVisibility(View.VISIBLE);
                    view_partysizeOn.setVisibility(View.VISIBLE);
                    view_partysizeOff.setVisibility(View.GONE);
                    if(contactList.size()>0) {
                        mAdapter = new ContactsAdapter(getActivity(), contactList);
                        recyclerView_contacts.setAdapter(mAdapter);
                    }

                } else {
                    layout_partySize.setVisibility(View.GONE);
                    view_partysizeOn.setVisibility(View.GONE);
                    view_partysizeOff.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.calendar_toggle:
                if (tBtn_calendar.isChecked()) {
                    layout_calendar.setVisibility(View.VISIBLE);
                    view_calendarOn.setVisibility(View.VISIBLE);
                    view_calendarOff.setVisibility(View.GONE);
                    Fragment childFragment = new CustomCalendar();
                    Bundle args = new Bundle();
                    args.putString("editCheck", "false");
                    childFragment.setArguments(args);
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(R.id.calendar_content_frame, childFragment).commit();
                    onSelectedDayChanged("true","false");
                } else {
                    layout_calendar.setVisibility(View.GONE);
                    view_calendarOn.setVisibility(View.GONE);
                    view_calendarOff.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.tv_goingto_event:
                view.startAnimation(buttonClick);
                markCheck="1";
                markEventsAPI();
                break;
            case R.id.tv_notgoing_event:
                view.startAnimation(buttonClick);
                markCheck="2";
                markEventsAPI();
                break;
            case R.id.tv_amenities_event:
                AmentitesCondolencesFragment fragment = new AmentitesCondolencesFragment();
                Bundle bundle = new Bundle();
                bundle.putDouble("lat",latitude);
                bundle.putDouble("lon",longitude); //key and value
                bundle.putString("eventplace",eventLocation);
                fragment.setArguments(bundle);
                basicActivity.replaceFragment(fragment);
                break;
            case R.id.edit_icon:
                ReviewEventFragment fragment1 = new ReviewEventFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putString("eventid",eventId);
                fragment1.setArguments(bundle1);
                basicActivity.replaceFragment(fragment1);
                break;
            case R.id.delete_icon:
                deleteEventAPI();
                break;
        }
    }
    private void recyclerInit(){
        recyclerView_contacts=(RecyclerView)view.findViewById(R.id.recycler_contacts);
        recyclerView_contacts.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView_contacts.setLayoutManager(mLayoutManager);

    }

    private void getPhoneContacts() {
        ArrayList<String> contacts = new ArrayList<>();
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.e("phonenumber", phoneNumber);
            if (phoneNumber.length() >= 10) {
                //String  result = phoneNumber.replaceAll("[^\\w\\s\\p{Ps}\\p{Pe}]","");
                String result = getOnlyDigits(phoneNumber);
                contacts.add(result);
                Log.e("phonenumberfinal", result);
                HashSet hs = new HashSet();
                hs.addAll(contacts); // demoArrayList= name of arrayList from which u want to remove duplicates
                contacts.clear();
                contacts.addAll(hs);
                for (int i = 0; i < contacts.size(); i++) {
                    if (i == 0) {
                        contact_no_values = contacts.get(i);
                        Log.e("selectedparentsss", contact_no_values);
                    } else {
                        contact_no_values = contact_no_values + "," + contacts.get(i);
                        Log.e("selectedparent", contact_no_values);
                    }
                }

            }
        }
        phones.close();
        getContactsAPI();
    }

    public static String getOnlyDigits(String s) {
        String number = "";
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        number = matcher.replaceAll("");
        if (number.length() > 10) {
            number = number.substring(number.length() - 10);
        }
        return number;
    }

    private void getContactsAPI() {

        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<ContactsResponse> userCall = apiInterface.my_contacts(appToken, access_token, contact_no_values,eventId);
        userCall.enqueue(new Callback<ContactsResponse>() {
            @Override
            public void onResponse(Call<ContactsResponse> call, Response<ContactsResponse> response) {
                if (isFragmentVisible) {
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        contactListingResponseArrayList.clear();
//                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
//                            itemsListingResponseArrayList.add(itemsListingModal);
//                        }*/
                        contactListingResponseArrayList = response.body().getArrayList();
                        Log.e("contactsize",""+contactListingResponseArrayList.size());
                        mAdapter = new ContactsAdapter(getActivity(),contactList);
                        recyclerView_contacts.setAdapter(mAdapter);

                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }
//                    else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
//                        recyclerView.setVisibility(View.GONE);
//                        empty_data.setVisibility(View.VISIBLE);
//                    }
                    else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactsResponse> call, Throwable t) {
                if (isFragmentVisible) {

                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible=false;
    }

    private void onSelectedDayChanged(String dateSelected,String showButtons) {
        EventBus.getDefault().post(new CalendarDateSelectedEvent(dateSelected,showButtons));
    }

    public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> {

        private Context context;
        String contactUrl,invitedCheck;
        private ArrayList<ContactNameResponseModal>recyclerList;
        public class MyViewHolder extends RecyclerView.ViewHolder{
            public ImageView image;
            public TextView tv_imageName;
            public String imageName;
            public MyViewHolder(View view){
                super(view);
                image = (ImageView) view.findViewById(R.id.imageViewName);
                tv_imageName=(TextView)view.findViewById(R.id.image_name);

            }
        }

        public ContactsAdapter(Context context,ArrayList<ContactNameResponseModal>recyclerList){
            this.recyclerList=recyclerList;
            this.context= context;
        }

        @Override
        public ContactsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_images,parent,false);
            return new ContactsAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ContactsAdapter.MyViewHolder holder, int position) {
            ContactNameResponseModal responseModal= recyclerList.get(position);
            String name= responseModal.getUsername();
            Log.e("name",name);
                contactUrl = responseModal.getPicture();
                if (contactUrl != null && !contactUrl.isEmpty()) {
                    Picasso.with(getActivity()).load(contactUrl).resize(215, 215).into(holder.image);
                } else {
                    Picasso.with(getActivity()).load(R.drawable.no_profile_grey_circle_bg).resize(215, 215).into(holder.image);
                }
                holder.tv_imageName.setText(responseModal.getUsername());


//            invitedCheck = contactListingResponseArrayList.get(position).getInvited();
//            if (invitedCheck.equalsIgnoreCase("1")) {
//                contactUrl = contactListingResponseArrayList.get(position).getPicture();
//                if (contactUrl.isEmpty()) {
//                    Picasso.with(getActivity()).load(R.drawable.no_profile_grey_circle_bg).resize(215, 215).into(holder.image);
//                } else {
//                    Picasso.with(getActivity()).load(contactUrl).resize(215, 215).into(holder.image);
//                }
//
//                holder.tv_imageName.setText(contactListingResponseArrayList.get(position).getUsername());
//            }
        }

        public void removeAt(int position){
            recyclerList.remove(position);
            notifyDataSetChanged();
        }
        @Override
        public int getItemCount() {
            return recyclerList.size();
        }
    }
    private void markEventsAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<LoginResponse> userCall = apiInterface.mark_event_invitation(appToken, access_token, inviteId,markCheck);
        userCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                        if(markCheck.equalsIgnoreCase("1")){
                            txtview_going.setEnabled(false);
                            txtview_going.setBackgroundResource(R.drawable.button_back_disable);
                            txtview_notGoing.setEnabled(true);
                            txtview_notGoing.setBackgroundResource(R.drawable.button_back);
                        }
                        else if(markCheck.equalsIgnoreCase("2")){
                            txtview_notGoing.setEnabled(false);
                            txtview_notGoing.setBackgroundResource(R.drawable.button_back_disable);
                            txtview_going.setEnabled(true);
                            txtview_going.setBackgroundResource(R.drawable.button_back_green);
                        }


                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }
//                    else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
//                        recyclerView.setVisibility(View.GONE);
//                        empty_data.setVisibility(View.VISIBLE);
//                    }
                    else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }

    private boolean isEventExpired(String date){
        boolean isExpired=false;
        Log.e("dateValues",""+cDate.getTime());
        Date expiredDate = (Date)stringToDate(date);
        Log.e("dateValues",""+expiredDate);
        if (cDate.getTime().after(expiredDate)&& !((date).equalsIgnoreCase(currentDate))) isExpired=true;

        return isExpired;
    }

    public Date stringToDate(String dateString) {

        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss zz yyyy",
                Locale.US);
        Date dateObj = null,dateObjNew=null;
        try {
            dateObj = recivedFormat.parse(dateString);
            String newDateValue=outputFormat.format(dateObj);
            dateObjNew= outputFormat.parse(newDateValue);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        return dateObjNew;
    }

    private void deleteEventAPI(){
        progressBar.setVisibility(View.VISIBLE);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

    /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                 Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<LoginResponse> userCall = apiInterface.delete_event(appToken, access_token, eventId);
        userCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                        getActivity().onBackPressed();

                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }
//                    else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
//                        recyclerView.setVisibility(View.GONE);
//                        empty_data.setVisibility(View.VISIBLE);
//                    }
                    else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }



}

