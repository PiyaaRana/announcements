package com.zoptal.announcements.fragments;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.activities.SocialWebviewActivity;
import com.zoptal.announcements.adapter.ContactsAdapter;
import com.zoptal.announcements.adapter.CropingOption;
import com.zoptal.announcements.adapter.CropingOptionAdapter;
import com.zoptal.announcements.adapter.ProfileEventAdapter;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.LoginActivity;
import com.zoptal.announcements.mainController.LoginSignUpActivity;
import com.zoptal.announcements.modal.CalendarEventsModalClass;
import com.zoptal.announcements.modal.ContactNameModel;
import com.zoptal.announcements.modal.InboxModal;
import com.zoptal.announcements.modal.ProfileEventModal;
import com.zoptal.announcements.responseClasses.CalendarEventResponse;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.responseClasses.ProfileResponse;
import com.zoptal.announcements.responseClasses.SocialLinkResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.DateConvertor;
import com.zoptal.announcements.utils.PermissionUtility;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment implements View.OnClickListener{
    View view;
    private RecyclerView recyclerView_contacts,recyclerView_events;
    private ContactsAdapter mAdapter;
    private ProfileEventAdapter eAdapter;
    private List<ContactNameModel> contactList;
    private List<ProfileEventModal> eventList;
    private BasicActivity basicActivity;
    private ToggleButton toggle_EditProfile;
    private ImageView pic,cameraicon,fbIcon,twIcon,pinIcon,instaIcon,close_btn,img_close_btn;
    private EditText edt_email,edt_username,edt_contact,edt_profileLink,edt_location;
    private File outPutFile,selectedFile;
    private String userChoosenTask,fbUrlCheck="false",instaUrlCheck="false",pinUrlCheck="false",twitterUrlCheck="false";
    private Uri mImageCaptureUri;
    public static final int REQUEST_CAMERA=1;
    public static final int SELECT_FILE = 2,CROPING_CODE=301;
    private Bitmap bitmap;
    private String appToken = "f6fa0fa5041479b76322145vgh1g410975af7766",eventId,fbUrlLink="",instaUrlLink="",twUrlLink="",pinUrlLink="";
    private String myProfile="true",otherUserId;
    private RelativeLayout mainLayout;
    private ProgressBar progressBar;
    private static final String USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$";
    private ArrayList<ProfileResponse.ProfileEventDataModal> profileListingResponseArrayList = new ArrayList<>();
    private RelativeLayout ll_fb,ll_insta,ll_twitter,ll_pintrest,ll_add;
    private Dialog dialog,dialogAddLink;
    private TextView tv_linkText,tv_saveLink,empty_data;
    private MyActivity myActivity;
    private ImageView img_fbLink,img_twLink,img_instaLink,img_pinLink,img_closeFb,img_closeTw,img_closeInsta,img_closePin;
    private LinearLayout ll_social_link;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.profile_fragment,container,false);
        basicActivity=(BasicActivity)getActivity();
        contactList= new ArrayList<>();
        eventList= new ArrayList<>();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        outPutFile = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        Bundle bundle= getArguments();
        if(bundle!=null){
            myProfile= bundle.getString("myprofile");
            otherUserId= bundle.getString("otheruserid");
        }
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        findViews();
        recyclerInit();
        return view;
    }

private void findViews(){
    mainLayout=(RelativeLayout)view.findViewById(R.id.fl_content_frame);
    toggle_EditProfile = (ToggleButton) view.findViewById(R.id.tgl_edit_profile);
    pic = (ImageView) view.findViewById(R.id.profile_icon_usr);
    pic.setEnabled(false);
    cameraicon = (ImageView) view.findViewById(R.id.cameraicon);
    edt_username = (EditText) view.findViewById(R.id.edt_heading_usr);
    edt_username.setEnabled(false);
    edt_email = (EditText) view.findViewById(R.id.edt_email_profile);
    edt_email.setEnabled(false);
    edt_contact = (EditText) view.findViewById(R.id.edt_contact_profile);
    edt_contact.setEnabled(false);
    edt_location= (EditText) view.findViewById(R.id.edt_loc_profile);
    edt_location.setEnabled(false);
    ll_fb=(RelativeLayout) view.findViewById(R.id.fb_layout);
    ll_social_link=(LinearLayout)view.findViewById(R.id.ll_social_link) ;
    ll_twitter=(RelativeLayout) view.findViewById(R.id.tw_layout);
    ll_insta=(RelativeLayout) view.findViewById(R.id.insta_layout);
    ll_pintrest=(RelativeLayout) view.findViewById(R.id.pin_layout);
    ll_add=(RelativeLayout) view.findViewById(R.id.add_layout);
    img_fbLink= (ImageView) view.findViewById(R.id.fb_link);
    img_twLink= (ImageView) view.findViewById(R.id.tw_link);
    img_instaLink= (ImageView) view.findViewById(R.id.insta_link);
    img_pinLink= (ImageView) view.findViewById(R.id.pin_link);
    img_closeFb= (ImageView) view.findViewById(R.id.close_fb);
    img_closeInsta= (ImageView) view.findViewById(R.id.close_insta);
    img_closeTw= (ImageView) view.findViewById(R.id.close_tw);
    img_closePin= (ImageView) view.findViewById(R.id.close_pin);
    empty_data= (TextView) view.findViewById(R.id.empty_data);

    progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
    params.addRule(RelativeLayout.CENTER_IN_PARENT);
    mainLayout.addView(progressBar, params);
    progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
    if(myProfile.equalsIgnoreCase("true")) {
        getProfileAPI();
    }
    else {
    userProfileAPI();
    }
    toggle_EditProfile.setOnClickListener(this);
    pic.setOnClickListener(this);
    ll_add.setOnClickListener(this);
    img_fbLink.setOnClickListener(this);
    img_twLink.setOnClickListener(this);
    img_instaLink.setOnClickListener(this);
    img_pinLink.setOnClickListener(this);
    img_closeFb.setOnClickListener(this);
    img_closeInsta.setOnClickListener(this);
    img_closeTw.setOnClickListener(this);
    img_closePin.setOnClickListener(this);
}

    private void recyclerInit() {
//        recyclerView_contacts = (RecyclerView) view.findViewById(R.id.recycler_mylist_profile);
//        recyclerView_contacts.setHasFixedSize(true);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//        recyclerView_contacts.setLayoutManager(mLayoutManager);
//        mAdapter = new ContactsAdapter(getActivity(), contactList);
//        recyclerView_contacts.setAdapter(mAdapter);

        recyclerView_events = (RecyclerView) view.findViewById(R.id.recycler_events_profile);
        recyclerView_events.setHasFixedSize(true);
        RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getActivity()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView_events.setLayoutManager(eLayoutManager);
        if(myProfile.equalsIgnoreCase("false")) {
            disableViews();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tgl_edit_profile:
                if(toggle_EditProfile.isChecked()){
                    toggle_EditProfile.setText("Save Profile");
                    cameraicon.setVisibility(View.VISIBLE);
                    pic.setEnabled(true);
                    edt_username.requestFocus();
                    edt_username.setEnabled(true);
                    edt_username.setTextColor(Color.BLACK);
                    edt_email.setEnabled(true);
                    edt_email.setTextColor(Color.BLACK);
                    edt_contact.setEnabled(true);
                    edt_contact.setTextColor(Color.BLACK);
                    edt_location.setEnabled(true);
                    edt_location.setTextColor(Color.BLACK);
                    }
                    else {
                    toggle_EditProfile.setText("Edit Profile");
                    cameraicon.setVisibility(View.GONE);
                    pic.setEnabled(false);
                    edt_username.setEnabled(false);
                    edt_username.setTextColor(getResources().getColor(R.color.dropdown_clr));
                    edt_email.setEnabled(false);
                    edt_email.setTextColor(getResources().getColor(R.color.dropdown_clr));
                    edt_contact.setEnabled(false);
                    edt_contact.setTextColor(getResources().getColor(R.color.dropdown_clr));
                    edt_location.setEnabled(false);
                    edt_location.setTextColor(getResources().getColor(R.color.dropdown_clr));
                    if (!(edt_username.getText().toString().trim().matches(USERNAME_PATTERN))) {
                        myActivity.showOneErrorDialog("Username should contain lower-case alphabets,special characters,numbers only.", "OK", getActivity());
                        toggle_EditProfile.setText("Save Profile");
                        edt_username.requestFocus();
                        edt_username.setEnabled(true);
                        return;
                    }
                    else {
                        edt_username.setEnabled(false);
                        updateProfileAPI();
                    }
                }
                break;
            case R.id.profile_icon_usr:
                selectImage();
                break;
            case R.id.add_layout:
                    addDialog();
                break;
            case R.id.fb_link:
               Intent intent_fb= new Intent(getActivity(), SocialWebviewActivity.class);
                intent_fb.putExtra("url",fbUrlLink);
               getActivity().startActivity(intent_fb);
                //Intent intent= new Intent(Intent.ACTION_VIEW,Uri.parse(fbUrlLink));
//                startActivity(intent);
                break;
            case R.id.tw_link:
                Intent intent_tw= new Intent(getActivity(), SocialWebviewActivity.class);
                intent_tw.putExtra("url",twUrlLink);
                getActivity().startActivity(intent_tw);
                break;
            case R.id.insta_link:
                Intent intent_insta= new Intent(getActivity(), SocialWebviewActivity.class);
                intent_insta.putExtra("url",instaUrlLink);
                getActivity().startActivity(intent_insta);
                break;
            case R.id.pin_link:
                Intent intent_pin= new Intent(getActivity(), SocialWebviewActivity.class);
                intent_pin.putExtra("url",pinUrlLink);
                getActivity().startActivity(intent_pin);
                break;
            case R.id.close_fb:
                fbUrlLink="";
                saveSocialLinkAPI();
                break;
            case R.id.close_tw:
                twUrlLink="";
                saveSocialLinkAPI();
                break;
            case R.id.close_insta:
                instaUrlLink="";
                saveSocialLinkAPI();
                break;
            case R.id.close_pin:
                pinUrlLink="";
                saveSocialLinkAPI();
                break;

        }
    }

    public void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    userChoosenTask="Take Photo";
                    boolean result= PermissionUtility.checkPermissionCamera(getActivity());
                    //if(result)
                        cameraIntent();
                }
                else if (items[item].equals("Choose from Library")) {
                    userChoosenTask="Choose from Library";
                    boolean result=PermissionUtility.checkPermission(getActivity());
                    if(result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void cameraIntent()
    {
        File f = new File(Environment.getExternalStorageDirectory(), "temp1.jpg");
        mImageCaptureUri = Uri.fromFile(f);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent()
    {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_FILE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_FILE && resultCode == RESULT_OK && null != data) {
            mImageCaptureUri = data.getData();
            System.out.println("Gallery Image URI : "+mImageCaptureUri);
            CropingIMG();
        }
        else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            Log.e("CameraCheck",""+mImageCaptureUri);
            CropingIMG();
            System.out.println("Camera Image URI : "+mImageCaptureUri);
        }
        else if (requestCode == CROPING_CODE && resultCode == RESULT_OK) {
            try {
                if(outPutFile.exists()){
                    bitmap = decodeFile(outPutFile);
                    pic.setImageBitmap(bitmap);
                    getImageFile(bitmap);
                }
                else {
                    Toast.makeText(getActivity().getApplicationContext(), "Error while save image", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap decodeFile(File f) {
        try {
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 512;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    private void CropingIMG() {
        final ArrayList<CropingOption> cropOptions = new ArrayList<CropingOption>();
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");
        List<ResolveInfo> list =getActivity().getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(getActivity(), "Can't find image croping app", Toast.LENGTH_SHORT).show();
            return;
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", 512);
            intent.putExtra("outputY", 512);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scaleUpIfNeeded", true);

            //TODO: don't use return-data tag because it's not return large image data and crash not given any message
            //intent.putExtra("return-data", true);

            //Create output file here
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outPutFile));

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = (ResolveInfo) list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                startActivityForResult(i, CROPING_CODE);
            } else {
                for (ResolveInfo res : list) {
                    final CropingOption co = new CropingOption();

                    co.title = getActivity().getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
                    co.icon = getActivity().getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);
                    co.appIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    cropOptions.add(co);
                }

                CropingOptionAdapter adapter = new CropingOptionAdapter(getActivity(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose Croping App");
                builder.setCancelable(false);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        startActivityForResult(cropOptions.get(item).appIntent, CROPING_CODE);
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if (mImageCaptureUri != null) {
                            getActivity().getContentResolver().delete(mImageCaptureUri, null, null);
                            mImageCaptureUri = null;
                        }
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
private void getImageFile(Bitmap bitmap){
        try {
            File destination = new File(getActivity().getCacheDir(),
                    System.currentTimeMillis() + ".jpg");
            destination.createNewFile();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 95 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            FileOutputStream fos = new FileOutputStream(destination);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            selectedFile = destination;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void addDialog() {
    dialog = new Dialog(getActivity(), android.R.style.Theme_Translucent) {
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            // Tap anywhere to close dialog.
            return true;
        }
    };
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.addlink_dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        fbIcon = (ImageView) dialog.findViewById(R.id.img_fb_icon);
        twIcon = (ImageView) dialog.findViewById(R.id.img_tw_icon);
        instaIcon = (ImageView) dialog.findViewById(R.id.img_insta_icon);
        pinIcon = (ImageView) dialog.findViewById(R.id.img_pin_icon);
        close_btn=(ImageView)dialog.findViewById(R.id.close_btn);

        if(fbUrlLink!=null && !fbUrlLink.isEmpty()){
            fbIcon.setVisibility(View.INVISIBLE);
        }
        if(instaUrlLink!=null && !instaUrlLink.isEmpty()){
            instaIcon.setVisibility(View.INVISIBLE);
        }
        if(pinUrlLink!=null && !pinUrlLink.isEmpty()){
            pinIcon.setVisibility(View.INVISIBLE);
        }
        if(twUrlLink!=null && !twUrlLink.isEmpty()){
            twIcon.setVisibility(View.INVISIBLE);
        }
        dialog.show();
        close_btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dialog.dismiss();

        }
    });
        fbIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbUrlCheck="true";
                addDialogLink();
               }
        });
        twIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                twitterUrlCheck="true";
                addDialogLink();
                            }
        });
        pinIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinUrlCheck="true";
                addDialogLink();
            }
        });
        instaIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                instaUrlCheck="true";
                addDialogLink();
            }
        });
}

    public void addDialogLink() {
        dialogAddLink = new Dialog(getActivity(), android.R.style.Theme_Translucent) {
            @Override
            public boolean onTouchEvent(MotionEvent event) {
                // Tap anywhere to close dialog.
                return true;
            }
        };
        dialogAddLink.setCanceledOnTouchOutside(true);
        dialogAddLink.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAddLink.setCancelable(true);
        dialogAddLink.setContentView(R.layout.social_link_addurl_dialog);
        dialogAddLink.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        tv_linkText = (TextView) dialogAddLink.findViewById(R.id.link_txt);
        edt_profileLink = (EditText) dialogAddLink.findViewById(R.id.edt_profile_url);
        img_close_btn=(ImageView)dialogAddLink.findViewById(R.id.img_close_btn);
       tv_saveLink = (TextView) dialogAddLink.findViewById(R.id.tv_save_link);
        if(fbUrlCheck.equalsIgnoreCase("true")){
            tv_linkText.setText("Enter your facebook profile link:");
            edt_profileLink.setHint("https://www.facebook.com/username");

        }
         if(instaUrlCheck.equalsIgnoreCase("true")){
            tv_linkText.setText("Enter your instagram profile link:");
            edt_profileLink.setHint("https://www.instagram.com/username");
        }
         if(twitterUrlCheck.equalsIgnoreCase("true")){
            tv_linkText.setText("Enter your twitter profile link:");
            edt_profileLink.setHint("https://www.twitter.com/username");
        }
        if(pinUrlCheck.equalsIgnoreCase("true")){
            tv_linkText.setText("Enter your pinterest profile link:");
            edt_profileLink.setHint("https://www.pinterest.com/username");
        }
        dialogAddLink.show();
        img_close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddLink.dismiss();
            }
        });
        tv_saveLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddLink.dismiss();
                if(fbUrlCheck.equalsIgnoreCase("true")){
                    fbUrlLink=edt_profileLink.getText().toString().trim();
                    if(fbUrlLink.contains("https://www.facebook.com/")) {
                        fbIcon.setVisibility(View.GONE);
                        saveSocialLinkAPI();
                    }
                    else {
                        myActivity.showOneErrorDialog("Please enter a valid link!", "OK", getActivity());
                    }

                }
                else if(instaUrlCheck.equalsIgnoreCase("true")){
                    instaUrlLink=edt_profileLink.getText().toString().trim();
                    if(instaUrlLink.contains("https://www.instagram.com/")) {
                    instaIcon.setVisibility(View.GONE);
                        saveSocialLinkAPI();
                    }
                    else {
                        myActivity.showOneErrorDialog("Please enter a valid link!", "OK", getActivity());
                    }

                }
                else if(twitterUrlCheck.equalsIgnoreCase("true")){
                    twUrlLink=edt_profileLink.getText().toString().trim();
                    if(twUrlLink.contains("https://www.twitter.com/")) {
                        twIcon.setVisibility(View.GONE);
                        saveSocialLinkAPI();
                    }
                    else {
                        myActivity.showOneErrorDialog("Please enter a valid link!", "OK", getActivity());
                    }
                }
                else if(pinUrlCheck.equalsIgnoreCase("true")) {
                    pinUrlLink = edt_profileLink.getText().toString().trim();
                    if (pinUrlLink.contains("https://www.pinterest.com/")) {
                        pinIcon.setVisibility(View.GONE);
                        saveSocialLinkAPI();
                    } else {
                        myActivity.showOneErrorDialog("Please enter a valid link!", "OK", getActivity());
                    }
                }
                else {

                }
            }
        });

    }

    private void disableViews(){
        edt_username.setEnabled(false);
        edt_contact.setEnabled(false);
        edt_email.setEnabled(false);
        edt_location.setEnabled(false);
        recyclerView_events.setEnabled(false);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.INVISIBLE);
        ll_add.setEnabled(false);
        img_closeFb.setVisibility(View.INVISIBLE);
        img_closePin.setVisibility(View.INVISIBLE);
        img_closeTw.setVisibility(View.INVISIBLE);
        img_closeInsta.setVisibility(View.INVISIBLE);
        toggle_EditProfile.setVisibility(View.GONE);

    }

    private void getProfileAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        // prepare call in Retrofit 2.0
        try {
       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
            Call<ProfileResponse> userCall = apiInterface.my_profile(appToken,access_token);
            userCall.enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    //Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        Toast.makeText(getActivity().getApplicationContext(), "An error occurred, Please try again", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body().isResult()) {
                        profileListingResponseArrayList.clear();
                        String imageurl= response.body().getProfileDataModal().getUserPic();
                        if (imageurl != null && !imageurl.isEmpty()) {
                            Picasso.with(getActivity()).load(imageurl).resize(512, 512).into(pic);
                        }
                        else {
                            Picasso.with(getActivity()).load(R.drawable.no_profile_grey_circle_bg).resize(512, 512).into(pic);
                        }
                        edt_username.setText(response.body().getProfileDataModal().getUsername());
                        edt_email.setText(response.body().getProfileDataModal().getUserEmail());
                        edt_contact.setText(response.body().getProfileDataModal().getUserContact());
                        edt_location.setText(response.body().getProfileDataModal().getUserLoc());
                       String fbUrl=response.body().getProfileDataModal().getFbLink();
                        String twitterUrl=response.body().getProfileDataModal().getTwLink();
                        String instaUrl=response.body().getProfileDataModal().getInstaLink();
                        String pinUrl=response.body().getProfileDataModal().getPinLink();
                        if(fbUrl!=null && !fbUrl.isEmpty()){
                           ll_fb.setVisibility(View.VISIBLE);
                           fbUrlLink=fbUrl;
                        }
                        if(twitterUrl!=null && !twitterUrl.isEmpty()){
                            ll_twitter.setVisibility(View.VISIBLE);
                            twUrlLink=twitterUrl;
                        }
                        if(instaUrl!=null && !instaUrl.isEmpty()){
                            ll_insta.setVisibility(View.VISIBLE);
                            instaUrlLink=instaUrl;
                        }
                        if(pinUrl!=null && !pinUrl.isEmpty()){
                            ll_pintrest.setVisibility(View.VISIBLE);
                            pinUrlLink=pinUrl;
                        }
                        profileListingResponseArrayList = response.body().getProfileDataModal().getProfileEventDataModal();
                        if(profileListingResponseArrayList.size()>0){
                            eAdapter = new ProfileEventAdapter(getActivity());
                            recyclerView_events.setAdapter(eAdapter);
                        }
                        else {
                            empty_data.setVisibility(View.VISIBLE);
                            recyclerView_events.setVisibility(View.GONE);
                        }

                    }
                    else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }else {
                        Toast.makeText(getActivity().getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //showAlert(response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ProfileResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    // Utils.disableEnableControls(true, mainLayout);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateProfileAPI() {
                    progressBar.setVisibility(View.VISIBLE);
            // Utils.disableEnableControls(false, mainLayout);
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .addInterceptor(RestClient.getClient())
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(WebApiUrls.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                    .build();

            RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
            String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");

            RequestBody reqFile;
            if (selectedFile == null) {
                reqFile = null;
            } else {
                reqFile = RequestBody.create(MediaType.parse("image/*"), selectedFile);
            }

            RequestBody fullname = RequestBody.create(MediaType.parse("text/plain"), edt_username.getText().toString().trim());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), edt_email.getText().toString().trim());
            RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), edt_contact.getText().toString().trim());
            RequestBody access_tokens = RequestBody.create(MediaType.parse("text/plain"), access_token);
            RequestBody appTokenProfile = RequestBody.create(MediaType.parse("text/plain"), appToken);
            RequestBody location = RequestBody.create(MediaType.parse("text/plain"), edt_location.getText().toString().trim());
            Call<ProfileResponse> userCall = apiInterface.update_profile(appTokenProfile, access_tokens,fullname, location, email, phone, reqFile);
            userCall.enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    //Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        Toast.makeText(getActivity().getApplicationContext(), "An error occurred, Please try again", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body().isResult()) {
                        Toast.makeText(getActivity().getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //showAlert(response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ProfileResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    // Utils.disableEnableControls(true, mainLayout);
                }
            });

    }

    public class ProfileEventAdapter extends RecyclerView.Adapter<ProfileEventAdapter.MyViewHolder> {
        private Context context;
        private ArrayList<CalendarEventsModalClass>calendarList;

        public class MyViewHolder extends RecyclerView.ViewHolder{
            public ImageView userImage;
            public TextView tv_userName,tv_eventName,tv_inboxTime,tv_inboxDate,tv_locName;
            public String imageName;
            private RelativeLayout click_layout;
            private LinearLayout locLayout;

            public MyViewHolder(View view){
                super(view);
                tv_eventName =(TextView)view.findViewById(R.id.profile_event_name);
                tv_inboxTime =(TextView)view.findViewById(R.id.profile_event_time);
                click_layout=(RelativeLayout)view.findViewById(R.id.shift_items);
                tv_locName =(TextView)view.findViewById(R.id.profile_event_loc);

            }
        }

        public ProfileEventAdapter(Context context){
            this.context= context;
        }

        @Override
        public ProfileEventAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_events_list,parent,false);
            return new ProfileEventAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ProfileEventAdapter.MyViewHolder holder, final int position) {
                holder.tv_eventName.setText(profileListingResponseArrayList.get(position).getEventName());
                holder.tv_locName.setText(profileListingResponseArrayList.get(position).getEventLocation());
                String eventdateRecieved = profileListingResponseArrayList.get(position).getEventDate();
                DateConvertor obj = new DateConvertor();
                final String eventdate = obj.changeDateFormatProfile(eventdateRecieved);
                Log.e("datevalue", eventdate);
                holder.tv_inboxTime.setText(eventdate);
//                holder.click_layout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        eventId = profileListingResponseArrayList.get(position).getId();
//                        if (context instanceof BasicActivity) {
//                            EventDetailScreen fragment = new EventDetailScreen();
//                            Bundle bundle = new Bundle();
//                            bundle.putString("eventid", eventId); //key and value
//                            fragment.setArguments(bundle);
//                            ((BasicActivity) context).replaceFragment(fragment);
//                        }
//                    }
//
//                });
            }
        public void removeAt(int position){
            profileListingResponseArrayList.remove(position);
            notifyDataSetChanged();
        }
        @Override
        public int getItemCount() {
            return profileListingResponseArrayList.size();
        }
    }

    private void saveSocialLinkAPI() {
        // Utils.disableEnableControls(false, mainLayout);
        progressBar.setVisibility(View.VISIBLE);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0
        try {
       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
            String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
            Log.e("param",access_token+" "+fbUrlLink+" "+twUrlLink+" "+pinUrlLink+" "+instaUrlLink);
            Call<SocialLinkResponse> userCall = apiInterface.update_social_links(appToken,access_token,fbUrlLink,twUrlLink,pinUrlLink,instaUrlLink);
            userCall.enqueue(new Callback<SocialLinkResponse>() {
                @Override
                public void onResponse(Call<SocialLinkResponse> call, Response<SocialLinkResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    //Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        Toast.makeText(getActivity(), "An error occurred, Please try again", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body().isResult()) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        fbUrlCheck="false";instaUrlCheck="false";pinUrlCheck="false";twitterUrlCheck="false";
                        String fbLink= response.body().getSocialDataModal().getFb_link();
                        String instaLink= response.body().getSocialDataModal().getInsta_link();
                        String pinLink= response.body().getSocialDataModal().getPin_link();
                        String twLink= response.body().getSocialDataModal().getTwitter_link();
                        if(fbLink!=null && !fbLink.isEmpty()){
                            ll_fb.setVisibility(View.VISIBLE);
                            fbUrlLink=fbLink;
                        }
                        else {
                            ll_fb.setVisibility(View.GONE);
                            fbUrlLink=fbLink;
                        }
                        if(instaLink!=null && !instaLink.isEmpty()){
                            ll_insta.setVisibility(View.VISIBLE);
                            instaUrlLink=instaLink;
                        }
                        else {
                            ll_insta.setVisibility(View.GONE);

                            instaUrlLink=instaLink;
                        }
                        if(pinLink!=null && !pinLink.isEmpty()){
                            ll_pintrest.setVisibility(View.VISIBLE);

                            pinUrlLink=pinLink;
                        }
                        else {
                            ll_pintrest.setVisibility(View.GONE);
                            pinUrlLink=pinLink;;


                        }
                        if(twLink!=null && !twLink.isEmpty()){
                            ll_twitter.setVisibility(View.VISIBLE);

                            twUrlLink=twLink;
                        }
                        else {
                            ll_twitter.setVisibility(View.GONE);
                            twUrlLink=twLink;

                        }


                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //showAlert(response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<SocialLinkResponse> call, Throwable t) {

                    // Utils.disableEnableControls(true, mainLayout);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userProfileAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        // prepare call in Retrofit 2.0
        try {
       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
       Log.e("data....",access_token+" "+otherUserId);
            Call<ProfileResponse> userCall = apiInterface.user_profile(appToken,access_token,otherUserId);
            userCall.enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    //Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        Toast.makeText(getActivity().getApplicationContext(), "An error occurred, Please try again", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body().isResult()) {
                        profileListingResponseArrayList.clear();
                        String imageurl = response.body().getProfileDataModal().getUserPic();
                        if (imageurl != null && !imageurl.isEmpty()) {
                            Picasso.with(getActivity()).load(imageurl).into(pic);
                        } else {
                            Picasso.with(getActivity()).load(R.drawable.no_profile_grey_circle_bg).resize(512, 512).into(pic);
                        }
                        edt_username.setText(response.body().getProfileDataModal().getUsername());
                        edt_email.setText(response.body().getProfileDataModal().getUserEmail());


                        String fbUrl = response.body().getProfileDataModal().getFbLink();
                        String twitterUrl = response.body().getProfileDataModal().getTwLink();
                        String instaUrl = response.body().getProfileDataModal().getInstaLink();
                        String pinUrl = response.body().getProfileDataModal().getPinLink();
                        if (fbUrl != null && !fbUrl.isEmpty()) {

                            ll_fb.setVisibility(View.VISIBLE);
                            fbUrlLink = fbUrl;
                        }
                        if (twitterUrl != null && !twitterUrl.isEmpty()) {
                            ll_twitter.setVisibility(View.VISIBLE);
                            twUrlLink = twitterUrl;
                        }
                        if (instaUrl != null && !instaUrl.isEmpty()) {
                            ll_insta.setVisibility(View.VISIBLE);
                            instaUrlLink = instaUrl;
                        }
                        if (pinUrl != null && !pinUrl.isEmpty()) {
                            ll_pintrest.setVisibility(View.VISIBLE);
                            pinUrlLink = pinUrl;
                        }
                        if (response.body().getSettingsShowDataModal().getShowLocation().equalsIgnoreCase("0")) {
                            edt_location.setText("");
                        } else {
                            edt_location.setText(response.body().getProfileDataModal().getUserLoc());
                        }
                        if (response.body().getSettingsShowDataModal().getShowLinks().equalsIgnoreCase("0")) {
                            ll_social_link.setVisibility(View.GONE);
                        } else {
                            ll_social_link.setVisibility(View.VISIBLE);
                        }
                        if (response.body().getSettingsShowDataModal().getShowContact().equalsIgnoreCase("0")) {
                            edt_contact.setText("");
                        } else {
                            edt_contact.setText(response.body().getProfileDataModal().getUserContact());
                        }
                        if (response.body().getSettingsShowDataModal().getShowCheckins().equalsIgnoreCase("0")) {
                            ll_add.setVisibility(View.INVISIBLE);
                        } else {
                            profileListingResponseArrayList = response.body().getProfileDataModal().getProfileEventDataModal();

                            if (profileListingResponseArrayList.size() > 0) {
                                eAdapter = new ProfileEventAdapter(getActivity());
                                recyclerView_events.setAdapter(eAdapter);
                            }

                        }
                    }else {
                        Toast.makeText(getActivity().getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //showAlert(response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ProfileResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    // Utils.disableEnableControls(true, mainLayout);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


