package com.zoptal.announcements.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import com.zoptal.announcements.R;
import com.zoptal.announcements.base.MyApp;
import com.zoptal.announcements.mainController.BasicActivity;

public class HomeFragment extends Fragment {
    View view;
    private TextView txtview_createEvent,txtview_awaitingEvent;
    private BasicActivity basicActivity;
    private String arrowVisibilityCheck="false";
    AlphaAnimation buttonClick;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.home_fragment,container,false);
        basicActivity = (BasicActivity) getActivity();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        buttonClick = new AlphaAnimation(1F, 0.5F);
        txtview_createEvent=(TextView)view.findViewById(R.id.tv_create_event);
        txtview_awaitingEvent=(TextView)view.findViewById(R.id.tv_awaiting_event);
            txtview_createEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(buttonClick);
              basicActivity.replaceFragment(new AnnouncementTypeFragment());
                ((MyApp)getActivity().getApplicationContext()).setEventPlace("");
                ((MyApp)getActivity().getApplicationContext()).setEventLatlng(null);
                ((MyApp)getActivity().getApplicationContext()).setDateValue("");
                ((MyApp)getActivity().getApplicationContext()).setStartTime("");
                ((MyApp)getActivity().getApplicationContext()).setEndTime("");
            }
        });

        txtview_awaitingEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(buttonClick);
                basicActivity.replaceFragment(new AwaitingEventsFragment());
            }
        });
        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (fm.getBackStackEntryCount() == 0) {
            BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.INVISIBLE);
        } else if(fm.getBackStackEntryCount()> 0) {
            BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        }
        else {

        }
        return view;
    }
}
