package com.zoptal.announcements.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.Toast;

import com.zoptal.announcements.R;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.responseClasses.ChangePasswordResponse;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ChangePasswordFragment extends Fragment {
    private View view;
    private EditText edt_newpassword,edt_oldpassword,edt_cnfrmpassword;
    private MyActivity myActivity;
    private String appToken = "f6fa0fa5041479b76322145vgh1g410975af7766";
    AlphaAnimation buttonClick;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.change_password,container,false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Log.e("acesstoken",access_token);
        buttonClick = new AlphaAnimation(1F, 0.5F);
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        findViews();
        view.findViewById(R.id.tv_changepswd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(buttonClick);
                checkValidation();
            }
        });

        return view;
    }

    private void findViews(){

        edt_oldpassword=(EditText)view.findViewById(R.id.edt_old_pswd);
        edt_newpassword =(EditText)view.findViewById(R.id.edt_new_pswd);
        edt_cnfrmpassword =(EditText)view.findViewById(R.id.edt_cnfrm_pswrd);

    }


    private void checkValidation(){
        if (edt_oldpassword.getText().length() == 0) {
            myActivity.showOneErrorDialog("Password should be minimum 6 characters long.","OK",getActivity());
            edt_oldpassword.setFocusable(true);
            return;
        }
        if (edt_newpassword.getText().length() == 0) {
            myActivity.showOneErrorDialog("Enter new password", "OK", getActivity());
            edt_newpassword.setFocusable(true);
            return;
        }

        if (!edt_cnfrmpassword.getText().toString().trim().equals(edt_newpassword.getText().toString().trim())) {
            myActivity.showOneErrorDialog("Password doesn't match", "OK", getActivity());
            edt_cnfrmpassword.setFocusable(true);
            return;
        }
        ChangePasswordAPI();
}

    private void ChangePasswordAPI() {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0
        try {
       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
            String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
            Call<ChangePasswordResponse> userCall = apiInterface.change_password(appToken,access_token, edt_oldpassword.getText().toString().trim(), edt_newpassword.getText().toString().trim());
            userCall.enqueue(new Callback<ChangePasswordResponse>() {
                @Override
                public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {

                    //Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        Toast.makeText(getActivity(), "An error occurred, Please try again", Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().isResult()) {
                        SharedPrefsUtils.setStringPreference(getActivity(), "access_token", response.body().getAccessToken());
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        edt_oldpassword.setText("");
                        edt_newpassword.setText("");
                        edt_cnfrmpassword.setText("");
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //showAlert(response.body().getMessage());
                    }
                }
                @Override
                public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {

                    // Utils.disableEnableControls(true, mainLayout);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
