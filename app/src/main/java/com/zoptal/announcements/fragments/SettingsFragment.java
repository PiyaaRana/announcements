package com.zoptal.announcements.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.zoptal.announcements.R;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.LoginActivity;
import com.zoptal.announcements.responseClasses.SettingResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.Utils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SettingsFragment extends Fragment implements View.OnClickListener {
    View view;
    private ToggleButton calndr_toggle, mycontact_toggle, checkin_toggle, loc_toggle, phone_toggle, notify_toggle,
            socialLink_toggle;
    private String  myEventOn, checkinOn, locOn, phoneOn, socialLinkOn,appToken = "f6fa0fa5041479b76322145vgh1g410975af7766";
    private TextView txtview_changePassword;
    private BasicActivity basicActivity;
    private Boolean isFragmentVisible=false;
    private RelativeLayout mainLayout,saveLayout;
    private MyActivity myActivity;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.settings, container, false);
        isFragmentVisible=true;
        basicActivity=(BasicActivity)getActivity();
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        findViews();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loc_toggle:
                if (loc_toggle.isChecked()) {
                    locOn = "1";
                } else {
                    locOn = "0";
                }
                break;
            case R.id.checkin_toggle:
                if (checkin_toggle.isChecked()) {
                    checkinOn = "1";
                } else {
                    checkinOn = "0";
                }
                break;
            case R.id.contact_toggle:
                if (phone_toggle.isChecked()) {
                    phoneOn = "1";
                } else {
                    phoneOn = "0";
                }
                break;

            case R.id.social_toggle:
                if (socialLink_toggle.isChecked()) {
                    socialLinkOn = "1";
                } else {
                    socialLinkOn = "0";
                }
                break;
            case R.id.tv_changepswd:
                basicActivity.replaceFragment(new ChangePasswordFragment());
                break;
            case R.id.save_layout:
                setSettingAPI();
                break;
        }
    }

    private void findViews(){

        checkin_toggle = (ToggleButton) view.findViewById(R.id.checkin_toggle);
        loc_toggle = (ToggleButton) view.findViewById(R.id.loc_toggle);
        phone_toggle = (ToggleButton) view.findViewById(R.id.contact_toggle);
        socialLink_toggle = (ToggleButton) view.findViewById(R.id.social_toggle);
        txtview_changePassword=(TextView)view.findViewById(R.id.tv_changepswd);
        saveLayout=(RelativeLayout)view.findViewById(R.id.save_layout);
        mainLayout=(RelativeLayout)view.findViewById(R.id.fl_content_frame);
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        getSettingAPI();

        checkin_toggle.setOnClickListener(this);
        loc_toggle.setOnClickListener(this);
        phone_toggle.setOnClickListener(this);
        socialLink_toggle.setOnClickListener(this);
        txtview_changePassword.setOnClickListener(this);
        saveLayout.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible=false;
    }

    private void getSettingAPI() {
    progressBar.setVisibility(View.VISIBLE);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<SettingResponse> userCall = apiInterface.get_user_settings(appToken,access_token);
        userCall.enqueue(new Callback<SettingResponse>() {

            @Override
            public void onResponse(Call<SettingResponse> call, Response<SettingResponse> response) {
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        locOn= response.body().getSettingsDataModal().getShowLocation();
                        socialLinkOn= response.body().getSettingsDataModal().getShowLinks();
                        checkinOn= response.body().getSettingsDataModal().getShowCheckins();
                        phoneOn= response.body().getSettingsDataModal().getShowContact();
                        if(isFragmentVisible){
                            if(locOn.equalsIgnoreCase("1")){
                                loc_toggle.setChecked(true);
                            }
                            else {
                                loc_toggle.setChecked(false);
                            }
                            if(socialLinkOn.equalsIgnoreCase("1")){
                                socialLink_toggle.setChecked(true);
                            }
                            else {
                              socialLink_toggle.setChecked(false);
                            }
                            if(checkinOn.equalsIgnoreCase("1")){
                                checkin_toggle.setChecked(true);
                            }
                            else {
                                checkin_toggle.setChecked(false);
                            }
                            if(phoneOn.equalsIgnoreCase("1")){
                                phone_toggle.setChecked(true);
                            }
                            else {
                                phone_toggle.setChecked(false);
                            }
                        }
//                        SharedPrefsUtils.setStringPreference(getActivity(), "show_location", response.body().getSettingsDataModal().getShowLocation());
//                        SharedPrefsUtils.setStringPreference(getActivity(), "show_checkin", response.body().getSettingsDataModal().getShowCheckins());
//                        SharedPrefsUtils.setStringPreference(getActivity(), "show_contact", response.body().getSettingsDataModal().getShowContact());
//                        SharedPrefsUtils.setStringPreference(getActivity(), "show_link", response.body().getSettingsDataModal().getShowLinks());

                        Toast.makeText(getActivity().getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //showAlert(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SettingResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                // Utils.disableEnableControls(true, mainLayout);
            }
        });
    }

    private void setSettingAPI() {
        progressBar.setVisibility(View.VISIBLE);
        Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<SettingResponse> userCall = apiInterface.update_user_settings(appToken,access_token,locOn,socialLinkOn,checkinOn,phoneOn);
        userCall.enqueue(new Callback<SettingResponse>() {

            @Override
            public void onResponse(Call<SettingResponse> call, Response<SettingResponse> response) {
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        Toast.makeText(getActivity().getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                        SharedPrefsUtils.setStringPreference(getActivity(), "show_location", response.body().getSettingsDataModal().getShowLocation());
//                        SharedPrefsUtils.setStringPreference(getActivity(), "show_checkin", response.body().getSettingsDataModal().getShowCheckins());
//                        SharedPrefsUtils.setStringPreference(getActivity(), "show_contact", response.body().getSettingsDataModal().getShowContact());
//                        SharedPrefsUtils.setStringPreference(getActivity(), "show_link", response.body().getSettingsDataModal().getShowLinks());
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //showAlert(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SettingResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                // Utils.disableEnableControls(true, mainLayout);
            }
        });
    }

}
