package com.zoptal.announcements.fragments;

import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.base.DataHelperClass;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.base.MyApp;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.mainController.LoginSignUpActivity;
import com.zoptal.announcements.eventBusClasses.CalendarDateSelectedEvent;
import com.zoptal.announcements.modal.ContactNameModel;
import com.zoptal.announcements.modal.ContactNameResponseModal;
import com.zoptal.announcements.responseClasses.ContactsResponse;
import com.zoptal.announcements.responseClasses.EventResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.DateConvertor;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.Utils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateEventFragment extends Fragment implements View.OnClickListener {
    View view;
    private EditText edt_eventName,edt_contactName,edt_contactPhone,edt_eventDesc,edt_qntynumber;
    private CheckBox chk_eventPublic,chk_eventPrivate,chk_selectAll;
    private RecyclerView recyclerView_contacts,recyclerView;
    private View view_locOff,view_locOn,view_eventtypeOff,view_eventtypeOn,view_partysizeOff,view_partysizeOn,view_calendarOff,view_calendarOn;
    private ToggleButton tBtn_loc,tBtn_eventType,tBtn_partySize,tBtn_calendar;
    private RelativeLayout layout_location,layout_eventType,layout_partySize,layout_calendar;
    private BasicActivity basicActivity;
    private LinearLayout ll_mapFrame,ll_calendarFrame;
    private ContactsAdapter mAdapter;
    private SelectedContactAdapter cadapter;
    private List<ContactNameModel>contactList;
    private String screeHeadingText,locationAddressString="empty",startTimeString="empty",endTimeString="empty",dateString="empty",selecttid,selectname,
    eventTypeCheck="Private";
    private TextView txtview_screenHeading,txtview_contextText,txtview_loctext,txtview_caltext,txtview_partysize,txtview_eventType,
    txtview_cancelEvent,txtview_saveEvent,txtview_inviteFriends,empty_data,txtview_editEvent;
    private ImageView img_increment,img_decrement;
    private int contactCount=0;
    AlphaAnimation buttonClick;
    private MyActivity myActivity;
    private Boolean isFragmentVisible=false;
    private RelativeLayout mainLayout;
    private ProgressBar progressBar;
    private String appToken = "f6fa0fa5041479b76322145vgh1g410975af7766",userData="false",EditCheck="true",  editInviteCheck="false";;
    private LatLng latLng;
    String contact_no_values = "";
    private ArrayList<ContactsResponse.ContactsDataModal> contactListingResponseArrayList = new ArrayList<>();
    private ArrayList<ContactsResponse.ContactsDataModal> contactListingReturnArrayList;
    private ArrayList<ContactNameResponseModal>contactfinalDataList;
    private Dialog contactDialog;
   private TextView tv_emptyData,txtview_contentDetailTxt;
   private ImageView close_btn;
   private RecyclerView.LayoutManager layoutManager,mLayoutManager;
   private Button submit_list;
   private List <String>selectedContactId,selectedContactName,selectedContactUrl;
   int i;
   private ScrollView mScrollView;
   private View transparentView;
   private String saveCancelCheck="false",countCheck="false",selectAllCheck="none";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.create_event_fragment,container,false);
        isFragmentVisible=true;
        basicActivity=(BasicActivity)getActivity();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
        BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
        buttonClick = new AlphaAnimation(1F, 0.5F);
        contactList= new ArrayList<>();
        contactListingReturnArrayList= new ArrayList<>();

        Bundle bundle = getArguments();
        if (bundle != null) {
            screeHeadingText = bundle.getString("eventtype", "");
            Log.e("eventssssss", screeHeadingText);
        }
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Log.e("accesstoken",access_token);
        findViews();
        recyclerInit();

        return view;
    }

    private void findViews(){
        edt_eventName=(EditText)view.findViewById(R.id.edt_event_name);
        edt_contactName=(EditText)view.findViewById(R.id.edt_contact_name);
        edt_contactPhone=(EditText)view.findViewById(R.id.edt_contact_phone);
        edt_eventDesc=(EditText)view.findViewById(R.id.edt_event_desc);
        txtview_screenHeading=(TextView)view.findViewById(R.id.tv_screen_heading);
        txtview_contextText=(TextView)view.findViewById(R.id.heading_context_txt);
        chk_eventPrivate=(CheckBox)view.findViewById(R.id.chk_private_event);
        chk_eventPublic=(CheckBox)view.findViewById(R.id.chk_public_event);
        img_increment=(ImageView)view.findViewById(R.id.inc);
        edt_qntynumber=(EditText) view.findViewById(R.id.qty_text);
        img_decrement=(ImageView)view.findViewById(R.id.dec);
        view_locOff=(View)view.findViewById(R.id.view_loc_off);
        view_locOn=(View)view.findViewById(R.id.view_loc_on);
        view_eventtypeOff=(View)view.findViewById(R.id.view_event_off);
        view_eventtypeOn=(View)view.findViewById(R.id.view_event_on);
        view_partysizeOff=(View)view.findViewById(R.id.view_party_off);
        view_partysizeOn=(View)view.findViewById(R.id.view_party_on);
        view_calendarOff=(View)view.findViewById(R.id.view_cal_off);
        view_calendarOn=(View)view.findViewById(R.id.view_cal_on);
        tBtn_loc=(ToggleButton)view.findViewById(R.id.loc_toggle);
        tBtn_eventType=(ToggleButton)view.findViewById(R.id.event_toggle);
        tBtn_partySize=(ToggleButton)view.findViewById(R.id.party_toggle);
        tBtn_calendar=(ToggleButton)view.findViewById(R.id.calendar_toggle);
        layout_location=(RelativeLayout)view.findViewById(R.id.rl_location);
        layout_eventType=(RelativeLayout)view.findViewById(R.id.rl_event_type);
        layout_partySize=(RelativeLayout)view.findViewById(R.id.rl_party_size);
        layout_calendar=(RelativeLayout)view.findViewById(R.id.rl_calendar);
        ll_mapFrame=(LinearLayout)view.findViewById(R.id.map_content_frame);
        mScrollView=(ScrollView)view.findViewById(R.id.main_scrollview);
        transparentView=(View)view.findViewById(R.id.falseScrollView);
        ll_calendarFrame=(LinearLayout)view.findViewById(R.id.calendar_content_frame);
        txtview_loctext=(TextView)view.findViewById(R.id.loc_txt);
        txtview_caltext=(TextView)view.findViewById(R.id.cal_txt);
        txtview_eventType=(TextView)view.findViewById(R.id.event_txt);
        txtview_partysize=(TextView)view.findViewById(R.id.party_txt);
        txtview_cancelEvent=(TextView)view.findViewById(R.id.tv_cancel_event);
        txtview_saveEvent=(TextView)view.findViewById(R.id.tv_save_event);
        txtview_editEvent = (TextView) view.findViewById(R.id.tv_edit_event);
        txtview_inviteFriends=(TextView)view.findViewById(R.id.tv_invite_frnds);
        txtview_contentDetailTxt=(TextView)view.findViewById(R.id.content_detail_txt);
        if(contactCount==0){
            txtview_inviteFriends.setText("Invite friends");
        }
        else {
            txtview_inviteFriends.setText("Invite more friends");
        }
        mainLayout = (RelativeLayout) view.findViewById(R.id.content_frame);
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        txtview_screenHeading.setText(screeHeadingText);
        if(screeHeadingText.equalsIgnoreCase("Funeral")){
            txtview_contextText.setText("Sorry for your loss");
        }
        else {
            txtview_contextText.setText("Congratulations on"+" "+screeHeadingText);
        }
        getPhoneContacts();
        chk_eventPrivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(chk_eventPrivate.isChecked()){
                    eventTypeCheck="Private";
                    chk_eventPublic.setChecked(false);
                    txtview_eventType.setText("private");
                    ((MyApp)getActivity().getApplicationContext()).setEventTypeValue(eventTypeCheck);
                    }
                    else {
                    txtview_eventType.setText("");
                }

            }
        });
        chk_eventPublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(chk_eventPublic.isChecked()){
                    eventTypeCheck="Public";
                    chk_eventPrivate.setChecked(false);
                    txtview_eventType.setText("Public");
                    ((MyApp)getActivity().getApplicationContext()).setEventTypeValue(eventTypeCheck);
                }
                else {
                    txtview_eventType.setText("");
                    eventTypeCheck="Private";
                }
            }
        });
        transparentView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        tBtn_loc.setOnClickListener(this);
        tBtn_eventType.setOnClickListener(this);
        tBtn_partySize.setOnClickListener(this);
        tBtn_calendar.setOnClickListener(this);
        img_increment.setOnClickListener(this);
        img_decrement.setOnClickListener(this);
        txtview_saveEvent.setOnClickListener(this);
        txtview_cancelEvent.setOnClickListener(this);
        txtview_inviteFriends.setOnClickListener(this);
        txtview_editEvent.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.loc_toggle:
                if(tBtn_loc.isChecked()){
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(tBtn_loc.getWindowToken(), 0);
                    layout_location.setVisibility(View.VISIBLE);
                    view_locOn.setVisibility(View.VISIBLE);
                    view_locOff.setVisibility(View.GONE);
                    Fragment childFragment = new MapFragment();
                    Bundle args = new Bundle();
                    args.putString("editCheck", EditCheck);
                    childFragment.setArguments(args);
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(R.id.map_content_frame, childFragment).commit();
                    }
                    else{
                    layout_location.setVisibility(View.GONE);
                    view_locOn.setVisibility(View.GONE);
                    view_locOff.setVisibility(View.VISIBLE);
                    locationAddressString=((MyApp)getActivity().getApplicationContext()).getEventPlace();
                    latLng=((MyApp)getActivity().getApplicationContext()).getEventLatlng();
                    txtview_loctext.setText(locationAddressString);
                }
                break;
            case R.id.event_toggle:
                if(tBtn_eventType.isChecked()){
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(tBtn_eventType.getWindowToken(), 0);
                    layout_eventType.setVisibility(View.VISIBLE);
                    view_eventtypeOn.setVisibility(View.VISIBLE);
                    view_eventtypeOff.setVisibility(View.GONE);
                }else {
                    layout_eventType.setVisibility(View.GONE);
                    view_eventtypeOn.setVisibility(View.GONE);
                    view_eventtypeOff.setVisibility(View.VISIBLE);
                    if(eventTypeCheck.equalsIgnoreCase("Private")){
                        txtview_eventType.setText("Private");
                    }
                    else if(eventTypeCheck.equalsIgnoreCase("Public")){
                        txtview_eventType.setText("Public");
                    }
                    else {

                    }
                }
                break;
            case R.id.party_toggle:
                if(tBtn_partySize.isChecked()){
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(tBtn_partySize.getWindowToken(), 0);
                    layout_partySize.setVisibility(View.VISIBLE);
                    view_partysizeOn.setVisibility(View.VISIBLE);
                    view_partysizeOff.setVisibility(View.GONE);
                }else {
                    layout_partySize.setVisibility(View.GONE);
                    view_partysizeOn.setVisibility(View.GONE);
                    view_partysizeOff.setVisibility(View.VISIBLE);
                    txtview_partysize.setText(edt_qntynumber.getText().toString().trim());
                }
                break;
            case R.id.calendar_toggle:
                if(tBtn_calendar.isChecked()){
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(tBtn_calendar.getWindowToken(), 0);
                    layout_calendar.setVisibility(View.VISIBLE);
                    view_calendarOn.setVisibility(View.VISIBLE);
                    view_calendarOff.setVisibility(View.GONE);
                    Fragment childFragment = new CustomCalendar();
                    Bundle args = new Bundle();
                    args.putString("editCheck", EditCheck);
                    childFragment.setArguments(args);
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(R.id.calendar_content_frame, childFragment).commit();
                }else {
                    layout_calendar.setVisibility(View.GONE);
                    view_calendarOn.setVisibility(View.GONE);
                    view_calendarOff.setVisibility(View.VISIBLE);
                    startTimeString=((MyApp)getActivity().getApplicationContext()).getStartTime();
                    endTimeString=((MyApp)getActivity().getApplicationContext()).getEndTime();
                    dateString=((MyApp)getActivity().getApplicationContext()).getDateValue();
                    dateString=((MyApp)getActivity().getApplicationContext()).getDateValue();
                    DateConvertor obj = new DateConvertor();
                    String eventdate = obj.changeDateFormatRecieved(dateString);
                    Log.e("datevalue", eventdate);
                    DateConvertor obj1 = new DateConvertor();
                    String eventstarttime = obj1.changeTimeFormatRecieve(startTimeString);
                    Log.e("datevalues", eventstarttime);
                    DateConvertor obj2 = new DateConvertor();
                    String eventendtime = obj2.changeTimeFormatRecieve(endTimeString);
                    Log.e("datevaluess", eventendtime);

                    txtview_caltext.setText(eventdate+" "+eventstarttime+" - "+eventendtime);

                }
                break;
            case R.id.inc:
                if(!(edt_qntynumber.getText().length()==0)) {
                    contactCount = Integer.parseInt(edt_qntynumber.getText().toString().trim());
                }
                contactCount++;
                img_decrement.setEnabled(true);
                edt_qntynumber.setText(""+contactCount);
                break;
            case R.id.dec:
                if(!(edt_qntynumber.getText().length()==0)) {
                    contactCount = Integer.parseInt(edt_qntynumber.getText().toString().trim());
                }
                contactCount--;
                if(contactCount<0){
                    contactCount=0;
                }
                if(contactCount==0 || contactCount<1){
                    img_decrement.setEnabled(false);
                }else {
                    edt_qntynumber.setText("" + contactCount);
                }
                break;
            case R.id.tv_cancel_event:
                view.startAnimation(buttonClick);
                deleteAllValues();
                onSelectedDayChanged(userData,"true");
                if(contactCount==0){
                    txtview_inviteFriends.setText("Invite friends");
                }
                else {
                    txtview_inviteFriends.setText("Invite more friends");
                }
                edt_eventName.requestFocus();

                if(saveCancelCheck.equalsIgnoreCase("true")) {
                    basicActivity.replaceFragment(new HomeFragment());
                }
                break;
            case R.id.tv_save_event:
                view.startAnimation(buttonClick);
                saveCancelCheck="true";
                txtview_contextText.setText("Review event details");
                txtview_contentDetailTxt.setText("Review and edit details about the event:");
                checkValidation();
                break;
            case R.id.tv_edit_event:
                view.startAnimation(buttonClick);
                editViews();
                if(contactCount==0){
                    txtview_inviteFriends.setText("Invite friends");
                }
                else {
                    txtview_inviteFriends.setText("Invite more friends");
                }
                break;
            case R.id.tv_invite_frnds:
                if(contact_no_values!=null && !contact_no_values.isEmpty()){
                editInviteCheck = "true";
                getContactListDialog();
            }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmentVisible=false;
    }
    private void recyclerInit(){
        recyclerView_contacts=(RecyclerView)view.findViewById(R.id.recycler_contacts);
        recyclerView_contacts.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView_contacts.setLayoutManager(mLayoutManager);
    }

    public void replaceMapFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment topFragment = fragmentManager.findFragmentById(R.id.map_content_frame);
        if (topFragment != null) {
            fragmentTransaction.remove(topFragment);
            fragmentTransaction.replace(R.id.map_content_frame, fragment);
        } else {
            fragmentTransaction.add(R.id.map_content_frame, fragment);
        }
    }

    private void onSelectedDayChanged(String dateSelected,String showButtons) {
        EventBus.getDefault().post(new CalendarDateSelectedEvent(dateSelected,showButtons));
    }
    private void disableViews(){
        EditCheck="false";
        userData="true";
        edt_eventName.setEnabled(false);
        edt_contactName.setEnabled(false);
        edt_contactPhone.setEnabled(false);
        edt_eventDesc.setEnabled(false);
        chk_eventPrivate.setEnabled(false);
        chk_eventPublic.setEnabled(false);
        img_increment.setEnabled(false);
        edt_qntynumber.setEnabled(false);
        img_decrement.setEnabled(false);
        txtview_inviteFriends.setEnabled(false);
        tBtn_calendar.setChecked(false);
        tBtn_partySize.setChecked(false);
        tBtn_eventType.setChecked(false);
        tBtn_loc.setChecked(false);
        layout_calendar.setVisibility(View.GONE);
        layout_location.setVisibility(View.GONE);
        layout_partySize.setVisibility(View.GONE);
        layout_eventType.setVisibility(View.GONE);
        txtview_cancelEvent.setEnabled(false);
        img_increment.setEnabled(false);
        img_decrement.setEnabled(false);
        txtview_cancelEvent.setBackgroundResource(R.drawable.button_back_disable);
        onSelectedDayChanged(userData,"false");
    }


    private void editViews(){
        EditCheck="true";
        userData="true";
        edt_eventName.setEnabled(true);
        edt_contactName.setEnabled(true);
        edt_contactPhone.setEnabled(true);
        edt_eventDesc.setEnabled(true);
        chk_eventPrivate.setEnabled(true);
        chk_eventPublic.setEnabled(true);
        img_increment.setEnabled(true);
        edt_qntynumber.setEnabled(true);
        img_decrement.setEnabled(true);
        txtview_loctext.setEnabled(true);
        txtview_inviteFriends.setEnabled(true);
        txtview_cancelEvent.setEnabled(true);
        img_increment.setEnabled(true);
        img_decrement.setEnabled(true);
        txtview_cancelEvent.setBackgroundResource(R.drawable.button_back);
        onSelectedDayChanged(userData,"true");
    }

   private void deleteAllValues(){
        userData="false";
       edt_eventName.setText("");
       edt_contactName.setText("");
       edt_contactPhone.setText("");
       edt_eventDesc.setText("");
       chk_eventPrivate.setChecked(false);
       chk_eventPublic.setChecked(false);
       txtview_loctext.setText("");
       txtview_caltext.setText("");
       txtview_eventType.setText("");
       txtview_partysize.setText("");
       edt_qntynumber.setText("");
       contactCount=0;
       txtview_inviteFriends.setText("Invite friends");
       if(editInviteCheck.equalsIgnoreCase("true")) {
           contactfinalDataList.clear();
           //mAdapter.notifyDataSetChanged();
           recyclerView_contacts.setAdapter(mAdapter);
           for (int j = 0; j < contactListingResponseArrayList.size(); j++) {
               contactListingResponseArrayList.get(j).setInvited("0");
           }
       }


    }
    private void clearAllValues(){
        userData="false";
        edt_eventName.setText("");
        edt_contactName.setText("");
        edt_contactPhone.setText("");
        edt_eventDesc.setText("");
        chk_eventPrivate.setChecked(false);
        chk_eventPublic.setChecked(false);
        txtview_loctext.setText("");
        txtview_caltext.setText("");
        txtview_eventType.setText("");
        txtview_partysize.setText("");
        edt_qntynumber.setText("");
        contactCount=0;
        if(editInviteCheck.equalsIgnoreCase("true")) {
            contactfinalDataList.clear();
            mAdapter.notifyDataSetChanged();
            recyclerView_contacts.setAdapter(mAdapter);
            for (int j = 0; j < contactListingResponseArrayList.size(); j++) {
                contactListingResponseArrayList.get(j).setInvited("0");
            }
        }
        onSelectedDayChanged(userData,"true");
    }


    private void checkValidation() {
        locationAddressString=((MyApp)getActivity().getApplicationContext()).getEventPlace();
        txtview_loctext.setText(locationAddressString);
        startTimeString=((MyApp)getActivity().getApplicationContext()).getStartTime();
        endTimeString=((MyApp)getActivity().getApplicationContext()).getEndTime();
        dateString=((MyApp)getActivity().getApplicationContext()).getDateValue();
        latLng=((MyApp)getActivity().getApplicationContext()).getEventLatlng();

        if (edt_eventName.getText().length() == 0) {
            myActivity.showOneErrorDialog("Event name should not be empty.", "OK", getActivity());
            edt_eventName.setFocusable(true);
            return;
        }
        if (edt_eventDesc.getText().length() == 0) {
            myActivity.showOneErrorDialog("Add description to the event.", "OK", getActivity());
            edt_eventDesc.setFocusable(true);
            return;
        }
        if (txtview_loctext.getText().length() == 0) {
            myActivity.showOneErrorDialog("Select the location of the event.", "OK", getActivity());
            txtview_loctext.setFocusable(true);
            return;
        }
        if(startTimeString==null || startTimeString.isEmpty()){
            startTimeString="";
            myActivity.showOneErrorDialog("Start time of event should not be empty.", "OK", getActivity());
            return;

        }
        if(endTimeString==null || endTimeString.isEmpty()){
            endTimeString="";
            myActivity.showOneErrorDialog("End time of event should not be empty.", "OK", getActivity());
            return;
        }
        if(dateString==null || dateString.isEmpty()){
            dateString="";
            myActivity.showOneErrorDialog("Date of event should not be empty.", "OK", getActivity());
            return;
        }

        if (edt_qntynumber.getText().length()==0||edt_qntynumber.getText().toString().trim().equalsIgnoreCase("0")){
            myActivity.showOneErrorDialog("Event size should not be empty.", "OK", getActivity());
            txtview_partysize.setFocusable(true);
            return;
        }
        if (txtview_caltext.getText().toString().trim().equalsIgnoreCase("-")) {
            myActivity.showOneErrorDialog("Choose the date and time of the event.", "OK", getActivity());
            txtview_caltext.setFocusable(true);
            return;
        }
        if (latLng==null) {
            myActivity.showOneErrorDialog("Please enter the location again.", "OK", getActivity());
            return;
        }
        else {
            DateConvertor obj = new DateConvertor();
            String eventdate = obj.changeDateFormatRecieved(dateString);
            Log.e("datevalue", eventdate);
            DateConvertor obj1 = new DateConvertor();
            String eventstarttime = obj1.changeTimeFormatRecieve(startTimeString);
            Log.e("datevalues", eventstarttime);
            DateConvertor obj2 = new DateConvertor();
            String eventendtime = obj2.changeTimeFormatRecieve(endTimeString);
            Log.e("datevaluess", eventendtime);
            txtview_caltext.setText(eventdate+" "+eventstarttime+" - "+eventendtime);
        txtview_partysize.setText(""+edt_qntynumber.getText().toString().trim());
            if(eventTypeCheck.equalsIgnoreCase("Private")){
                txtview_eventType.setText("Private");
            }
            else if(eventTypeCheck.equalsIgnoreCase("Public")){
                txtview_eventType.setText("Public");
            }

            if (txtview_saveEvent.getText().toString().trim().equalsIgnoreCase("Send invite to this event")) {
                createEventAPI();
            } else {
                txtview_saveEvent.setText("Send invite to this event");
                txtview_editEvent.setVisibility(View.VISIBLE);
                ((MyApp) getActivity().getApplicationContext()).setEventTypeValue(eventTypeCheck);
                disableViews();
            }
        }

    }
    private void createEventAPI() {
        progressBar.setVisibility(View.VISIBLE);
//        DateConvertor obj = new DateConvertor();
//        final String eventdate = obj.changeDateFormat(dateString);
//        Log.e("datevalue", eventdate);
        final String eventdate =dateString;
//        DateConvertor obj1 = new DateConvertor();
//        final String eventStartTime = obj1.changeTimeFormatSent(startTimeString);
//        final String eventEndTime = obj1.changeTimeFormatSent(endTimeString);
        String latitude=String.valueOf(latLng.latitude);
        String longitude=String.valueOf(latLng.longitude);

        for(int i=0;i<contactListingResponseArrayList.size();i++){
            if(contactListingResponseArrayList.get(i).getInvited().equalsIgnoreCase("1")){
                if(i==0){
                    selecttid=contactListingResponseArrayList.get(i).getId();
                    Log.e("selectedparentsss",selecttid);
                }
                else
                {
                    selecttid=selecttid+","+contactListingResponseArrayList.get(i).getId();
                    Log.e("selectedparent",selecttid);
                }
            }
            else
            {

            }
        }
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();
        final Gson gson = new GsonBuilder().create();


            RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<EventResponse> userCall = apiInterface.add_event(appToken, access_token, screeHeadingText,edt_eventName.getText().toString().trim(),
        edt_eventDesc.getText().toString().trim(),eventdate,startTimeString,endTimeString, eventTypeCheck,edt_contactName.getText().toString().trim(),
        edt_contactPhone.getText().toString().trim(),locationAddressString,latitude,edt_qntynumber.getText().toString().trim(),longitude,selecttid );
        userCall.enqueue(new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                DataHelperClass.getInstance().setEventDataModal(response.body().getEventDataModal());
                if (isFragmentVisible) {
                    progressBar.setVisibility(View.GONE);
                    Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                        //Log.e("response",""+response.body().getEventDataModal().getEventLocation());
                        myActivity.showOneErrorDialog("Invitation sent.", "OK", getActivity());
                       deleteAllValues();
                        basicActivity.replaceFragment(new HomeFragment());
                        ((MyApp)getActivity().getApplicationContext()).setEventPlace("");
                        ((MyApp)getActivity().getApplicationContext()).setEventLatlng(null);
                        ((MyApp)getActivity().getApplicationContext()).setDateValue("");
                        ((MyApp)getActivity().getApplicationContext()).setStartTime("");
                        ((MyApp)getActivity().getApplicationContext()).setEndTime("");

//                        String eventResponse= response.body().toString();
//                        ReviewEventFragment fragment6 = new ReviewEventFragment();
//                        Bundle bundle6 = new Bundle();
//                        bundle6.putString("eventtype", screeHeadingText); //key and value
//                        bundle6.putString("eventResponse", ""+eventResponse); //key and value
//                        fragment6.setArguments(bundle6);
//                        ((BasicActivity) getActivity()).replaceFragment(fragment6);

//                        contactListingResponseArrayList.clear();
//                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
//                            itemsListingResponseArrayList.add(itemsListingModal);
//                        }*/
//                        contactListingResponseArrayList = response.body().getArrayList();
//                        if (contactListingResponseArrayList.size() > 0) {
//                            recyclerView.setVisibility(View.VISIBLE);
//                            tv_emptyData.setVisibility(View.GONE);
//                            cadapter = new ParentAdapter(getActivity());
//                            recyclerView.setAdapter(cadapter);
//                        } else {
//                            recyclerView.setVisibility(View.GONE);
//                            tv_emptyData.setVisibility(View.VISIBLE);
//                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    } else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
                        recyclerView.setVisibility(View.GONE);
                        tv_emptyData.setVisibility(View.VISIBLE);
                    } else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }

    private void getPhoneContacts() {
        ArrayList<String> contacts = new ArrayList<>();
        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.e("phonenumber", phoneNumber);
            if (phoneNumber.length() >= 10) {
                //String  result = phoneNumber.replaceAll("[^\\w\\s\\p{Ps}\\p{Pe}]","");
                String result = getOnlyDigits(phoneNumber);
                contacts.add(result);
                Log.e("phonenumberfinal", result);
                HashSet hs = new HashSet();
                hs.addAll(contacts); // demoArrayList= name of arrayList from which u want to remove duplicates
                contacts.clear();
                contacts.addAll(hs);
                for (int i = 0; i < contacts.size(); i++) {
                    if (i == 0) {
                        contact_no_values = contacts.get(i);
                        Log.e("selectedparentsss", contact_no_values);
                    } else {
                        contact_no_values = contact_no_values + "," + contacts.get(i);
                        Log.e("selectedparent", contact_no_values);
                    }
                }

            }
        }
        phones.close();
        getContactsAPI();
    }

    public static String getOnlyDigits(String s) {
        String number = "";
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        number = matcher.replaceAll("");
        if (number.length() > 10) {
            number = number.substring(number.length() - 10);
        }
        return number;
    }

    private void getContactsAPI() {

        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0

       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
        String access_token = SharedPrefsUtils.getStringPreference(getActivity(), "access_token");
        Call<ContactsResponse> userCall = apiInterface.my_contacts(appToken, access_token, contact_no_values,"");
        userCall.enqueue(new Callback<ContactsResponse>() {
            @Override
            public void onResponse(Call<ContactsResponse> call, Response<ContactsResponse> response) {
                if (isFragmentVisible) {
                    if (response.body() == null) {
                        myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                        return;
                    }
                    if (response.body().isResult()) {
                         contactListingResponseArrayList.clear();
//                        /*for (ItemsListingResponse.ItemsListingModal itemsListingModal : response.body().getArrayList()) {
//                            itemsListingResponseArrayList.add(itemsListingModal);
//                        }*/
                        contactListingResponseArrayList = response.body().getArrayList();

                    } else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(getActivity(), "logged_in", false);
                        Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                        return;
                    }
//                    else if (response.body().getMessage().equalsIgnoreCase("No list found.")) {
//                        recyclerView.setVisibility(View.GONE);
//                        empty_data.setVisibility(View.VISIBLE);
//                    }
                    else {
                        myActivity.showOneErrorDialog(response.body().getMessage(), "OK", getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactsResponse> call, Throwable t) {
                if (isFragmentVisible) {
                    Utils.disableEnableControls(true, mainLayout);
                    progressBar.setVisibility(View.GONE);
                    myActivity.showOneErrorDialog("An error occurred, Please try again!", "OK", getActivity());
                }
            }
        });

    }
    public void getContactListDialog() {
        contactDialog = new Dialog(getActivity(), android.R.style.Theme_Translucent) {
            @Override
            public boolean onTouchEvent(MotionEvent event) {
                // Tap anywhere to close dialog.
                return true;
            }
        };
        contactDialog.setCanceledOnTouchOutside(false);
        contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        contactDialog.setCancelable(false);
        contactDialog.setContentView(R.layout.list_dialog);
        contactfinalDataList= new ArrayList<>();

        recyclerView = (RecyclerView) contactDialog.findViewById(R.id.recycler_contact);
        close_btn=(ImageView)contactDialog.findViewById(R.id.close_btn);
        chk_selectAll=(CheckBox) contactDialog.findViewById(R.id. select_all_chk);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        submit_list = (Button) contactDialog.findViewById(R.id.submit_list);
        empty_data=(TextView)contactDialog.findViewById(R.id.empty_data);
        if(selectAllCheck.equalsIgnoreCase("true")){
            chk_selectAll.setChecked(true);
        }
        else if(selectAllCheck.equalsIgnoreCase("false")){
            chk_selectAll.setChecked(false);
        }

        //parseParentData(parentResponse);
        // notifyParents();
        if (contactListingResponseArrayList.size() > 0) {
            txtview_inviteFriends.setText("Invite more friends");
            recyclerView.setVisibility(View.VISIBLE);
            empty_data.setVisibility(View.GONE);
            cadapter = new SelectedContactAdapter(getActivity(),selectAllCheck);
            recyclerView.setAdapter(cadapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            empty_data.setVisibility(View.VISIBLE);
            txtview_inviteFriends.setText("Invite friends");
        }

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDialog.dismiss();
                if (selectAllCheck.equalsIgnoreCase("true")) {
                    contactListingReturnArrayList = cadapter.returnList();
                    Log.e("value", "" + contactListingReturnArrayList);
                    String selected_parent_name = "", selectedUrl = "";
                    selectedContactId = new ArrayList<String>();
                    selectedContactName = new ArrayList<String>();
                    selectedContactUrl = new ArrayList<String>();
                    if (contactListingReturnArrayList.size() > 0) {
                        for (int i = 0; i < contactListingReturnArrayList.size(); i++) {
                            if (contactListingReturnArrayList.get(i).getInvited().equalsIgnoreCase("1")) {
                                selectedContactId.add(contactListingReturnArrayList.get(i).getId());
                                selectedContactUrl.add(contactListingReturnArrayList.get(i).getPicture());
                                selectedContactName.add(contactListingReturnArrayList.get(i).getUsername());
                            }

                        }
                    }

                    if (selectedContactId.size() > 0) {
                        for (int i = 0; i < selectedContactId.size(); i++) {
                            ContactNameResponseModal dataModal = new ContactNameResponseModal();
                            dataModal.setId(selectedContactId.get(i));
                            dataModal.setUsername(selectedContactName.get(i));
                            dataModal.setPicture(selectedContactUrl.get(i));
                            contactfinalDataList.add(dataModal);
                        }
                        recyclerView_contacts.setVisibility(View.VISIBLE);
                        mAdapter = new ContactsAdapter(getActivity(), contactfinalDataList);
                        recyclerView_contacts.setAdapter(mAdapter);
                        Log.e("size", "" + contactfinalDataList.size());
//                        contactCount = contactfinalDataList.size();
//                        if (edt_qntynumber.getText().length() == 0) {
//                            edt_qntynumber.setText("0");
//                        }
                        //contactCount=contactfinalDataList.size()+Integer.parseInt(edt_qntynumber.getText().toString().trim());
                        //Log.e("countCheck",countCheck);
//                    if(countCheck.equalsIgnoreCase("true")) {
//                        contactCount = contactCount + editTextCount;
//                        edt_qntynumber.setText("" + contactCount);
//                    }
//                    else {
                        //contactCount = contactCount;
                        //edt_qntynumber.setText("" + contactCount);
                        // }
                    } else {
                        empty_data.setVisibility(View.VISIBLE);
                        recyclerView_contacts.setVisibility(View.GONE);
                        //edt_qntynumber.setText("" + contactCount);

                    }
                }
                if (selectAllCheck.equalsIgnoreCase("false")) {
//                    contactCount=0;
//                    edt_qntynumber.setText("");
                    recyclerView_contacts.setVisibility(View.GONE);
//                    contactfinalDataList.clear();
//                    mAdapter = new ContactsAdapter(getActivity(), contactfinalDataList);
//                    recyclerView_contacts.setAdapter(mAdapter);
                }
            }

        });

        submit_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDialog.dismiss();
                if (selectAllCheck.equalsIgnoreCase("true")) {
                    contactListingReturnArrayList = cadapter.returnList();
                    Log.e("value", "" + contactListingReturnArrayList);
                    String selected_parent_name = "", selectedUrl = "";
                    selectedContactId = new ArrayList<String>();
                    selectedContactName = new ArrayList<String>();
                    selectedContactUrl = new ArrayList<String>();
                    if (contactListingReturnArrayList.size() > 0) {
                        for (int i = 0; i < contactListingReturnArrayList.size(); i++) {
                            if (contactListingReturnArrayList.get(i).getInvited().equalsIgnoreCase("1")) {
                                selectedContactId.add(contactListingReturnArrayList.get(i).getId());
                                selectedContactUrl.add(contactListingReturnArrayList.get(i).getPicture());
                                selectedContactName.add(contactListingReturnArrayList.get(i).getUsername());
                            }

                        }
                    }

                    if (selectedContactId.size() > 0) {
                        for (int i = 0; i < selectedContactId.size(); i++) {
                            ContactNameResponseModal dataModal = new ContactNameResponseModal();
                            dataModal.setId(selectedContactId.get(i));
                            dataModal.setUsername(selectedContactName.get(i));
                            dataModal.setPicture(selectedContactUrl.get(i));
                            contactfinalDataList.add(dataModal);
                        }
                        recyclerView_contacts.setVisibility(View.VISIBLE);
                        mAdapter = new ContactsAdapter(getActivity(), contactfinalDataList);
                        recyclerView_contacts.setAdapter(mAdapter);
                        Log.e("size", "" + contactfinalDataList.size());
//                        contactCount = contactfinalDataList.size();
//                        if (edt_qntynumber.getText().length() == 0) {
//                            edt_qntynumber.setText("0");
//                        }
                        //contactCount=contactfinalDataList.size()+Integer.parseInt(edt_qntynumber.getText().toString().trim());
                        //Log.e("countCheck",countCheck);
//                    if(countCheck.equalsIgnoreCase("true")) {
//                        contactCount = contactCount + editTextCount;
//                        edt_qntynumber.setText("" + contactCount);
//                    }
//                    else {
                        //contactCount = contactCount;
                        //edt_qntynumber.setText("" + contactCount);
                        // }
                    } else {
                        empty_data.setVisibility(View.VISIBLE);
                        recyclerView_contacts.setVisibility(View.GONE);
                        //edt_qntynumber.setText("" + contactCount);

                    }
                } else if (selectAllCheck.equalsIgnoreCase("false")) {
//                    contactCount=0;
//                    edt_qntynumber.setText("");
                    recyclerView_contacts.setVisibility(View.GONE);
//                    contactfinalDataList.clear();
//                    mAdapter = new ContactsAdapter(getActivity(), contactfinalDataList);
//                    recyclerView_contacts.setAdapter(mAdapter);
                } else {
                    contactListingReturnArrayList = cadapter.returnList();
                    Log.e("value", "" + contactListingReturnArrayList);
                    String selected_parent_name = "", selectedUrl = "";
                    selectedContactId = new ArrayList<String>();
                    selectedContactName = new ArrayList<String>();
                    selectedContactUrl = new ArrayList<String>();
                    if (contactListingReturnArrayList.size() > 0) {
                        for (int i = 0; i < contactListingReturnArrayList.size(); i++) {

                            if (contactListingReturnArrayList.get(i).getInvited().equalsIgnoreCase("1")) {
                                selectedContactId.add(contactListingReturnArrayList.get(i).getId());
                                selectedContactUrl.add(contactListingReturnArrayList.get(i).getPicture());
                                selectedContactName.add(contactListingReturnArrayList.get(i).getUsername());
                            }

                        }
                    }

                    if (selectedContactId.size() > 0) {
                        for (int i = 0; i < selectedContactId.size(); i++) {
                            ContactNameResponseModal dataModal = new ContactNameResponseModal();
                            dataModal.setId(selectedContactId.get(i));
                            dataModal.setUsername(selectedContactName.get(i));
                            dataModal.setPicture(selectedContactUrl.get(i));
                            contactfinalDataList.add(dataModal);
                        }
                        recyclerView_contacts.setVisibility(View.VISIBLE);
                        mAdapter = new ContactsAdapter(getActivity(), contactfinalDataList);
                        recyclerView_contacts.setAdapter(mAdapter);
                        Log.e("size", "" + contactfinalDataList.size());
//                    if(edt_qntynumber.getText().length()==0){
//                        edt_qntynumber.setText("0");
//                    }
                        //contactCount=contactfinalDataList.size()+Integer.parseInt(edt_qntynumber.getText().toString().trim());
                        //Log.e("countCheck",countCheck);
//                    if(countCheck.equalsIgnoreCase("true")) {
//                        contactCount = contactCount + editTextCount;
//                        edt_qntynumber.setText("" + contactCount);
//                    }
//                    else {
                        //contactCount = contactCount;
                        //edt_qntynumber.setText("" + contactCount);
                        // }
                    } else {
                        empty_data.setVisibility(View.VISIBLE);
                        recyclerView_contacts.setVisibility(View.GONE);
                        //edt_qntynumber.setText(""+contactCount);

                    }
                }
            }
        });
        chk_selectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    selectAllCheck="true";
                    cadapter = new SelectedContactAdapter(getActivity(),selectAllCheck);
                    recyclerView.setAdapter(cadapter);

                }
                else {
                    selectAllCheck="false";
                    cadapter = new SelectedContactAdapter(getActivity(),selectAllCheck);
                    recyclerView.setAdapter(cadapter);

                    //selectAllCheck="none";
                }
            }
        });
        contactDialog.show();
    }

    public class SelectedContactAdapter extends RecyclerView.Adapter<SelectedContactAdapter.MyViewHolder> {
         private Context context;
        String selectedteacherid,imageurl,selectAll;
        StringBuilder tid;
        StringBuilder tname;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView game_data;
            public CheckBox chcktchr;
            public ImageView image_parent;

            public MyViewHolder(View v) {
                super(v);
                game_data = (TextView) v.findViewById(R.id.prnt_name);
                image_parent = (ImageView) v.findViewById(R.id.parent_img);
                chcktchr = (CheckBox) v.findViewById(R.id.prnt_chk);
                if(selectAllCheck.equalsIgnoreCase("true")){
                    //chcktchr.setChecked(true);
                    for(int j=0;j<contactListingResponseArrayList.size();j++){
                        contactListingResponseArrayList.get(j).setInvited("1");
                    }
                }
                else if(selectAllCheck.equalsIgnoreCase("false")){
                    //chcktchr.setChecked(false);
                    for(int j=0;j<contactListingResponseArrayList.size();j++){
                        contactListingResponseArrayList.get(j).setInvited("0");
                    }
                }
            }
        }

        public SelectedContactAdapter(Context context,String selectAll) {
            this.context = context;
            this.selectAll=selectAll;
        }

        @Override
        public SelectedContactAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_contact_dialog, parent, false);
            return new SelectedContactAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(SelectedContactAdapter.MyViewHolder holder,final int position) {
            String invitedCheck= contactListingResponseArrayList.get(position).getInvited();
                if (invitedCheck.equalsIgnoreCase("1")) {
                    holder.chcktchr.setChecked(true);
                    //countCheck = "true";
                } else {
                    holder.chcktchr.setChecked(false);

                    if (countCheck.equalsIgnoreCase("true")) {

                    }

                }

            holder.game_data.setText(contactListingResponseArrayList.get(position).getUsername());

            imageurl=contactListingResponseArrayList.get(position).getPicture();

            if (imageurl.isEmpty()) {
                Picasso.with(getActivity()).load(R.drawable.no_profile_grey_circle_bg).resize(512, 512).into(holder.image_parent);
            } else {
                Picasso.with(getActivity()).load(imageurl).resize(512, 512).into(holder.image_parent);
            }
            holder.chcktchr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        contactListingResponseArrayList.get(position).setInvited("1");
                        //contactCount++;

                        chk_selectAll.setChecked(false);
                        selectAllCheck="none";

//                        selectedteacherid = contactListingResponseArrayList.get(position).getId();
//                        selectedContactId.add(selectedteacherid);
//                        selectedContactName.add(contactListingResponseArrayList.get(position).getUsername());
//                        selectedContactUrl.add(contactListingResponseArrayList.get(position).getPicture());
//                        Log.e("teacher id---", "" + selectedteacherid);
//
//                        tid = new StringBuilder();
//                        tname = new StringBuilder();
//
//                        for (int i = 0; i < selectedContactId.size(); i++) {
//                            tid.append(selectedContactId.get(i));
//                            if (i != selectedContactId.size() - 1) {
//                                tid.append(",");
//                            }
//                            selecttid = tid.toString();
//                            Log.e("seledidddd", "" + selecttid);
//                        }
//                        for (int i = 0; i < selectedContactName.size(); i++) {
//
//                            tname.append(selectedContactName.get(i));
//                            if (i != selectedContactName.size() - 1) {
//                                tname.append(",");
//                            }
//                            selectname = tname.toString();
//                            Log.e("seledidddd", "" + selectname);
//                        }
                    } else {
                        contactListingResponseArrayList.get(position).setInvited("0");
                       // contactCount--;

                        chk_selectAll.setChecked(false);
                        selectAllCheck="none";

//                        selectedteacherid = contactListingResponseArrayList.get(position).getId();
//                        selectedContactId.remove(selectedteacherid);
//                        selectedContactName.remove(contactListingResponseArrayList.get(position).getUsername());
//                        Log.e("teacher id---", "" + selectedteacherid);
//                        selectedContactUrl.remove(contactListingResponseArrayList.get(position).getPicture());
//
//                        tid = new StringBuilder();
//                        tname = new StringBuilder();
//                        for (int i = 0; i < selectedContactId.size(); i++) {
//
//                            tid.append(selectedContactId.get(i));
//                            if (i != selectedContactId.size() - 1) {
//                                tid.append(",");
//                            }
//                            selecttid = tid.toString();
//                            Log.e("seledidddd", "" + selecttid);
//                        }
//                        for (int i = 0; i < selectedContactName.size(); i++) {
//
//                            tname.append(selectedContactName.get(i));
//                            if (i != selectedContactName.size() - 1) {
//                                tname.append(",");
//                            }
//                            selectname = tname.toString();
//                            Log.e("seledidddd", "" + selectname);
//                        }
                    }
                }
            });

        }

        public ArrayList<ContactsResponse.ContactsDataModal> returnList() {
            return contactListingResponseArrayList;
        }

        public void removeAt(int position) {
            contactListingResponseArrayList.remove(position);
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return contactListingResponseArrayList.size();
        }
    }

    public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> {
        private Context context;
       String contactUrl;
       private ArrayList<ContactNameResponseModal>recyclerList;
        public class MyViewHolder extends RecyclerView.ViewHolder{
            public ImageView image;
            public TextView tv_imageName;
            public String imageName;
            public MyViewHolder(View view){
                super(view);
                image = (ImageView) view.findViewById(R.id.imageViewName);
                tv_imageName=(TextView)view.findViewById(R.id.image_name);
            }
        }

        public ContactsAdapter(Context context,ArrayList<ContactNameResponseModal>recyclerList){
            this.context= context;
            this.recyclerList=recyclerList;
        }
        @Override
        public ContactsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_images,parent,false);
            return new ContactsAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ContactsAdapter.MyViewHolder holder, int position) {
            final ContactNameResponseModal listModel = recyclerList.get(position);
            contactUrl= listModel.getPicture();
            if (contactUrl.isEmpty()) {
                Picasso.with(getActivity()).load(R.drawable.no_profile_grey_circle_bg).resize(215, 215).into(holder.image);
            } else {
                Picasso.with(getActivity()).load(contactUrl).resize(215, 215).into(holder.image);
            }

            holder.tv_imageName.setText(listModel.getUsername());
        }

        public void removeAt(int position){
            recyclerList.remove(position);
            notifyItemRemoved(position);
        }
        @Override
        public int getItemCount() {
            return recyclerList.size();
        }
    }





}
