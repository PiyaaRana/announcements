package com.zoptal.announcements.fragments;

/**
 * Created by Abc on 10/24/2017.
 */

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.zoptal.announcements.R;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.base.MyApp;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.eventBusClasses.CalendarDateSelectedEvent;
import com.zoptal.announcements.utils.DateConvertor;

import org.json.JSONArray;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;


@TargetApi(3)

public class CustomCalendar extends Fragment implements View.OnClickListener {

    private static final String tag = "MyCalendarActivity";
    String date_month_year, s, cal_fragment = "false",calendar_response;
    JSONArray jsonArraynew;
    private TextView currentMonth;
    private Button selectedDayMonthYearButton;
    private ImageView prevMonth;
    private ImageView nextMonth;
    private GridView calendarView;
    private GridCellAdapter adapter;
    private java.util.Calendar _calendar,cDate;
    String accesstoken, theday;
    Boolean flag = false;
    ProgressDialog pd;
    BasicActivity activity;
    @SuppressLint("NewApi")
    private int month, year;
    @SuppressWarnings("unused")
    @SuppressLint({"NewApi", "NewApi", "NewApi", "NewApi"})
    private final DateFormat dateFormatter = new DateFormat();
    private static final String dateTemplate = "MMMM , yyyy";
    View view;
    private boolean isFragmnetVisible=false;
    private int  pHr, pMin,cHr, cMin;
    TimePickerDialog timePickerDialog_from,timePickerDialog_to;
    private String filePath,format_from,format_to,eventtimeFrom,eventtimeTo;
    private TextView txtview_startTime,txtview_endTime;
    private MyActivity myActivity;
    private String EditCheck="true";
    private static final String timeTemplate = "MMMM , yyyy";
    private String firstdate="00",startTimeString,endTimeString,dateString,buttonValue="true",monthvalue="00";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.calendar, container, false);
        isFragmnetVisible=true;
        activity = (BasicActivity) getActivity();
        EventBus.getDefault().register(this);
        _calendar = java.util.Calendar.getInstance(Locale.getDefault());
        cDate= Calendar.getInstance();
        pHr=cHr= _calendar.get(Calendar.HOUR_OF_DAY);
        pMin=cMin=_calendar.get(Calendar.MINUTE);
        month = _calendar.get(java.util.Calendar.MONTH) + 1;
        year = _calendar.get(java.util.Calendar.YEAR);
        Log.d(tag, "Calendar Instance:= " + "Month: " + month + " " + "Year: "
                + year);
        /*selectedDayMonthYearButton = (Button) this
                .findViewById(R.id.selectedDayMonthYear);
        selectedDayMonthYearButton.setText("Selected: ");*/
        prevMonth = (ImageView) view.findViewById(R.id.prevMonth);
        prevMonth.setOnClickListener(this);
        currentMonth = (TextView) view.findViewById(R.id.currentMonth);
        currentMonth.setText(DateFormat.format(dateTemplate,
                _calendar.getTime()));
        nextMonth = (ImageView) view.findViewById(R.id.nextMonth);
        nextMonth.setOnClickListener(this);
        txtview_startTime=(TextView)view.findViewById(R.id.tv_starttime);
        txtview_startTime.setOnClickListener(this);
        txtview_endTime=(TextView)view.findViewById(R.id.tv_endtime);
        txtview_endTime.setOnClickListener(this);
        calendarView = (GridView) view.findViewById(R.id.calendar);

        Bundle bundle= getArguments();
        if(bundle!=null){
            EditCheck= bundle.getString("editCheck");
        }
        if(EditCheck.equalsIgnoreCase("false")) {
            buttonValue = "false";
            txtview_startTime.setEnabled(false);
            txtview_endTime.setEnabled(false);
            prevMonth.setVisibility(View.INVISIBLE);
            nextMonth.setVisibility(View.INVISIBLE);
            startTimeString = ((MyApp) getActivity().getApplicationContext()).getStartTime();
            endTimeString = ((MyApp) getActivity().getApplicationContext()).getEndTime();
            dateString = ((MyApp) getActivity().getApplicationContext()).getDateValue();
            DateConvertor obj = new DateConvertor();
            String eventdate = obj.changeDateFormat(dateString);
            String eventmonth = obj.dateConvertorCalendar(dateString);
            Log.e("datevaluessss", eventdate + " " + eventmonth + " " + dateString);
            dateString = eventdate + "-" + " ";
            DateConvertor obj1 = new DateConvertor();
            String eventstarttime = obj1.changeTimeFormatRecieve(startTimeString);
            Log.e("datevalue", eventstarttime);
            DateConvertor obj2 = new DateConvertor();
            String eventendtime = obj2.changeTimeFormatRecieve(endTimeString);
            Log.e("datevalue", eventendtime);

            txtview_startTime.setText(eventstarttime);
            txtview_endTime.setText(eventendtime);
            if (dateString != null && !dateString.isEmpty()) {
                String[] datearray = dateString.split("-");
                firstdate = datearray[2];
                monthvalue = datearray[1];
                month = Integer.parseInt(monthvalue);
                Log.e("datemonth", firstdate + " " + month);
                setGridCellAdapterToDate(month, year, buttonValue);
                currentMonth.setText(eventmonth);
            }
        }
      else  if(EditCheck.equalsIgnoreCase("update")){
            startTimeString=((MyApp)getActivity().getApplicationContext()).getStartTime();
            endTimeString=((MyApp)getActivity().getApplicationContext()).getEndTime();
            dateString=((MyApp)getActivity().getApplicationContext()).getDateValue();
            DateConvertor obj = new DateConvertor();
            DateConvertor obj1 = new DateConvertor();
            String eventdate = obj.changeDateFormatRecieved(dateString);
            String eventmonth=obj1.monthConvertorCalendar(dateString);
            Log.e("datevalue", eventdate);
            dateString=dateString+"-"+" ";
            DateConvertor obj2 = new DateConvertor();
            String eventstarttime = obj2.changeTimeFormatRecieve(startTimeString);
            Log.e("datevalue", eventstarttime);
            DateConvertor obj3 = new DateConvertor();
            String eventendtime = obj3.changeTimeFormatRecieve(endTimeString);
            Log.e("datevalue", eventendtime);

            txtview_startTime.setText(eventstarttime);
            txtview_endTime.setText(eventendtime);
            String[] datearray = dateString.split("-");
            firstdate=datearray[2];
            Log.e("datevalues",firstdate);
            monthvalue=datearray[1];
            month=Integer.parseInt(monthvalue);
            Log.e("datemonth",firstdate+" "+month);
            setGridCellAdapterToDate(month,year,buttonValue);
            currentMonth.setText(eventmonth);
            String[] stimearray = startTimeString.split(":");
            String hourtime= stimearray[0];
            String mintime= stimearray[1];
            String frmtime= stimearray[2];
            cHr=Integer.parseInt(hourtime);
            cMin=Integer.parseInt(mintime);
            if(eventstarttime.contains("A")){
                format_from="AM";
            }
            else {
                format_from="PM";
            }
            String[] etimearray = startTimeString.split(":");
            String ehourtime= etimearray[0];
            String emintime= etimearray[1];
            String efrmtime= etimearray[2];
            pHr=Integer.parseInt(ehourtime);
            pMin=Integer.parseInt(emintime);
            if(eventendtime.contains("A")){
                format_to="AM";
            }
            else {
                format_to="PM";
            }
        }
        else {
            setGridCellAdapterToDate(month, year, buttonValue);
        }
            return view;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFragmnetVisible=false;
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(CalendarDateSelectedEvent event){
        String currentDate = event.getCurrentDate();
        String showButtons=event.getShowButtons();
        if(currentDate.equalsIgnoreCase("true")){

        }
        else {
            txtview_startTime.setText("");
            txtview_endTime.setText("");
            firstdate="00";
            adapter.notifyDataSetChanged();
            calendarView.setAdapter(adapter);


        }
        if(showButtons.equalsIgnoreCase("false")){
            txtview_endTime.setEnabled(false);
            txtview_startTime.setEnabled(false);
            prevMonth.setVisibility(View.INVISIBLE);
            nextMonth.setVisibility(View.INVISIBLE);
            buttonValue="false";
          setGridCellAdapterToDate(month,year,buttonValue);

        }
        else if(showButtons.equalsIgnoreCase("true")){
            txtview_endTime.setEnabled(true);
            txtview_startTime.setEnabled(true);
            prevMonth.setVisibility(View.VISIBLE);
            nextMonth.setVisibility(View.VISIBLE);
            prevMonth.setEnabled(true);
            nextMonth.setEnabled(true);
            calendarView.setEnabled(true);
            buttonValue="true";
            setGridCellAdapterToDate(month,year,buttonValue);
        }
        //you can now set this date to view.
    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;
        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();
        float x = 1;
        if (items > columns) {
            x = items / columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }
        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);
    }

    /**
     * @param month
     * @param year
     */
    private void setGridCellAdapterToDate(int month, int year,String buttonValue) {
        adapter = new GridCellAdapter(getActivity().getApplicationContext(),
                R.id.calendar_day_gridcell, month, year,buttonValue);
        _calendar.set(year, month - 1, _calendar.get(java.util.Calendar.DAY_OF_MONTH));
        currentMonth.setText(DateFormat.format(dateTemplate,
                _calendar.getTime()));

        adapter.notifyDataSetChanged();
        calendarView.setAdapter(adapter);
        setGridViewHeightBasedOnChildren(calendarView, 7);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevMonth:
                if (month <= 1) {
                    month = 12;
                    year--;
                } else {
                    if (month <= cDate.get(Calendar.MONTH)+1) {
                        prevMonth.setEnabled(false);
                    } else {

                        month--;
                        Log.d(tag, "Setting Prev Month in GridCellAdapter: " + "Month: "
                                + month + " Year: " + year);
                        setGridCellAdapterToDate(month, year, buttonValue);
                    }
                }
                break;
            case R.id.nextMonth:
                prevMonth.setEnabled(true);
                if (month > 11) {
                    month = 1;
                    year++;
                } else {
                    month++;
                }

                Log.d(tag, "Setting Next Month in GridCellAdapter: " + "Month: "
                        + month + " Year: " + year);
                setGridCellAdapterToDate(month, year,buttonValue);
                break;
            case R.id.tv_starttime:
                timePickerDialog_from = new TimePickerDialog(getActivity(), onTimeSetFrom, cHr, cMin, false);
                timePickerDialog_from.show();
                break;
            case R.id.tv_endtime:
                timePickerDialog_to = new TimePickerDialog(getActivity(), onTimeSetTo, pHr, pMin, false);
                timePickerDialog_to.show();
                break;
        }
    }

    @Override
    public void onDestroy() {
        Log.d(tag, "Destroying View …");
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    // Inner Class
    public class GridCellAdapter extends BaseAdapter  {
        private static final String tag = "GridCellAdapter";
        private final Context _context;
        private final List<String> list;
        private static final int DAY_OFFSET = 1;
        private final String[] weekdays = new String[]{"Sun", "Mon", "Tue",
                "Wed", "Thu", "Fri", "Sat"};
        private final String[] months = {"January", "February", "March",
                "April", "May", "June", "July", "August", "September",
                "October", "November", "December"};
        private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30,
                31, 30, 31};
        private int daysInMonth;
        private int currentDayOfMonth;
        private int currentWeekDay;
        private Button gridcell;
        private TextView num_events_per_day;
        private final HashMap<String, Integer> eventsPerMonthMap;
        private final SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "dd-MMM-yyyy");
        private ArrayList<String> datesarray = new ArrayList<String>();
        private String button_type,buttonValue;
        private RelativeLayout datelayout;
        private ImageView iv_shedule;

        // Days in Current Month
        public GridCellAdapter(Context context, int textViewResourceId,
                               int month, int year,String buttonValue) {
            super();
            this._context = context;
            this.buttonValue=buttonValue;
            this.list = new ArrayList<String>();
            Log.d(tag, "==> Passed in Date FOR Month: " + month + " "
                    + "Year: " + year);
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            setCurrentDayOfMonth(calendar.get(java.util.Calendar.DAY_OF_MONTH));
            setCurrentWeekDay(calendar.get(java.util.Calendar.DAY_OF_WEEK));
            Log.d(tag, "New Calendar:= " + calendar.getTime().toString());
            Log.d(tag, "CurrentDayOfWeek :" + getCurrentWeekDay());
            Log.d(tag, "CurrentDayOfMonth :" + getCurrentDayOfMonth());
// Print Month
            printMonth(month, year);
// Find Number of Events
            eventsPerMonthMap = findNumberOfEventsPerMonth(year, month);
        }
        private String getMonthAsString(int i) {
            return months[i];
        }
        private String getWeekDayAsString(int i) {
            return weekdays[i];
        }
        private int getNumberOfDaysOfMonth(int i) {
            return daysOfMonth[i];
        }
        public String getItem(int position) {
            return list.get(position);
        }
        @Override
        public int getCount() {
            return list.size();
        }
        /**
         * Prints Month
         *
         * @param mm
         * @param yy
         */
        private void printMonth(int mm, int yy) {
            Log.d(tag, "==> printMonth: mm: " + mm + " " + "yy: " + yy);
            int trailingSpaces = 0;
            int daysInPrevMonth = 0;
            int prevMonth = 0;
            int prevYear = 0;
            int nextMonth = 0;
            int nextYear = 0;
            int currentMonth = mm - 1;
            String currentMonthName = getMonthAsString(currentMonth);
            daysInMonth = getNumberOfDaysOfMonth(currentMonth);

            Log.d(tag, "Current Month: " + " " + currentMonthName + " having "
                    + daysInMonth + " days.");

            GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);
            Log.d(tag, "Gregorian Calendar:= " + cal.getTime().toString());

            if (currentMonth == 11) {
                prevMonth = currentMonth - 1;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 0;
                prevYear = yy;
                nextYear = yy + 1;
                Log.d(tag, "*->PrevYear: " + prevYear + " PrevMonth:"
                        + prevMonth + " NextMonth: " + nextMonth
                        + " NextYear: " + nextYear);
            } else if (currentMonth == 0) {
                prevMonth = 11;
                prevYear = yy - 1;
                nextYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 1;
                Log.d(tag, "**-> PrevYear: " + prevYear + " PrevMonth:"
                        + prevMonth + " NextMonth: " + nextMonth
                        + " NextYear: " + nextYear);
            } else {
                prevMonth = currentMonth - 1;
                nextMonth = currentMonth + 1;
                nextYear = yy;
                prevYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                Log.d(tag, "***—> PrevYear: " + prevYear + " PrevMonth:"
                        + prevMonth + " NextMonth: " + nextMonth
                        + " NextYear: " + nextYear);
            }

            int currentWeekDay = cal.get(java.util.Calendar.DAY_OF_WEEK) - 1;
            trailingSpaces = currentWeekDay;
            Log.d(tag, "Week Day:" + currentWeekDay + " is "
                    + getWeekDayAsString(currentWeekDay));
            Log.d(tag, "No. Trailing space to Add: " + trailingSpaces);
            Log.d(tag, "No. of Days in Previous Month: " + daysInPrevMonth);

            if (cal.isLeapYear(cal.get(java.util.Calendar.YEAR)))
                if (mm == 2)
                    ++daysInMonth;
                else if (mm == 3)
                    ++daysInPrevMonth;
// Trailing Month days
            for (int i = 0; i < trailingSpaces; i++) {
                Log.d(tag,
                        "PREV MONTH:= "
                                + prevMonth
                                + " => "
                                + getMonthAsString(prevMonth)
                                + " "
                                + String.valueOf((daysInPrevMonth
                                - trailingSpaces + DAY_OFFSET)
                                + i));
                list.add(String
                        .valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET)
                                + i)
                        + "-GREY"
                        + "-"
                        + getMonthAsString(prevMonth)
                        + "-"
                        + prevYear);
            }

// Current Month Days
            for (int i = 1; i <= daysInMonth; i++) {
                Log.d(currentMonthName, String.valueOf(i) + " "
                        + getMonthAsString(currentMonth) + " " + yy);
                if (i == getCurrentDayOfMonth()) {
                    list.add(String.valueOf(i) + "-BLUE" + "-"
                            + getMonthAsString(currentMonth) + "-" + yy);
                } else {
                    list.add(String.valueOf(i) + "-WHITE" + "-"
                            + getMonthAsString(currentMonth) + "-" + yy);
                }
            }

// Leading Month days
            for (int i = 0; i < list.size() % 7; i++) {
                Log.d(tag, "NEXT MONTH:= " + getMonthAsString(nextMonth));
                list.add(String.valueOf(i + 1) + "-GREY" + "-"
                        + getMonthAsString(nextMonth) + "-" + nextYear);
            }
        }

        /**
         * NOTE: YOU NEED TO IMPLEMENT THIS PART Given the YEAR, MONTH, retrieve
         * ALL entries from a SQLite database for that month. Iterate over the
         * List of All entries, and get the dateCreated, which is converted into
         * day.
         *
         * @param year
         * @param month
         * @return
         */
        private HashMap<String, Integer> findNumberOfEventsPerMonth(int year,
                                                                    int month) {
            HashMap<String, Integer> map = new HashMap<String, Integer>();
            return map;
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) _context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.screen_gridcell, parent, false);
            }

// Get a reference to the Day gridcell
            gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
             iv_shedule = (ImageView) row.findViewById(R.id.iv_shedule);
            datelayout= (RelativeLayout) row.findViewById(R.id.date_layout);
            if(buttonValue.equalsIgnoreCase("false")){
                gridcell.setEnabled(false);
            }
            else {
                gridcell.setEnabled(true);
            }

            gridcell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    date_month_year = (String) view.getTag();
                    DateConvertor obj = new DateConvertor();
                    String eventdate = obj.changeDateFormat(date_month_year);
                    Log.e("datevalue", eventdate);
                    //selectedDayMonthYearButton.setText("Selected: " + date_month_year);
                    ((MyApp)getActivity().getApplicationContext()).setDateValue(eventdate);
                    String[] datearray = date_month_year.split("-");
                    firstdate=datearray[0];
                    String monthvalue=datearray[1];
                    adapter.notifyDataSetChanged();
                    calendarView.setAdapter(adapter);
                    //iv_shedule.setVisibility(View.VISIBLE);
                    //adapter.notifyDataSetChanged();
                    Log.e("Selected date", date_month_year);
                    try {
                        Date parsedDate = dateFormatter.parse(date_month_year);
                        Log.d(tag, "Parsed Date: " + parsedDate.toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
// ACCOUNT FOR SPACING
            Log.d(tag, "Current Day: " + getCurrentDayOfMonth());
            String[] day_color = list.get(position).split("-");
            theday = day_color[0];
            String themonth = day_color[2];
            String theyear = day_color[3];
            if ((!eventsPerMonthMap.isEmpty()) && (eventsPerMonthMap != null)) {
                if (eventsPerMonthMap.containsKey(theday)) {
                    num_events_per_day = (TextView) row
                            .findViewById(R.id.num_events_per_day);
                    Integer numEvents = (Integer) eventsPerMonthMap.get(theday);
                    num_events_per_day.setText(numEvents.toString());
                }
            }
// Set the Day GridCell
            gridcell.setText(theday);
            datesarray.add(theday);
            gridcell.setTag(theday + "-" + themonth + "-" + theyear);
            Log.d(tag, "Setting GridCell " + theday + "-" + themonth + "-"
                    + theyear);

            if(EditCheck.equalsIgnoreCase("false")||EditCheck.equalsIgnoreCase("update")) {

                    if (firstdate.substring(0, 1).equalsIgnoreCase("0")) {
                        firstdate = firstdate.replace("0", "");
                        if(firstdate.equalsIgnoreCase("")){
                            firstdate="0";
                        }
                        //Log.e("datevaluess", firstdate);
                        //Log.e("datevaluess", firstdate + " " + theday);
                    }

            }
                    if (theday.equals(firstdate)) {
                    //Log.e("datevaluesss", firstdate + " " + theday);
                    iv_shedule.setVisibility(View.VISIBLE);
                }


          //if (datesarray.get(position).equals("5") || datesarray.get(position).equals("8") || datesarray.get(position).equals("9")
//                    || datesarray.get(position).equals("11") || datesarray.get(position).equals("17") || datesarray.get(position).equals("19")
//                    || datesarray.get(position).equals("21")
//                    || datesarray.get(position).equals("22") || datesarray.get(position).equals("23")) {
//                iv_shedule.setVisibility(View.VISIBLE);
//                //gridcell.setTextColor(Color.parseColor("#ff0000"));
//
//            }
          if (day_color[1].equals("GREY")) {
                gridcell.setTextColor(Color.parseColor("#ffffff"));
                iv_shedule.setVisibility(View.GONE);
            }
            if (day_color[1].equals("WHITE")) {
                gridcell.setTextColor(getResources().getColor(
                        R.color.grey));
            }
            if (day_color[1].equals("BLUE")) {
                gridcell.setTextColor(getResources()
                        .getColor(R.color.grey));
            }
            if(month==cDate.get(Calendar.MONTH)+1){
                if(Integer.parseInt(theday)<cDate.get(Calendar.DAY_OF_MONTH)){
                    gridcell.setEnabled(false);
                    //gridcell.setBackgroundResource(R.color.grey);
                    gridcell.setTextColor(getResources().getColor(R.color.view_line));
                }
            }
            // theday.equals(datesarray.get(position).)
            return row;
        }

       // @Override
       // public void onClick(View view) {
//            date_month_year = (String) view.getTag();
//            //selectedDayMonthYearButton.setText("Selected: " + date_month_year);
//            String[] datearray = date_month_year.split("-");
//            String firstdate=datearray[0];
//            String monthvalue=datearray[1];
//
//            //iv_shedule.setVisibility(View.VISIBLE);
//            //adapter.notifyDataSetChanged();
//            Log.e("Selected date", date_month_year);
//            try {
//                Date parsedDate = dateFormatter.parse(date_month_year);
//                Log.d(tag, "Parsed Date: " + parsedDate.toString());
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }

      //  }
        public int getCurrentDayOfMonth() {
            return currentDayOfMonth;
        }
        private void setCurrentDayOfMonth(int currentDayOfMonth) {
            this.currentDayOfMonth = currentDayOfMonth;
        }
        public void setCurrentWeekDay(int currentWeekDay) {
            this.currentWeekDay = currentWeekDay;
        }
        public int getCurrentWeekDay() {
            return currentWeekDay;
        }
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    private TimePickerDialog.OnTimeSetListener onTimeSetFrom = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute ) {

            if (hourOfDay == 0) {

                hourOfDay += 12;

                format_from = "AM";
            } else if (hourOfDay == 12) {

                format_from = "PM";

            } else if (hourOfDay > 12) {

                hourOfDay -= 12;

                format_from = "PM";
            } else {

                format_from = "AM";
            }
            cHr = hourOfDay;
            cMin = minute;

            updateDisplayTimeFrom(cHr, cMin, format_from);
        }
    };


    private TimePickerDialog.OnTimeSetListener onTimeSetTo = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            if (hourOfDay == 0) {

                hourOfDay += 12;

                format_to = "AM";
            } else if (hourOfDay == 12) {

                format_to = "PM";

            } else if (hourOfDay > 12) {

                hourOfDay -= 12;

                format_to = "PM";

            } else {

                format_to = "AM";
            }

            pHr = hourOfDay;
            pMin = minute;

            Log.e("cvaluess",""+pHr);

            updateDisplayTimeTo(pHr, pMin,format_to);


        }
    };
    private void updateDisplayTimeFrom(int hr, int min,String format) {
        txtview_startTime.setText(String.format("%02d:%02d%s", hr, min," "+format));
        Log.e("timeeventfrom",""+txtview_startTime.getText().toString());
        txtview_startTime.setTextColor(Color.BLACK);
        DateConvertor obj1 = new DateConvertor();
         String eventStartTime = obj1.changeTimeFormatSentComplete(txtview_startTime.getText().toString());
        Log.e("timeeventfrom",""+txtview_startTime.getText().toString()+" "+eventStartTime);
        ((MyApp)getActivity().getApplicationContext()).setStartTime(eventStartTime);
    }

    private void updateDisplayTimeTo(int hr, int min,String format) {
        if (((cHr > pHr )||(cHr==pHr && cMin==pMin)) && (format_to.equals(format_from))&& cHr!=12) {
            Log.e("cvalue", "" + cHr + "" + pHr+" "+cMin+" "+pMin);
            myActivity.showOneErrorDialog("Enter valid time.","OK",getActivity());
            txtview_endTime.setText("");
        } else {
            txtview_endTime.setText(String.format("%02d:%02d%s", hr, min," "+format));
            Log.e("timeeventto", "" + txtview_endTime.getText().toString());
            txtview_endTime.setTextColor(Color.BLACK);
            DateConvertor obj1 = new DateConvertor();
            String eventEndTime = obj1.changeTimeFormatSentComplete(txtview_endTime.getText().toString());
            Log.e("timeeventto",""+txtview_startTime.getText().toString()+" "+eventEndTime);
            ((MyApp)getActivity().getApplicationContext()).setEndTime(eventEndTime);
        }
    }

}




