package com.zoptal.announcements.fcm;

/**
 * Created by Abc on 12/18/2017.
 */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.zoptal.announcements.R;
import com.zoptal.announcements.mainController.BasicActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String title, body,badgecount;
    public static final String TAG="firebaseMessaging";
    @Override
    public void onCreate() {
        super.onCreate();
    }

    //@RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG,"FROM:"+remoteMessage.getFrom());
        if(remoteMessage.getData().size()>0){
            Log.e(TAG,"MESG DATA"+remoteMessage.getData());
            sendNotification(remoteMessage.getData().get("description"));
            Log.e("desc",remoteMessage.getData().get("description"));
//            title= remoteMessage.getData().get("description");
//            body= remoteMessage.getData().get("type");
//            badgecount=remoteMessage.getData().get("badge_count");
//            Log.e("badge",badgecount);
            Intent intent = new Intent("custom-event-name");
            // You can also include some extra data.
            //intent.putExtra("badge", 0);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        }
        if(remoteMessage.getNotification()!= null){
            Log.e(TAG,"NOTIFY DATA"+remoteMessage.getNotification().getBody());
        }
    }
    private void sendNotification(String body) {
        Log.e("descs",body);
        int notifyID = 1;
        String CHANNEL_ID = "my_channel_03";// The id of the channel.
        CharSequence name = getString(R.string.default_notification_channel_name);// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }
        Intent intent= new Intent(this,BasicActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pending=  PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);
        Uri notificationsound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.app_icon);

        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notification = new Notification.Builder(this)
                    .setContentTitle("Announcements")
                    .setContentText(body)
                    .setSmallIcon(R.mipmap.app_icon)
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(pending)
                    .setSound(notificationsound)
                    .build();
        }
        else {
            notification = new Notification.Builder(this)
                    .setContentTitle("Announcements")
                    .setContentText(body)
                    .setSmallIcon(R.mipmap.app_icon)
                    .setContentIntent(pending)
                    .setSound(notificationsound)
                    .build();
        }
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannel);
        }
// Issue the notification.
        mNotificationManager.notify(notifyID , notification);
    }
}