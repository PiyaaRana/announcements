package com.zoptal.announcements.fcm;

/**
 * Created by Abc on 12/18/2017.
 */

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService{

    public static final String TAG="firebaseInstance";

    @Override
    public void onTokenRefresh() {
        String refreshToken= FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG,"refreshToken:"+refreshToken);
    }
}

