package com.zoptal.announcements.responseClasses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CalendarEventResponse {
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("result")
    private boolean result;

    @SerializedName("code")
    private String code;


    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;


    public ArrayList<CalendarEventDataModal> getCalendarArrayList() {
        return calendarArrayList;
    }

    public void setCalendarArrayList(ArrayList<CalendarEventDataModal> calendarArrayList) {
        this.calendarArrayList = calendarArrayList;
    }

    @SerializedName("data")
    private ArrayList<CalendarEventDataModal> calendarArrayList;
    public class CalendarEventDataModal {
        @SerializedName("id")
        private String id;

        @SerializedName("user_id")
        private String userId;

        @SerializedName("category")
        private String category;

        @SerializedName("name")
        private String eventName;

        @SerializedName("contact_name")
        private String contactName;

        @SerializedName("contact_phone")
        private String contactPhone;

        @SerializedName("party_size")
        private String partySize;

        @SerializedName("description")
        private String description;

        @SerializedName("event_date")
        private String eventDate;

        @SerializedName("start_time")
        private String startTime;

        @SerializedName("end_time")
        private String endTime;
        @SerializedName("creator_pic")
        private String creator_pic;
        @SerializedName("event_type")
        private String eventType;
        @SerializedName("location")
        private String eventLocation;
        @SerializedName("latitude")
        private String eventLatitude;
        @SerializedName("longitude")
        private String eventLongitude;
        @SerializedName("picture")
        private String userPic;
        @SerializedName("invite_id")
        private String inviteId;
        @SerializedName("invite_status")
        private String goingStatus;
        @SerializedName("in_calendar")
        private String calCheck;
        @SerializedName("contacts")
        private ArrayList<ContactEventDataModal> contactDetails;

        public String getInviteId() {
            return inviteId;
        }

        public void setInviteId(String inviteId) {
            this.inviteId = inviteId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCreator_pic() {
            return creator_pic;
        }

        public void setCreator_pic(String creator_pic) {
            this.creator_pic = creator_pic;
        }

        public String getPartySize() {
            return partySize;
        }

        public void setPartySize(String partySize) {
            this.partySize = partySize;
        }

        public String getEventLocation() {
            return eventLocation;
        }

        public void setEventLocation(String eventLocation) {
            this.eventLocation = eventLocation;
        }

        public String getEventType() {
            return eventType;
        }

        public void setEventType(String eventType) {
            this.eventType = eventType;
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getContactName() {
            return contactName;
        }

        public void setContactName(String contactName) {
            this.contactName = contactName;
        }

        public String getContactPhone() {
            return contactPhone;
        }

        public void setContactPhone(String contactPhone) {
            this.contactPhone = contactPhone;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }

        public String getEventLatitude() {
            return eventLatitude;
        }

        public void setEventLatitude(String eventLatitude) {
            this.eventLatitude = eventLatitude;
        }

        public String getEventLongitude() {
            return eventLongitude;
        }

        public String getCalCheck() {
            return calCheck;
        }

        public void setCalCheck(String calCheck) {
            this.calCheck = calCheck;
        }

        public String getGoingStatus() {
            return goingStatus;
        }

        public void setGoingStatus(String goingStatus) {
            this.goingStatus = goingStatus;
        }

        public void setEventLongitude(String eventLongitude) {
            this.eventLongitude = eventLongitude;
        }

        public String getUserPic() {
            return userPic;
        }

        public void setUserPic(String userPic) {
            this.userPic = userPic;
        }

        public ArrayList<ContactEventDataModal> getContactDetails() {
            return contactDetails;
        }

        public void setContactDetails(ArrayList<ContactEventDataModal> contactDetails) {
            this.contactDetails = contactDetails;
        }
    }

    public class ContactEventDataModal {
        @SerializedName("id")
        private String contact_id;

        @SerializedName("receiver_name")
        private String receiverName;

        @SerializedName("receiver_picture")
        private String receiverPic;

        public String getContact_id() {
            return contact_id;
        }

        public void setContact_id(String contact_id) {
            this.contact_id = contact_id;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public String getReceiverPic() {
            return receiverPic;
        }

        public void setReceiverPic(String receiverPic) {
            this.receiverPic = receiverPic;
        }
    }
}
