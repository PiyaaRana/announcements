package com.zoptal.announcements.responseClasses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EventResponse {
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("result")
    private boolean result;

    @SerializedName("code")
    private String code;

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    public EventDataModal getEventDataModal() {
        return eventDataModal;
    }

    public void setEventDataModal(EventDataModal eventDataModal) {
        this.eventDataModal = eventDataModal;
    }

    @SerializedName("data")
    private EventResponse.EventDataModal eventDataModal;

    public class EventDataModal {
        @SerializedName("id")
        private String id;

        @SerializedName("user_id")
        private String userId;

        @SerializedName("category")
        private String category;

        @SerializedName("name")
        private String eventName;

        @SerializedName("contact_name")
        private String contactName;

        @SerializedName("contact_phone")
        private String contactPhone;

        @SerializedName("party_size")
        private String partySize;

        @SerializedName("description")
        private String description;

        @SerializedName("event_date")
        private String eventDate;

        @SerializedName("start_time")
        private String startTime;

        @SerializedName("end_time")
        private String endTime;

        @SerializedName("event_type")
        private String eventType;

        @SerializedName("location")
        private String eventLocation;

        @SerializedName("latitude")
        private String eventLatitude;

        @SerializedName("longitude")
        private String eventLongitude;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


        public String getPartySize() {
            return partySize;
        }

        public void setPartySize(String partySize) {
            this.partySize = partySize;
        }

        public String getEventLocation() {
            return eventLocation;
        }

        public void setEventLocation(String eventLocation) {
            this.eventLocation = eventLocation;
        }

        public String getEventType() {
            return eventType;
        }

        public void setEventType(String eventType) {
            this.eventType = eventType;
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getContactName() {
            return contactName;
        }

        public void setContactName(String contactName) {
            this.contactName = contactName;
        }

        public String getContactPhone() {
            return contactPhone;
        }

        public void setContactPhone(String contactPhone) {
            this.contactPhone = contactPhone;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }

        public String getEventLatitude() {
            return eventLatitude;
        }

        public void setEventLatitude(String eventLatitude) {
            this.eventLatitude = eventLatitude;
        }

        public String getEventLongitude() {
            return eventLongitude;
        }

        public void setEventLongitude(String eventLongitude) {
            this.eventLongitude = eventLongitude;
        }
    }
}

