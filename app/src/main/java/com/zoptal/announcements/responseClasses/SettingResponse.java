package com.zoptal.announcements.responseClasses;

import com.google.gson.annotations.SerializedName;

public class SettingResponse {
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("result")
    private boolean result;

    @SerializedName("code")
    private String code;


    @SerializedName("message")
    private String message;

    public SettingsDataModal getSettingsDataModal() {
        return settingsDataModal;
    }

    public void setSettingsDataModal(SettingsDataModal settingsDataModal) {
        this.settingsDataModal = settingsDataModal;
    }

    @SerializedName("data")
    private SettingResponse.SettingsDataModal settingsDataModal;

    public class SettingsDataModal {
        @SerializedName("show_location")
        private String showLocation;

        @SerializedName("show_social_links")
        private String showLinks;
        @SerializedName("show_past_checkin")
        private String showCheckins;

        @SerializedName("show_contact_info")
        private String ShowContact;

        public String getShowCheckins() {
            return showCheckins;
        }

        public void setShowCheckins(String showCheckins) {
            this.showCheckins = showCheckins;
        }

        public String getShowContact() {
            return ShowContact;
        }

        public void setShowContact(String showContact) {
            ShowContact = showContact;
        }

        public String getShowLinks() {
            return showLinks;
        }

        public void setShowLinks(String showLinks) {
            this.showLinks = showLinks;
        }

        public String getShowLocation() {
            return showLocation;
        }

        public void setShowLocation(String showLocation) {
            this.showLocation = showLocation;
        }
    }


}
