package com.zoptal.announcements.responseClasses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProfileResponse {
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("result")
    private boolean result;

    @SerializedName("code")
    private String code;

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    public ProfileDataModal getProfileDataModal() {
        return profileDataModal;
    }

    public void setProfileDataModal(ProfileDataModal profileDataModal) {
        this.profileDataModal = profileDataModal;
    }

    public SettingsShowDataModal getSettingsShowDataModal() {
        return settingsShowDataModal;
    }

    public void setSettingsShowDataModal(SettingsShowDataModal settingsShowDataModal) {
        this.settingsShowDataModal = settingsShowDataModal;
    }

    @SerializedName("data")
    private ProfileResponse.ProfileDataModal profileDataModal;

    public class ProfileDataModal {
        @SerializedName("username")
        private String username;

        @SerializedName("user_id")
        private String userId;

        @SerializedName("email")
        private String userEmail;

        @SerializedName("phone")
        private String userContact;

        @SerializedName("picture")
        private String userPic;

        @SerializedName("location")
        private String userLoc;

        @SerializedName("social_fb_link")
        private String fbLink;

        @SerializedName("social_tw_link")
        private String twLink;

        @SerializedName("social_pin_link")
        private String pinLink;

        @SerializedName("social_insta_link")
        private String instaLink;

        @SerializedName("events")
        private ArrayList<ProfileEventDataModal> profileEventDataModal;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserPic() {
            return userPic;
        }

        public void setUserPic(String userPic) {
            this.userPic = userPic;
        }

        public String getUserContact() {
            return userContact;
        }

        public void setUserContact(String userContact) {
            this.userContact = userContact;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public String getUserLoc() {
            return userLoc;
        }

        public void setUserLoc(String userLoc) {
            this.userLoc = userLoc;
        }

        public String getFbLink() {
            return fbLink;
        }

        public void setFbLink(String fbLink) {
            this.fbLink = fbLink;
        }

        public String getInstaLink() {
            return instaLink;
        }

        public void setInstaLink(String instaLink) {
            this.instaLink = instaLink;
        }

        public String getPinLink() {
            return pinLink;
        }

        public void setPinLink(String pinLink) {
            this.pinLink = pinLink;
        }

        public String getTwLink() {
            return twLink;
        }

        public void setTwLink(String twLink) {
            this.twLink = twLink;
        }

        public ArrayList<ProfileEventDataModal> getProfileEventDataModal() {
            return profileEventDataModal;
        }

        public void setProfileEventDataModal(ArrayList<ProfileEventDataModal> profileEventDataModal) {
            this.profileEventDataModal = profileEventDataModal;
        }
    }

        public class ProfileEventDataModal {
            @SerializedName("id")
            private String id;

            @SerializedName("user_id")
            private String userId;

            @SerializedName("category")
            private String category;

            @SerializedName("name")
            private String eventName;

            @SerializedName("contact_name")
            private String contactName;

            @SerializedName("contact_phone")
            private String contactPhone;

            @SerializedName("party_size")
            private String partySize;

            @SerializedName("description")
            private String description;

            @SerializedName("event_date")
            private String eventDate;

            @SerializedName("start_time")
            private String startTime;

            @SerializedName("end_time")
            private String endTime;

            @SerializedName("event_type")
            private String eventType;

            @SerializedName("location")
            private String eventLocation;

            @SerializedName("latitude")
            private String eventLatitude;

            @SerializedName("longitude")
            private String eventLongitude;

            @SerializedName("picture")
            private String userPic;

            @SerializedName("invite_id")
            private String inviteId;
            @SerializedName("status")
            private String goingStatus;

            @SerializedName("username")
            private String username;


            public String getInviteId() {
                return inviteId;
            }

            public void setInviteId(String inviteId) {
                this.inviteId = inviteId;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }


            public String getPartySize() {
                return partySize;
            }

            public void setPartySize(String partySize) {
                this.partySize = partySize;
            }

            public String getEventLocation() {
                return eventLocation;
            }

            public void setEventLocation(String eventLocation) {
                this.eventLocation = eventLocation;
            }

            public String getEventType() {
                return eventType;
            }

            public void setEventType(String eventType) {
                this.eventType = eventType;
            }

            public String getEventName() {
                return eventName;
            }

            public void setEventName(String eventName) {
                this.eventName = eventName;
            }

            public String getEndTime() {
                return endTime;
            }

            public void setEndTime(String endTime) {
                this.endTime = endTime;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getStartTime() {
                return startTime;
            }

            public void setStartTime(String startTime) {
                this.startTime = startTime;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public String getContactName() {
                return contactName;
            }

            public void setContactName(String contactName) {
                this.contactName = contactName;
            }

            public String getContactPhone() {
                return contactPhone;
            }

            public void setContactPhone(String contactPhone) {
                this.contactPhone = contactPhone;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getEventDate() {
                return eventDate;
            }

            public void setEventDate(String eventDate) {
                this.eventDate = eventDate;
            }

            public String getEventLatitude() {
                return eventLatitude;
            }

            public void setEventLatitude(String eventLatitude) {
                this.eventLatitude = eventLatitude;
            }

            public String getEventLongitude() {
                return eventLongitude;
            }

            public String getGoingStatus() {
                return goingStatus;
            }

            public void setGoingStatus(String goingStatus) {
                this.goingStatus = goingStatus;
            }

            public void setEventLongitude(String eventLongitude) {
                this.eventLongitude = eventLongitude;
            }

            public String getUserPic() {
                return userPic;
            }

            public void setUserPic(String userPic) {
                this.userPic = userPic;
            }


        }
    @SerializedName("user_settings")
    private ProfileResponse.SettingsShowDataModal settingsShowDataModal;

    public class SettingsShowDataModal {
        @SerializedName("show_location")
        private String showLocation;

        @SerializedName("show_social_links")
        private String showLinks;
        @SerializedName("show_past_checkin")
        private String showCheckins;

        @SerializedName("show_contact_info")
        private String ShowContact;

        public String getShowLocation() {
            return showLocation;
        }

        public void setShowLocation(String showLocation) {
            this.showLocation = showLocation;
        }

        public String getShowLinks() {
            return showLinks;
        }

        public void setShowLinks(String showLinks) {
            this.showLinks = showLinks;
        }

        public String getShowCheckins() {
            return showCheckins;
        }

        public void setShowCheckins(String showCheckins) {
            this.showCheckins = showCheckins;
        }

        public String getShowContact() {
            return ShowContact;
        }

        public void setShowContact(String showContact) {
            ShowContact = showContact;
        }
    }


    }

