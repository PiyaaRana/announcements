package com.zoptal.announcements.responseClasses;

import com.google.gson.annotations.SerializedName;

public class SocialLinkResponse {

        public boolean isResult() {
            return result;
        }

        public void setResult(boolean result) {
            this.result = result;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @SerializedName("result")
        private boolean result;

        @SerializedName("code")
        private String code;

        @SerializedName("status")
        private String status;

        @SerializedName("message")
        private String message;

    public SocialDataModal getSocialDataModal() {
        return socialDataModal;
    }

    public void setSocialDataModal(SocialDataModal socialDataModal) {
        this.socialDataModal = socialDataModal;
    }

    @SerializedName("data")
        SocialLinkResponse.SocialDataModal socialDataModal;

        public class SocialDataModal {
            @SerializedName("social_fb_link")
            private String fb_link;

            @SerializedName("social_tw_link")
            private String twitter_link;

            @SerializedName("social_pin_link")
            private String pin_link;

            @SerializedName("social_insta_link")
            private String insta_link;

            public String getFb_link() {
                return fb_link;
            }

            public void setFb_link(String fb_link) {
                this.fb_link = fb_link;
            }

            public String getInsta_link() {
                return insta_link;
            }

            public void setInsta_link(String insta_link) {
                this.insta_link = insta_link;
            }

            public String getPin_link() {
                return pin_link;
            }

            public void setPin_link(String pin_link) {
                this.pin_link = pin_link;
            }

            public String getTwitter_link() {
                return twitter_link;
            }

            public void setTwitter_link(String twitter_link) {
                this.twitter_link = twitter_link;
            }
        }
    }


