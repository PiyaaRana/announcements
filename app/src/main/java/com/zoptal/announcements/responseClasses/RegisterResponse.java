package com.zoptal.announcements.responseClasses;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("result")
    private boolean result;

    @SerializedName("code")
    private String code;


    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;


    public RegisterDataModal getRegisterDataModal() {
        return registerDataModal;
    }

    public void setRegisterDataModal(RegisterDataModal registerDataModal) {
        this.registerDataModal = registerDataModal;
    }

    @SerializedName("data")
    private RegisterDataModal registerDataModal;

    public class RegisterDataModal {
        @SerializedName("user_id")
        private int user_id;

        @SerializedName("access_token")
        private String access_token;

        @SerializedName("email")
        private String email;

        @SerializedName("phone")
        private String phone_no;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        @SerializedName("username")
        private String username;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone_no() {
            return phone_no;
        }

        public void setPhone_no(String phone_no) {
            this.phone_no = phone_no;
        }
    }
}
