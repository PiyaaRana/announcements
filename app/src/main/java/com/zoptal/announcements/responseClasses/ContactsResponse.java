package com.zoptal.announcements.responseClasses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ContactsResponse {
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("result")
    private boolean result;

    @SerializedName("code")
    private String code;

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    public ArrayList<ContactsDataModal> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<ContactsDataModal> arrayList) {
        this.arrayList = arrayList;
    }

    @SerializedName("data")
    private ArrayList<ContactsDataModal> arrayList;
    public class ContactsDataModal {
        @SerializedName("id")
        private String id;

        @SerializedName("access_token")
        private String access_token;

        @SerializedName("email")
        private String email;

        @SerializedName("phone")
        private String phone_no;



        public String getPhone_no() {
            return phone_no;
        }

        public void setPhone_no(String phone_no) {
            this.phone_no = phone_no;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        @SerializedName("username")
        private String username;

        @SerializedName("picture")
        private String picture;

        @SerializedName("invited")
        private String invited;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getInvited() {
            return invited;
        }

        public void setInvited(String invited) {
            this.invited = invited;
        }
    }
}

