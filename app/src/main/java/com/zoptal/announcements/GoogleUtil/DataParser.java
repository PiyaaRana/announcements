package com.zoptal.announcements.GoogleUtil;

/**
 * Created by Abc on 8/17/2017.
 */

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DataParser {

    private HashMap<String, String> getPlace(JSONObject googlePlaceJson)
    {
        HashMap<String, String> googlePlaceMap = new HashMap<>();
        String placeName = "";
        String vicinity= "";
        String latitude= "";
        String longitude="";
        String reference="";
        String duration="";

        Log.d("DataParser","jsonobject ="+googlePlaceJson.toString());


        try {
            if (!googlePlaceJson.isNull("name")) {
                placeName = googlePlaceJson.getString("name");
            }
            if (!googlePlaceJson.isNull("vicinity")) {
                vicinity = googlePlaceJson.getString("vicinity");
            }

            latitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lat");
            longitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lng");

            reference = googlePlaceJson.getString("reference");


            googlePlaceMap.put("place_name", placeName);
            googlePlaceMap.put("vicinity", vicinity);
            googlePlaceMap.put("lat", latitude);
            googlePlaceMap.put("lng", longitude);
            googlePlaceMap.put("reference", reference);



        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return googlePlaceMap;

    }
    private List<HashMap<String, String>> getPlaces(JSONArray jsonArray) {
        List<HashMap<String, String>> placelist= new ArrayList<>();


            int count = jsonArray.length();
            //placelist = new ArrayList<>();
            HashMap<String, String> placeMap = null;

            for (int i = 0; i < count; i++) {
                try {
                    placeMap = getPlace((JSONObject) jsonArray.get(i));
                    placelist.add(placeMap);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



        return placelist;
    }

    public List<HashMap<String, String>> parse(String jsonData)
    {
        JSONArray jsonArray = null;
        JSONObject jsonObject;

        //Log.d("json data", jsonData);
        if(jsonData!=null) {
    try {
        jsonObject = new JSONObject(jsonData);
        if (jsonObject != null) {
            jsonArray = jsonObject.getJSONArray("results");
        }
    } catch (JSONException e) {
        e.printStackTrace();
    }
}
else {
    Log.e("checkkkk","checkkkkk");
}
        return getPlaces(jsonArray);
    }
}

