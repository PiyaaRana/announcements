package com.zoptal.announcements.adapter;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoptal.announcements.R;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.modal.LocationModal;

import java.util.List;


/**
 * Created by Abc on 4/23/2018.
 */

public class PlacesLocationAdapter extends RecyclerView.Adapter<PlacesLocationAdapter.ViewHolder> {


    private Context context;
    private List<LocationModal> myList;
    String name;

    public PlacesLocationAdapter(List<LocationModal> myList, Context context){
        super();
        this.myList=myList;
        this.context = context;
    }

    @Override
    public PlacesLocationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.showamenities_adapter, parent, false);
        PlacesLocationAdapter.ViewHolder viewHolder = new PlacesLocationAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PlacesLocationAdapter.ViewHolder holder, int position) {
        final LocationModal contact = myList.get(position);

        holder.tv_myListName.setText(contact.getStoreName());
        holder.tv_myListDate.setText(contact.getVicinity());

        holder.clickLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String loc= contact.getStoreName()+"+"+contact.getVicinity();
               String map= "http://maps.google.co.in/maps?q="+loc;
                Intent i= new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_myListName;
        private TextView tv_myListNo;
        private TextView tv_myListDate;
        private TextView tv_myListEdit;
        private ImageView edit_event;
        private ImageView img_myListIcon;
        private RelativeLayout clickLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_myListName = (TextView)itemView.findViewById(R.id.mylist_name);

            tv_myListDate = (TextView)itemView.findViewById(R.id.update_text);
            clickLayout=(RelativeLayout)itemView.findViewById(R.id.click_layout);


        }
    }
}
