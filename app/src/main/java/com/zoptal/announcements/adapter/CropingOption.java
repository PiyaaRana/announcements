package com.zoptal.announcements.adapter;

/**
 * Created by Abc on 2/14/2018.
 */

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class CropingOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}