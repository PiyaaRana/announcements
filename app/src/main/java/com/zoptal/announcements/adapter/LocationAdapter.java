package com.zoptal.announcements.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zoptal.announcements.R;
import com.zoptal.announcements.modal.MyListModal;

import java.util.List;


/**
 * Created by Abc on 4/20/2018.
 */



public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {


    private Context context;
    private List<MyListModal> myList;
    String name;

    public LocationAdapter(List<MyListModal> myList, Context context){
        super();
        this.myList=myList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.showamenities_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MyListModal contact = myList.get(position);

        holder.tv_myListName.setText(contact.getListName());

//        holder.tv_myListEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(context instanceof BasicActivity){
//                    ((BasicActivity)context).replaceFragment(new EditListFragment());
//                }
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return myList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_myListName;
        private TextView tv_myListNo;
        private TextView tv_myListDate;
        private TextView tv_myListEdit;
        private ImageView edit_event;
        private ImageView img_myListIcon;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_myListName = (TextView)itemView.findViewById(R.id.mylist_name);



        }
    }
}



