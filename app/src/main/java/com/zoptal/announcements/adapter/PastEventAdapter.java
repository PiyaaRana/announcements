package com.zoptal.announcements.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoptal.announcements.R;
import com.zoptal.announcements.fragments.EventDetailScreen;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.modal.InboxModal;
import com.zoptal.announcements.modal.PastEventModal;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class PastEventAdapter extends RecyclerView.Adapter<PastEventAdapter.ViewHolder> {

    private Context context;
    String name, event_loc, event_date, event_time, event_title, calcheck = "true", add_cal;
    String eventid, add_calendar, checkpdf, checkcaltext = "add", pdf_link;
    List<PastEventModal> recyclerList;

    public PastEventAdapter(List<PastEventModal> recyclerList, Context context) {
        super();
        this.recyclerList = recyclerList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_event_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final PastEventModal recycler_list = recyclerList.get(position);
        holder.game_data.setText(recycler_list.getPastEventName());
        holder.date_data.setText(recycler_list.getPastEventDate());
        holder.time_data.setText(recycler_list.getPastEventTime());
        holder.loc_data.setText(recycler_list.getPastEventLoc());


        }

    @Override
    public int getItemCount() {
        return recyclerList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView game_data, holiday;
        public TextView date_data;
        public TextView time_data;
        public TextView loc_data;
        RelativeLayout event_detail;
        public Button btn_addCalendar,btn_shareEvent;

        public ViewHolder(View itemView) {
            super(itemView);
            game_data = (TextView) itemView.findViewById(R.id.tv_host_name);
            date_data = (TextView) itemView.findViewById(R.id.tv_event_name);
            time_data = (TextView) itemView.findViewById(R.id.time_data_event);
            loc_data = (TextView) itemView.findViewById(R.id.tv_event_loc);
            event_detail = (RelativeLayout) itemView.findViewById(R.id.click_layout);
            btn_addCalendar= (Button)itemView.findViewById(R.id.btn_cal_evnt);
            btn_shareEvent = (Button)itemView.findViewById(R.id.btn_tell_frnd);
            btn_addCalendar.setVisibility(View.GONE);
            btn_shareEvent.setVisibility(View.GONE);


        }
    }

}
