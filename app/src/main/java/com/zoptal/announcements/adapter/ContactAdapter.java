package com.zoptal.announcements.adapter;

/**
 * Created by Abc on 10/24/2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.modal.ContactModel;
import com.zoptal.announcements.modal.ContactNameModel;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {
    private Context context;
    String name,invite;
    String personid,notifyevent_id,imageurl,buttoncheck;
    ProgressDialog pd;
    List<ContactNameModel> recyclerList;

    public ContactAdapter(List<ContactNameModel> recyclerList, Context context,String buttoncheck){
        super();

        this.recyclerList = recyclerList;
        this.context = context;
        this.buttoncheck=buttoncheck;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ContactNameModel recycler_list =  recyclerList.get(position);
        holder.person_name.setText(recycler_list.getImageName());
//        if(buttoncheck.equalsIgnoreCase("false")){
//            holder.send_event.setVisibility(View.GONE);
//        }
//        else {
//            holder.send_event.setVisibility(View.VISIBLE);
//        }

    }

    @Override
    public int getItemCount() {
        return recyclerList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView person_name;
        public ImageView person_pic;
        public TextView send_event;

        public ViewHolder(View itemView) {
            super(itemView);
            person_name = (TextView)itemView.findViewById(R.id.contact_name);
            person_pic= (ImageView) itemView.findViewById(R.id.profile_icon_inbox);
            //send_event  = (TextView)itemView.findViewById(R.id.send_event);


        }
    }


}

