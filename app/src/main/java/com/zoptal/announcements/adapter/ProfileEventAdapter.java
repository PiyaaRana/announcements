package com.zoptal.announcements.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoptal.announcements.R;
import com.zoptal.announcements.fragments.EventDetailScreen;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.modal.InboxModal;
import com.zoptal.announcements.modal.ProfileEventModal;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ProfileEventAdapter extends RecyclerView.Adapter<ProfileEventAdapter.MyViewHolder> {
    private List<ProfileEventModal> InboxList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView image;
        public TextView tv_userName,tv_eventName,tv_inboxTime;
        public String imageName;
        private RelativeLayout click_layout;
        public MyViewHolder(View view){
            super(view);
            tv_eventName =(TextView)view.findViewById(R.id.profile_event_name);
            tv_inboxTime =(TextView)view.findViewById(R.id.profile_event_time);
            click_layout=(RelativeLayout)view.findViewById(R.id.shift_items);

            click_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myactivity = new Intent(context.getApplicationContext(), EventDetailScreen.class);
                    myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.getApplicationContext().startActivity(myactivity);
                }
            });

        }
    }

    public ProfileEventAdapter(Context context, List<ProfileEventModal> InboxList){
        this.InboxList=InboxList;
        this.context= context;
    }

    @Override
    public ProfileEventAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_events_list,parent,false);
        return new ProfileEventAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProfileEventAdapter.MyViewHolder holder, int position) {
        ProfileEventModal contact = InboxList.get(position);

        holder.tv_eventName.setText(contact.getInboxEventName());
        holder.tv_inboxTime.setText(contact.getInboxEventTime());
        holder.click_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof BasicActivity) {
                    EventDetailScreen fragment = new EventDetailScreen();
//                    Bundle bundle=new Bundle();
//                    bundle.putString("eventid_notify", eventid); //key and value
//                    fragment.setArguments(bundle);
                    ((BasicActivity) context).replaceFragment(fragment);
                }
            }
        });
    }

    public void removeAt(int position){
        InboxList.remove(position);
        notifyItemRemoved(position);
    }
    @Override
    public int getItemCount() {
        return InboxList.size();
    }
}
