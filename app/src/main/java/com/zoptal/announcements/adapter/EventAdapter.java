package com.zoptal.announcements.adapter;

/**
 * Created by Abc on 10/17/2017.
 */

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.modal.ContactNameModel;
import com.zoptal.announcements.modal.EventContactsModal;
import com.zoptal.announcements.modal.EventModal;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    public Button contacts;
    private Context context;
    String name, event_loc,event_date,event_time,event_title,calcheck="true",add_cal;
    String eventid,add_calendar,checkpdf,checkcaltext="add",pdf_link;
    ProgressDialog pd;
    Dialog eventDialog;
    List<EventModal> recyclerList;

    public EventAdapter(List<EventModal> recyclerList, Context context){
        super();
        this.recyclerList = recyclerList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.awaiting_event_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final EventModal recycler_list =  recyclerList.get(position);
        holder.tv_eventName.setText(recycler_list.getEventName());
        holder.tv_eventType.setText(recycler_list.getEventType());
        holder.tv_eventLoc.setText(recycler_list.getEventLocation());
        holder.tv_eventDesc.setText(recycler_list.getEventDesc());
        holder.tv_eventSize.setText(recycler_list.getEventPartySize());
        holder.tv_eventTime.setText(recycler_list.getEventTime());

        LinearLayoutManager hs_linearLayout = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerView_EventContacts.setLayoutManager(hs_linearLayout);
        holder.recyclerView_EventContacts.setHasFixedSize(true);
        ContactsAdapter eventListChildAdapter = new ContactsAdapter(context,recyclerList.get(position).getContactNameModelList());
        holder.recyclerView_EventContacts.setAdapter(eventListChildAdapter);
    }

    @Override
    public int getItemCount() {
        return recyclerList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tv_eventName,tv_eventType,tv_eventLoc;
        public TextView tv_eventDesc,tv_eventSize,tv_eventTime,tv_contactNumberText;
        public RecyclerView recyclerView_EventContacts;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_eventName = (TextView)itemView.findViewById(R.id.list_event_name);
            tv_eventType = (TextView)itemView.findViewById(R.id.list_event_type);
            tv_eventLoc = (TextView)itemView.findViewById(R.id.list_event_loc);
            tv_eventSize = (TextView)itemView.findViewById(R.id.size_value);
            tv_eventTime = (TextView)itemView.findViewById(R.id.time_value);
            tv_eventDesc = (TextView)itemView.findViewById(R.id.list_event_desc);
            tv_contactNumberText = (TextView)itemView.findViewById(R.id.contact_numbr_txt);
            recyclerView_EventContacts=(RecyclerView)itemView.findViewById(R.id.recycler_contact_event);

        }
    }

//    public void changeText(ViewHolder holder,int position) {
//
//        if (checkcaltext.equals("add")) {
//
//            holder.addCalendar.setText("Add to Calendar");
//
//
//        } else {
//            holder.addCalendar.setText("Remove from Calendar");
//        }
//    }
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> {
    private List<EventContactsModal> ImageList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView image;
        public TextView tv_imageName;
        public String imageName;
        public MyViewHolder(View view){
            super(view);
            image = (ImageView) view.findViewById(R.id.imageViewName);
            tv_imageName=(TextView)view.findViewById(R.id.image_name);
             tv_imageName.setVisibility(View.GONE);
        }
    }

    public ContactsAdapter(Context context, List<EventContactsModal> ImageList){
        this.ImageList=ImageList;
        this.context= context;
    }

    @Override
    public ContactsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_images,parent,false);
        return new ContactsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContactsAdapter.MyViewHolder holder, int position) {
        EventContactsModal contact = ImageList.get(position);
        Picasso.with(context).load(contact.getImage()).into(holder.image);
        }

    public void removeAt(int position){
        ImageList.remove(position);
        notifyItemRemoved(position);
    }
    @Override
    public int getItemCount() {
        return ImageList.size();
    }
}

}



