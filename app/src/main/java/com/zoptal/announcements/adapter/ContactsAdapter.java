package com.zoptal.announcements.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.modal.ContactNameModel;
import java.util.List;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by Abc on 1/30/2018.
 */

    public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> {
        private List<ContactNameModel> ImageList;
        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder{
            public ImageView image;
            public TextView tv_imageName;
            public String imageName;
            public MyViewHolder(View view){
                super(view);
                image = (ImageView) view.findViewById(R.id.imageViewName);
                tv_imageName=(TextView)view.findViewById(R.id.image_name);
//                image.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent myactivity = new Intent(context.getApplicationContext(), ImageDetailsScreen.class);
//                        myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
//                        context.getApplicationContext().startActivity(myactivity);
//
//                    }
//                });

            }
        }

        public ContactsAdapter(Context context, List<ContactNameModel> ImageList){
            this.ImageList=ImageList;
            this.context= context;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_images,parent,false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ContactNameModel contact = ImageList.get(position);
            Picasso.with(context).load(contact.getImage()).into(holder.image);
            holder.tv_imageName.setText(contact.getImageName());
        }

        public void removeAt(int position){
            ImageList.remove(position);
            notifyItemRemoved(position);
        }
        @Override
        public int getItemCount() {
            return ImageList.size();
        }
    }


