package com.zoptal.announcements.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zoptal.announcements.R;
import com.zoptal.announcements.fragments.EventDetailScreen;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.modal.CalendarModalClass;
import com.zoptal.announcements.modal.ContactNameModel;
import com.zoptal.announcements.modal.InboxModal;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder> {
    private List<CalendarModalClass> CalendarList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView image;
        public TextView tv_userName,tv_eventName,tv_inboxTime,tv_locName;
        public String imageName;
        private RelativeLayout click_layout;
        private LinearLayout locLayout;

        public MyViewHolder(View view){
            super(view);
            tv_userName=(TextView)view.findViewById(R.id.inbox_username);
            tv_eventName =(TextView)view.findViewById(R.id.inbox_event_name);
            tv_inboxTime =(TextView)view.findViewById(R.id.inbox_event_time);
            click_layout=(RelativeLayout)view.findViewById(R.id.shift_items);
            locLayout=(LinearLayout)view.findViewById(R.id.loc_layout);
            tv_locName =(TextView)view.findViewById(R.id.event_loc);
            click_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myactivity = new Intent(context.getApplicationContext(), EventDetailScreen.class);
                    myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.getApplicationContext().startActivity(myactivity);
                }
            });
        }
    }

    public CalendarAdapter(Context context, List<CalendarModalClass> CalendarList){
        this.CalendarList=CalendarList;
        this.context= context;
    }

    @Override
    public CalendarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_event_list,parent,false);
        return new CalendarAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CalendarAdapter.MyViewHolder holder, int position) {
        CalendarModalClass contact = CalendarList.get(position);
        holder.tv_userName.setText(contact.getHostName());
        holder.tv_eventName.setText(contact.getCalendarEventName());
        holder.tv_inboxTime.setText(contact.getCalendarEventTime());
        holder.tv_locName.setText(contact.getCalendarEventLocation());
        holder.click_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof BasicActivity) {
                    EventDetailScreen fragment = new EventDetailScreen();
//                    Bundle bundle=new Bundle();
//                    bundle.putString("eventid_notify", eventid); //key and value
//                    fragment.setArguments(bundle);
                    ((BasicActivity) context).replaceFragment(fragment);
                }
            }
        });
    }

    public void removeAt(int position){
        CalendarList.remove(position);
        notifyItemRemoved(position);
    }
    @Override
    public int getItemCount() {
        return CalendarList.size();
    }
}


