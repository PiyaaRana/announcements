package com.zoptal.announcements.utils;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TypefaceTextviewCursive extends AppCompatTextView {
    private Context context;
    private AttributeSet attrs;
    private int defStyle;

    public TypefaceTextviewCursive(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public TypefaceTextviewCursive(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        init();
    }

    public TypefaceTextviewCursive(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        this.attrs = attrs;
        this.defStyle = defStyle;
        init();
    }

    private void init() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "font/smoothy_cursive.ttf");
        this.setTypeface(font);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        tf = Typeface.createFromAsset(getContext().getAssets(), "font/smoothy_cursive.ttf");
        super.setTypeface(tf, style);
    }

    @Override
    public void setTypeface(Typeface tf) {
        tf = Typeface.createFromAsset(getContext().getAssets(), "font/smoothy_cursive.ttf");
        super.setTypeface(tf);
    }
}