package com.zoptal.announcements.utils;

import okhttp3.logging.HttpLoggingInterceptor;

public class RestClient {

    public static HttpLoggingInterceptor getClient() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }
}