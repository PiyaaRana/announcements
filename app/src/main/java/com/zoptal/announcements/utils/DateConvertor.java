package com.zoptal.announcements.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Abc on 11/8/2017.
 */

public class DateConvertor {

    public String changeDateFormat(String dateString) {

        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "dd-MMMM-yyyy", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }
    public String changeDateFormatRecieved(String dateString) {

        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MMMM-yyyy",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }

    public String changeDateFormatProfile(String dateString) {

        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("EEEE,dd MMMM yyyy",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }

//    public static void main(String [] args) throws Exception {
//        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
//        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
//        Date date = parseFormat.parse("10:30 PM");
//        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
//    }

    public String changeTimeFormatSent(String dateString) {

        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "hh:mm a", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }
    public String changeTimeFormatSentComplete(String dateString) {

        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "hh:mm a", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm:ss",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }


    public String changeTimeFormatRecieve(String dateString) {

        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "hh:mm:ss", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mm a",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }

    public String changeTimeFormatZoneRecieve(String dateString) {

        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "hh:mm:ss", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("EEEE,dd/MM/yyyy",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }

    public String monthConvertor(String dateString) {

        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "dd/MM/yyyy", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMMM dd,yyyy",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }

    public String timeConvertorCal(String timeString) {
        String dateStringValue="";

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        try {
            Date date = parseFormat.parse(timeString);
            //System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
             dateStringValue= String.valueOf(displayFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateStringValue;
    }


    public String dateConvertorGrid(String dateString) {
        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "dd-MMMM-yyyy", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }
    public String dateConvertorCalendar(String dateString) {
        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "dd-MMMM-yyyy", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMMM ,yyyy",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }
    public String monthConvertorCalendar(String dateString) {
        SimpleDateFormat recivedFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMMM ,yyyy",
                Locale.US);
        Date dateObj = null;
        try {
            dateObj = recivedFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        return outputFormat.format(dateObj);
    }

}
