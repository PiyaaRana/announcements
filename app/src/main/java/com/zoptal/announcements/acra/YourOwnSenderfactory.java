package com.zoptal.announcements.acra;

import android.content.Context;

import org.acra.config.ACRAConfiguration;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderFactory;

/**
 * Created by Sidharth on 11/19/2016.
 */
public class YourOwnSenderfactory implements ReportSenderFactory {


    // NB requires a no arg constructor.

    public ReportSender create(Context context, ACRAConfiguration config) {
        return new YourOwnSender(config);
    }
}

