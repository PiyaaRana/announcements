package com.zoptal.announcements.retrofit;



import com.zoptal.announcements.responseClasses.CalendarEventResponse;
import com.zoptal.announcements.responseClasses.ChangePasswordResponse;
import com.zoptal.announcements.responseClasses.ContactsResponse;
import com.zoptal.announcements.responseClasses.EventResponse;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.responseClasses.ProfileResponse;
import com.zoptal.announcements.responseClasses.RegisterResponse;
import com.zoptal.announcements.responseClasses.SettingResponse;
import com.zoptal.announcements.responseClasses.SocialLinkResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Zoptal.101 on 05/10/17.
 */

public interface RetrofitAPIInnterface {

    @POST("register")
    @FormUrlEncoded
    Call<RegisterResponse> register(@Field("app_token") String apptoken, @Field("username") String username,
                                    @Field("email") String email, @Field("phone") String phone, @Field("password") String password, @Field("device_type") String device_type, @Field("device_token") String device_token);

    @POST("login")
    @FormUrlEncoded
    Call<LoginResponse> login(@Field("app_token") String apptoken, @Field("username") String username, @Field("password") String password,
                              @Field("device_type") String device_type, @Field("device_token") String device_token);

    @POST("forgot_password")
    @FormUrlEncoded
    Call<LoginResponse> forgot_password(@Field("app_token") String apptoken, @Field("email") String email);

    @POST("change_password")
    @FormUrlEncoded
    Call<ChangePasswordResponse> change_password(@Field("app_token") String apptoken, @Field("access_token") String accesstoken, @Field("old_password") String old_password, @Field("password") String password);

    @POST("logout")
    @FormUrlEncoded
    Call<LoginResponse> logout(@Field("app_token") String apptoken, @Field("access_token") String access_token);

    @POST("my_contacts")
    @FormUrlEncoded
    Call<ContactsResponse> my_contacts(@Field("app_token") String apptoken, @Field("access_token") String access_token, @Field("contacts") String contacts,@Field("event_id") String eventid);

    @POST("add_event")
    @FormUrlEncoded
    Call<EventResponse> add_event(@Field("app_token") String apptoken, @Field("access_token") String access_token, @Field("category") String category_name, @Field("name") String event_name,
                                  @Field("description") String event_desc, @Field("event_date") String event_date, @Field("start_time") String start_time, @Field("end_time") String end_time, @Field("event_type") String event_type,
                                  @Field("contact_name") String contact_name, @Field("contact_phone") String contact_phone, @Field("location") String event_loc, @Field("latitude") String event_latitude, @Field("party_size") String partySize, @Field("longitude") String event_longitude,
                                  @Field("invited_user_ids") String invited_users);

    @POST("my_events")
    @FormUrlEncoded
    Call<CalendarEventResponse> my_events(@Field("app_token") String apptoken, @Field("access_token") String access_token, @Field("event_date") String eventDate, @Field("period_type") String timeDuration,
                                          @Field("contacts") String contactList, @Field("search") String searchString);

    @POST("inbox")
    @FormUrlEncoded
    Call<CalendarEventResponse> inbox(@Field("app_token") String apptoken, @Field("access_token") String access_token,@Field("contacts") String contactList,@Field("search") String searchString);

    @POST("mark_event_invitation")
    @FormUrlEncoded
    Call<LoginResponse> mark_event_invitation(@Field("app_token") String apptoken, @Field("access_token") String access_token,@Field("invite_id") String inviteId,@Field("mark_event") String markEvent);

    @POST("received_event_invitations")
    @FormUrlEncoded
    Call<CalendarEventResponse> received_event_invitations(@Field("app_token") String apptoken, @Field("access_token") String access_token,@Field("invite_type")String inviteType,@Field("contacts") String contactList,@Field("search") String search);

    @POST("add_to_calendar")
    @FormUrlEncoded
    Call<LoginResponse> add_to_calendar(@Field("app_token") String apptoken, @Field("access_token") String access_token,@Field("event_id") String eventId);

    @POST("my_profile")
    @FormUrlEncoded
    Call<ProfileResponse> my_profile(@Field("app_token") String apptoken, @Field("access_token") String access_token);

    @POST("update_profile")
    @Multipart
    Call<ProfileResponse> update_profile(@Part("app_token") RequestBody apptoken,@Part("access_token") RequestBody access_token, @Part("fullname") RequestBody fullname,@Part("location") RequestBody location, @Part("email") RequestBody email,@Part("phone") RequestBody phone,@Part("picture\"; filename=\"myfile.jpg\" ") RequestBody profile_pic);

    @POST("update_social_links")
    @FormUrlEncoded
    Call<SocialLinkResponse> update_social_links(@Field("app_token") String apptoken, @Field("access_token") String access_token, @Field("social_fb_link") String fbLink, @Field("social_tw_link") String twitterLink,
                                                 @Field("social_pin_link") String pinLink, @Field("social_insta_link") String instaLink);
    @POST("remove_from_calendar")
    @FormUrlEncoded
    Call<LoginResponse> remove_from_calendar(@Field("app_token") String apptoken, @Field("access_token") String access_token,@Field("event_id") String eventId);

    @POST("update_user_settings")
    @FormUrlEncoded
    Call<SettingResponse> update_user_settings(@Field("app_token") String appToken,@Field("access_token") String access_token, @Field("show_location") String getLocation, @Field("show_social_links") String getLinks,@Field("show_past_checkin") String getCheckins,
                    @Field("show_contact_info") String getContacts);
    @POST("get_user_settings")
    @FormUrlEncoded
    Call<SettingResponse> get_user_settings(@Field("app_token") String appToken,@Field("access_token") String access_token);

    @POST("user_profile")
    @FormUrlEncoded
    Call<ProfileResponse> user_profile(@Field("app_token") String apptoken, @Field("access_token") String access_token,@Field("user_id") String userId);
    @POST("my_calendars")
    @FormUrlEncoded
    Call<CalendarEventResponse> my_calendars(@Field("app_token") String apptoken, @Field("access_token") String access_token, @Field("event_date") String eventDate, @Field("period_type") String timeDuration,
                                          @Field("contacts") String contactList, @Field("search") String searchString);

    @POST("edit_event")
    @FormUrlEncoded
    Call<EventResponse> edit_event(@Field("app_token") String apptoken, @Field("access_token") String access_token,@Field("event_id") String eventId, @Field("category") String category_name, @Field("name") String event_name,
                                  @Field("description") String event_desc, @Field("event_date") String event_date, @Field("start_time") String start_time, @Field("end_time") String end_time, @Field("event_type") String event_type,
                                  @Field("contact_name") String contact_name, @Field("contact_phone") String contact_phone, @Field("location") String event_loc, @Field("latitude") String event_latitude, @Field("party_size") String partySize, @Field("longitude") String event_longitude,
                                  @Field("invited_user_ids") String invited_users);

    @POST("delete_event")
    @FormUrlEncoded
    Call<LoginResponse> delete_event(@Field("app_token") String apptoken, @Field("access_token") String access_token,@Field("event_id") String eventId);

}