package com.zoptal.announcements.modal;

public class InboxModal {
    private String hostName,inboxEventName,inboxEventTime;
    public InboxModal(String hostName,String inboxEventName,String inboxEventTime){
        this.hostName=hostName;
        this.inboxEventName=inboxEventName;
        this.inboxEventTime=inboxEventTime;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getInboxEventName() {
        return inboxEventName;
    }

    public void setInboxEventName(String inboxEventName) {
        this.inboxEventName = inboxEventName;
    }

    public String getInboxEventTime() {
        return inboxEventTime;
    }

    public void setInboxEventTime(String inboxEventTime) {
        this.inboxEventTime = inboxEventTime;
    }
}
