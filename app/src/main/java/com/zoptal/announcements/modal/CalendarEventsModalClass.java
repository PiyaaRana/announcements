package com.zoptal.announcements.modal;

public class CalendarEventsModalClass {

        private String hostName,calendarEventName,calendarEventDate,calendarEventLocation,hostContact,eventStartTime,eventEndTime,userImage,
    userId;

        public String getHostName() {
            return hostName;
        }

        public void setHostName(String hostName) {
            this.hostName = hostName;
        }

        public String getCalendarEventLocation() {
            return calendarEventLocation;
        }

        public void setCalendarEventLocation(String calendarEventLocation) {
            this.calendarEventLocation = calendarEventLocation;
        }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCalendarEventName() {
            return calendarEventName;
        }

        public void setCalendarEventName(String calendarEventName) {
            this.calendarEventName = calendarEventName;
        }

    public String getCalendarEventDate() {
        return calendarEventDate;
    }

    public void setCalendarEventDate(String calendarEventDate) {
        this.calendarEventDate = calendarEventDate;
    }

    public String getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(String eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getHostContact() {
        return hostContact;
    }

    public void setHostContact(String hostContact) {
        this.hostContact = hostContact;
    }

    public String getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }
}


