package com.zoptal.announcements.modal;

public class CalendarModalClass {
    private String hostName,calendarEventName,calendarEventTime,calendarEventLocation;

    public CalendarModalClass(String hostName,String calendarEventName,String calendarEventTime,String calendarEventLocation){
        this.hostName=hostName;
        this.calendarEventName=calendarEventName;
        this.calendarEventTime=calendarEventTime;
        this.calendarEventLocation=calendarEventLocation;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getCalendarEventLocation() {
        return calendarEventLocation;
    }

    public void setCalendarEventLocation(String calendarEventLocation) {
        this.calendarEventLocation = calendarEventLocation;
    }

    public String getCalendarEventName() {
        return calendarEventName;
    }

    public void setCalendarEventName(String calendarEventName) {
        this.calendarEventName = calendarEventName;
    }

    public String getCalendarEventTime() {
        return calendarEventTime;
    }

    public void setCalendarEventTime(String calendarEventTime) {
        this.calendarEventTime = calendarEventTime;
    }
}
