package com.zoptal.announcements.modal;

/**
 * Created by Abc on 1/30/2018.
 */

public class ContactNameModel {
        private int image;
        private String imageName;

        public ContactNameModel(int image,String imageName) {
            this.image= image;
            this.imageName=imageName;
        }
        public int getImage() {
            return image;
        }
        public void setImage(int image) {
            this.image = image;
        }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
