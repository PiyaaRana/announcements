package com.zoptal.announcements.modal;

import java.util.List;

public class EventModal {
    private String eventName,eventType,eventLocation,eventPartySize,eventTime;
    private int eventDesc;
    private List<EventContactsModal>contactNameModelList;

    public EventModal(String eventName,String eventType,String eventLocation,int eventDesc,String eventPartySize, String eventTime,
                      List<EventContactsModal>contactNameModels){
        this.eventName=eventName;
        this.eventType=eventType;
        this.eventLocation=eventLocation;
        this.eventDesc=eventDesc;
        this.eventPartySize=eventPartySize;
        this.eventTime=eventTime;
        this.contactNameModelList= contactNameModels;
    }


    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public int getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(int eventDesc) {
        this.eventDesc = eventDesc;
    }

    public String getEventPartySize() {
        return eventPartySize;
    }

    public void setEventPartySize(String eventPartySize) {
        this.eventPartySize = eventPartySize;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public List<EventContactsModal> getContactNameModelList() {
        return contactNameModelList;
    }

    public void setContactNameModelList(List<EventContactsModal> contactNameModelList) {
        this.contactNameModelList = contactNameModelList;
    }
}
