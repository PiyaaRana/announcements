package com.zoptal.announcements.modal;

public class PastEventModal {
    private String pastEventName,pastEventTime,pastEventLoc,pastEventDate;
    public PastEventModal(String pastEventName,String pastEventDate,String pastEventTime,String pastEventLoc){
        this.pastEventName=pastEventName;
        this.pastEventTime=pastEventTime;
        this.pastEventLoc=pastEventLoc;
        this.pastEventDate=pastEventDate;
    }

    public String getPastEventName() {
        return pastEventName;
    }

    public void setPastEventName(String pastEventName) {
        this.pastEventName = pastEventName;
    }

    public String getPastEventLoc() {
        return pastEventLoc;
    }

    public void setPastEventLoc(String pastEventLoc) {
        this.pastEventLoc = pastEventLoc;
    }

    public String getPastEventTime() {
        return pastEventTime;
    }

    public void setPastEventTime(String pastEventTime) {
        this.pastEventTime = pastEventTime;
    }

    public String getPastEventDate() {
        return pastEventDate;
    }

    public void setPastEventDate(String pastEventDate) {
        this.pastEventDate = pastEventDate;
    }
}
