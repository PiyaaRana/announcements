package com.zoptal.announcements.modal;

public class ProfileEventModal {
    private String inboxEventName,inboxEventTime;
    public ProfileEventModal(String inboxEventName,String inboxEventTime){
        this.inboxEventName=inboxEventName;
        this.inboxEventTime=inboxEventTime;
    }
    public String getInboxEventName() {
        return inboxEventName;
    }

    public void setInboxEventName(String inboxEventName) {
        this.inboxEventName = inboxEventName;
    }

    public String getInboxEventTime() {
        return inboxEventTime;
    }

    public void setInboxEventTime(String inboxEventTime) {
        this.inboxEventTime = inboxEventTime;
    }

}
