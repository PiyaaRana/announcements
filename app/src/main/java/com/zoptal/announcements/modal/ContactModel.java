package com.zoptal.announcements.modal;



public class ContactModel {


    private String person_name,person_id,inviteCheck,imageurl;

    public ContactModel(){

    }


    public String getPerson_id() {
        return person_id;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getInviteCheck() {
        return inviteCheck;
    }

    public void setInviteCheck(String inviteCheck) {
        this.inviteCheck = inviteCheck;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}


