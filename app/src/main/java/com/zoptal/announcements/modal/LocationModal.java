package com.zoptal.announcements.modal;

/**
 * Created by Abc on 4/23/2018.
 */

public class LocationModal {
    String storeName,vicinity;
    Double Latitude,Longitude;

    public LocationModal(String storeName, String vicinity){
        this.storeName=storeName;
        this.vicinity=vicinity;

    }


    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }
}
