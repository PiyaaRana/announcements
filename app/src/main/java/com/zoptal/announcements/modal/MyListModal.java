package com.zoptal.announcements.modal;

/**
 * Created by Abc on 4/17/2018.
 */

public class MyListModal {

    private String ListName;

    public MyListModal(String ListName){
        this.ListName=ListName;

    }

    public String getListName() {
        return ListName;

    }

    public void setListName(String listName) {
        ListName = listName;
    }
}
