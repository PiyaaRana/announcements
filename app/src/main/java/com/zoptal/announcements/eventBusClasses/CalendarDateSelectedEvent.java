package com.zoptal.announcements.eventBusClasses;

public class CalendarDateSelectedEvent {
    private String currentDate,showButtons;

    public CalendarDateSelectedEvent(String date,String showButtons){

        this.currentDate = date;
        this.showButtons=showButtons;
    }

    public String getCurrentDate(){

        return currentDate;
    }

    public String getShowButtons() {
        return showButtons;
    }
}

