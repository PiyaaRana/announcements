package com.zoptal.announcements.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


public class AppActivity extends AppCompatActivity {



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void startActivity(Class<?> activity){
        Intent intent=new Intent(this,activity);
        startActivity(intent);
    }

    protected void startActivityWithFinish(Class<?> activity){
        Intent intent=new Intent(this,activity);
        startActivity(intent);
        finish();
    }
    protected void startActivityWithAllFinish(Class<?> activity){
        Intent intent=new Intent(this,activity);
        startActivity(intent);
        finishAffinity();
    }



}
