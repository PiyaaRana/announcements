package com.zoptal.announcements.base;

import com.zoptal.announcements.responseClasses.CalendarEventResponse;
import com.zoptal.announcements.responseClasses.EventResponse;

import java.util.ArrayList;

public class DataHelperClass {
    private static final DataHelperClass ourInstance = new DataHelperClass();

    public static DataHelperClass getInstance() {
        return ourInstance;
    }

    private DataHelperClass() {
    }

    EventResponse.EventDataModal eventDataModal;

    public EventResponse.EventDataModal getEventDataModal() {
        return eventDataModal;
    }

    public void setEventDataModal(EventResponse.EventDataModal eventDataModal) {
        this.eventDataModal = eventDataModal;
    }

   ArrayList< CalendarEventResponse.CalendarEventDataModal> calendarEventDataModal;

    public ArrayList<CalendarEventResponse.CalendarEventDataModal> getCalendarEventDataModal() {
        return calendarEventDataModal;
    }

    public void setCalendarEventDataModal(ArrayList<CalendarEventResponse.CalendarEventDataModal> calendarEventDataModal) {
        this.calendarEventDataModal = calendarEventDataModal;
    }
}
