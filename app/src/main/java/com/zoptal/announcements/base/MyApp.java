package com.zoptal.announcements.base;

/**
 * Created by Abc on 10/25/2017.
 */

import android.app.Application;

import com.google.android.gms.maps.model.LatLng;
import com.zoptal.announcements.R;
import com.zoptal.announcements.acra.YourOwnSenderfactory;
import com.zoptal.announcements.utils.FontsOverride;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

//@ReportsCrashes(
//        reportSenderFactoryClasses = {YourOwnSenderfactory.class},
//        mode = ReportingInteractionMode.DIALOG,
//
//        resDialogText = R.string.crash_dialog_text,//optional. default is a warning sign
//        resDialogTitle = R.string.crash_dialog_title // optional. default is your application name
//        /*  mailTo = "raman.zoptal@gmail.com",
//          mode = ReportingInteractionMode.DIALOG,
//          customReportContent = { ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT },
//          resDialogText = R.string.crash_dialog_text,//optional. default is a warning sign
//          resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
//          //  resDialogOkToast = R.string.crash_dialog_ok_toast,
//          reportDialogClass = CustomCrashReportDialog.class,// optional. displays a Toast message when the user accepts to send a report.
//          reportSenderFactoryClasses = {YourOwnSenderfactory.class},
//          alsoReportToAndroidFramework = false*/
//)
public class MyApp extends Application {

    private static MyApp instance;
    private String eventPlace,startTime,endTime,dateValue,eventTypeValue;
    private LatLng eventLatlng;
    public static MyApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FontsOverride.setDefaultFont(this, "DEFAULT", "font/helvetica_medium.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "font/helvetica_medium.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "font/helvetica_medium.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "font/helvetica_medium.ttf");
//        ACRA.init(this);
//        ACRA.isInitialised();
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEventPlace() {
        return eventPlace;
    }

    public void setEventPlace(String eventPlace) {
        this.eventPlace = eventPlace;
    }

    public LatLng getEventLatlng() {
        return eventLatlng;
    }

    public void setEventLatlng(LatLng eventLatlng) {
        this.eventLatlng = eventLatlng;
    }

    public String getDateValue() {
        return dateValue;
    }

    public void setDateValue(String dateValue) {
        this.dateValue = dateValue;
    }

    public String getEventTypeValue() {
        return eventTypeValue;
    }

    public void setEventTypeValue(String eventTypeValue) {
        this.eventTypeValue = eventTypeValue;
    }
}

