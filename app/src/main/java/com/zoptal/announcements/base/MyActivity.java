package com.zoptal.announcements.base;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;


import com.zoptal.announcements.R;

import java.io.ByteArrayOutputStream;



/**
 * Created by Abc on 2/12/2018.
 */

public class MyActivity extends AppCompatActivity {

    public static void showOneErrorDialog(String msg, String btn_txt, Context context) {
        final Dialog dialogBox = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        dialogBox.setContentView(R.layout.add_detail_dialog);

        TextView ok = (TextView) dialogBox.findViewById(R.id.ok);
        TextView detailhead = (TextView) dialogBox.findViewById(R.id.detailhead);
        TextView bodycontent = (TextView) dialogBox.findViewById(R.id.detailtext);
        bodycontent.setText(msg.toString().trim());
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();
            }
        });
        dialogBox.show();
    }

    public static byte[] getimageData(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        int b= byteArrayOutputStream.toByteArray().length;
        Log.e("sizeimage",""+b);
        return byteArrayOutputStream.toByteArray();
    }

    public void setStatusBar(){
        Window window = getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.setStatusBarColor(getResources().getColor(R.color.colorAccent));

        }
    }

}


