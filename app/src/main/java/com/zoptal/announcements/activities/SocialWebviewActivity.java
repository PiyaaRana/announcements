package com.zoptal.announcements.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.zoptal.announcements.R;

public class SocialWebviewActivity extends AppCompatActivity{
    private LinearLayout layout_back;
    private WebView webView;
    private RelativeLayout mainLayout;
    private ProgressBar progressBar;
    private String socialUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.social_webview_activity);
        Intent intent= getIntent();
        if(intent!=null){
            socialUrl= intent.getStringExtra("url");
        }
        findViews();
    }

    private void findViews(){
        layout_back=(LinearLayout)findViewById(R.id.ll_back);
        mainLayout=(RelativeLayout) findViewById(R.id.web_layout);
        webView = (WebView) findViewById(R.id.webview);
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        startWebView(socialUrl);
    }

    private void startWebView(String url) {
        webView.setWebViewClient(new WebViewClient() {
            //Create new webview Client to show progress dialog
            //When opening a url or click on link
            //ProgressDialog progressDialog;
            //If you will not use this method url links are opeen in new brower not in webview

            public boolean shouldOverrideUrlLoading(WebView v, String url) {

                v.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
            }

            //Show loader on url load

            public void onLoadResource(WebView view, String url) {


            }
            public void onPageFinished(WebView vw, String url) {
                super.onPageFinished(vw, url);
                progressBar.setVisibility(View.INVISIBLE);
            }



        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);

    }
}
