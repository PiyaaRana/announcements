package com.zoptal.announcements.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zoptal.announcements.GoogleUtil.DataParser;
import com.zoptal.announcements.GoogleUtil.DownloadURL;
import com.zoptal.announcements.R;
import com.zoptal.announcements.adapter.LocationAdapter;
import com.zoptal.announcements.adapter.PlacesLocationAdapter;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.mainController.BasicActivity;
import com.zoptal.announcements.modal.LocationModal;
import com.zoptal.announcements.modal.MyListModal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SearchAmenitiesActivity extends android.support.v4.app.Fragment implements GoogleApiClient.ConnectionCallbacks
        ,GoogleApiClient.OnConnectionFailedListener,LocationListener {

    GoogleApiClient googleApiClient;
    GoogleMap mMap;
    Location mLocation;
    LocationRequest locationRequest;
    Marker marker;
    private double latitude=0.0,latitude_map;
    private double longitude=0.0,longitude_map;
    LatLng latLng;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private TextView complete_add;
    View view;
    private LocationAdapter madapter;
    private EditText edt_search;
    private PlacesLocationAdapter cadapter;
    private RecyclerView recyclerViewList;
    private List<MyListModal> myListModalList;
    Boolean flag=true;
    private boolean isFragmentVisible=false;
    Dialog dialog;
    String search="stores+hotels+transport",searches="gifts",eventLocation;
   private int PROXIMITY_RADIUS = 10000;
    ArrayList<String>storeName,vicinities,result;
    ArrayList<Double>latList,longList;
    private ImageView img_SearchIcon;
    private MyActivity myActivity;
    private ProgressBar progressBar;
    private RelativeLayout mainLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.search_amenities_fragment,container,false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            isFragmentVisible=true;
            BasicActivity.getThis().findViewById(R.id.ll_back).setVisibility(View.VISIBLE);
            BasicActivity.getThis().findViewById(R.id.ll_menu).setVisibility(View.VISIBLE);
            Bundle bundle = getArguments();
            if (bundle != null) {
                latitude = bundle.getDouble("lat", 0.0);
                longitude = bundle.getDouble("lon", 0.0);
                //Log.e("eventssssss", ""+latitude+"  "+""+longitude );
                eventLocation= bundle.getString("eventplace" );
              latLng=  getLocationFromAddress(getActivity(),eventLocation);
              latitude= latLng.latitude;
              longitude=latLng.longitude;

            }

            myListModalList= new ArrayList<>();
            recyclerViewList = (RecyclerView) view.findViewById(R.id.recycler_mylist);
            edt_search = (EditText) view.findViewById(R.id.edt_search);
            img_SearchIcon= (ImageView) view.findViewById(R.id.srchicon);
            mainLayout= (RelativeLayout) view.findViewById(R.id.main_layout);
            progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            mainLayout.addView(progressBar, params);
            progressBar.setVisibility(View.VISIBLE);     // To Hide ProgressBar
//            final Boolean abc = checkInternetConenction();
//            if (abc) {
//                buildGoogleApi();
//            }else {
//                progressBar.setVisibility(View.GONE);
//            }
            final Boolean abc1 = checkInternetConenction();
            if (abc1) {

                getNearbyBanks();
            }
            else {
                progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
            }

            img_SearchIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String searchText=  edt_search.getText().toString().trim();
                    String[] splited = searchText.split("\\s+");
                    if(splited.length>0) {
                        if(splited.length==1){
                            searches = splited[0];
                        }
                        else {
                            for (int i = 0; i < splited.length - 1; i++) {
                                searches = splited[i] + "+" + splited[i + 1];
                            }
                        }
                    }
                    else {
                        myActivity.showOneErrorDialog("Enter any search keyword.", "OK", getActivity());
                    }
                    progressBar.setVisibility(View.VISIBLE);

                    final Boolean abc = checkInternetConenction();
                    if (abc) {

                        getNearbyBanks();
                    }
                    else {
                        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
                    }
                }
            });
            edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        String searchText=  edt_search.getText().toString().trim();
                        String[] splited = searchText.split("\\s+");
                        if(splited.length>0) {
                            if(splited.length==1){
                                searches = splited[0];
                            }
                            else {
                                for (int i = 0; i < splited.length - 1; i++) {
                                    searches = splited[i] + "+" + splited[i + 1];
                                }
                            }
                        }
                        else {
                            myActivity.showOneErrorDialog("Enter any search keyword.", "OK", getActivity());
                        }
                        progressBar.setVisibility(View.VISIBLE);

                        final Boolean abc = checkInternetConenction();
                        if (abc) {

                            getNearbyBanks();
                        }
                        else {
                            progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
                        }

                        return true;
                    }
                    return false;
                }
            });

//            madapter = new LocationAdapter(myListModalList,getActivity());
//            recyclerViewList.setHasFixedSize(true);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity())
//            {
//                @Override
//                public boolean canScrollVertically() {
//                    return false;
//                }
//            };
//            recyclerViewList.setLayoutManager(mLayoutManager);
//            //recyclerView
//            // List.setNestedScrollingEnabled(false);
//            recyclerViewList.setAdapter(madapter);

            //prepareList();

        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
//Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            getActivity().finish();
        } else {
        }
        LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage("Do you want to enable GPS?");
            dialog.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    getActivity().startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }


        return view;
    }
    @Override
    public void onDestroyView() {
        googleApiClient=null;
        isFragmentVisible=false;
        super.onDestroyView();
    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(getActivity());
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        0).show();
            }
            return false;
        }
        return true;
    }
    protected synchronized void buildGoogleApi(){
        googleApiClient= new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(10*1000);
        locationRequest.setFastestInterval(1*1000);
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED)
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,locationRequest,this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onLocationChanged(Location location) {
        mLocation=location;
        if(isFragmentVisible) {
            //getCompleteAddressString(latitude, longitude);



        }
//        latitude=latitude_map;
//        longitude=longitude_map;
        //latLng = new LatLng(latitude,longitude);
        //Log.e("latlng",""+latLng);

        if(googleApiClient != null){
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient,this);
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }

    }
    @SuppressLint("LongLogTag")
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                //complete_add.setText(strAdd);
               // Log.e("My Current loction address", strReturnedAddress.toString());
            } else {
                Log.e("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void prepareList() {
        MyListModal contact = new MyListModal("Kroger");
        myListModalList.add(contact);

        contact = new MyListModal("Store Name");
        myListModalList.add(contact);

        contact = new MyListModal("Store");
        myListModalList.add(contact);

        contact = new MyListModal("Grocery");
        myListModalList.add(contact);
        madapter.notifyDataSetChanged();
    }


    private void getNearbyBanks(){
        String url = getUrl(latitude, longitude, search,searches);
        Object[] DataTransfer = new Object[2];
        DataTransfer[1] = url;
        GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData();
        getNearbyPlacesData.execute(DataTransfer);
        //Toast.makeText(getActivity(), "These are your Nearby Stores! ",
               // Toast.LENGTH_LONG).show();
    }

    private String getUrl(double latitude, double longitude, String nearbyPlace,String keyword) {
        StringBuilder googlePlacesUrl = new
                StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("type=" + nearbyPlace);
        googlePlacesUrl.append("&location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);

        googlePlacesUrl.append("&keyword=" + keyword);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&opennow=true");
        googlePlacesUrl.append("&key=" + "AIzaSyBLs1l4wEWnwCHNJ_TdiJ_TPAnDn_vQVS8");

        Log.e("googleplace",googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
    }

    public class GetNearbyPlacesData extends AsyncTask<Object, String, String> {

        String googlePlacesData;
        GoogleMap mMap;
        String url;
        ArrayList<LocationModal> myPlacesList;

        @Override
        protected String doInBackground(Object... params) {
            try {
                Log.d("GetNearbyStoresData", "doInBackground entered");

                url = (String) params[1];
                Log.e("googleurls",url);
                DownloadURL urlConnection = new DownloadURL();
                googlePlacesData = urlConnection.readUrl(url);
                Log.d("GooglePlacesReadTask", "doInBackground Exit");
            } catch (Exception e) {
                Log.d("GooglePlacesReadTask", e.toString());
            }
            return googlePlacesData;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("GooglePlacesReadTask", "onPostExecute Entered");
            Log.e("result",""+result);
            List<HashMap<String, String>> nearbyPlacesList = null;
            DataParser dataParser = new DataParser();
            nearbyPlacesList = dataParser.parse(result);
            if(nearbyPlacesList.size()>0|| nearbyPlacesList==null) {
                ShowNearbyPlaces(nearbyPlacesList);
            }
            else {
                progressBar.setVisibility(View.GONE);
                recyclerViewList.setVisibility(View.INVISIBLE);
                myActivity.showOneErrorDialog("No search results found.", "OK", getActivity());
            }
            Log.d("GooglePlacesReadTask", "onPostExecute Exit");
        }

        private void ShowNearbyPlaces(List<HashMap<String, String>> nearbyPlacesList) {

            storeName = new ArrayList<>();
            vicinities = new ArrayList<>();
            latList= new ArrayList<>();
            longList= new ArrayList<>();
            result= new ArrayList<>();
            for (int i = 0; i < nearbyPlacesList.size(); i++) {

                Log.d("onPostExecute", "Entered into showing locations");
                MarkerOptions markerOptions = new MarkerOptions();
                HashMap<String, String> googlePlace = nearbyPlacesList.get(i);
                Log.e("googleplaces",""+googlePlace);
                double lat = Double.parseDouble(googlePlace.get("lat"));
                double lng = Double.parseDouble(googlePlace.get("lng"));
                String placeName = googlePlace.get("place_name");
                String vicinity = googlePlace.get("vicinity");
                storeName.add(placeName);
                vicinities.add(vicinity);
                latList.add(lat);
                longList.add(lng);

//                LatLng latLng = new LatLng(lat, lng);
//                Log.e("latlng",""+latLng);
//                markerOptions.position(latLng);
//                markerOptions.title(placeName + " : " + vicinity);
//                mMap.addMarker(markerOptions);
//                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
////move map camera
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//                mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
            }

            Log.e("lat_long_list","   "+latList+"  "+longList);

//for(int j=0;j<latList.size();j++) {
//   String duration= getUrls(latList.get(j),longList.get(j));
//    String results=getDuration(duration);
//    result.add(results);
//    Log.e("duration",""+results);
//}

                    progressBar.setVisibility(View.GONE);


            cadapter =new PlacesLocationAdapter(getDataSet(),getActivity());
            recyclerViewList.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity()) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            recyclerViewList.setLayoutManager(mLayoutManager);
            //recyclerView
            // List.setNestedScrollingEnabled(false);
            recyclerViewList.setAdapter(cadapter);
//            if(latList.size()!=0) {
//
//                ApiDirectionsAsyncTask apiDirectionsAsyncTask = new ApiDirectionsAsyncTask();
//                apiDirectionsAsyncTask.execute();
//
//
//            }


        }
    }

    private ArrayList<LocationModal> getDataSet(){
        ArrayList results = new ArrayList<LocationModal>();
        for (int index = 0; index < storeName.size(); index++) {
            LocationModal obj = new LocationModal(storeName.get(index),vicinities.get(index)
            );
            results.add(index, obj);
        }
        return results;


    }

    private String getUrls(double lat1, double lng1){

        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latitude + "," + longitude + "&destination=" + lat1 + "," + lng1 + "&mode=driving&sensor=false"
                +"&key=AIzaSyAIg_fF-OgtiX-Ny5Bmo3srkx3TAs3XEYk";
        String googlePlacesDuration="";
        DownloadURL urlConnection = new DownloadURL();
        try {
            googlePlacesDuration = urlConnection.readUrl(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return googlePlacesDuration;

    }

    private String getDuration(String data) {
        StringBuilder stringBuilder = new StringBuilder();
        Double dist = 0.0;
        String timeValue="";


        //JSONObject jsonObject = new JSONObject();
        try {

            JSONObject jsonObject = new JSONObject(data);

            JSONArray array = jsonObject.getJSONArray("routes");

            JSONObject routes = array.getJSONObject(0);

            JSONArray legs = routes.getJSONArray("legs");

            JSONObject steps = legs.getJSONObject(0);

            JSONObject distance = steps.getJSONObject("duration");

            Log.e("Duration", distance.toString());
            timeValue = distance.getString("text") ;

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return timeValue;
    }


    public class ApiDirectionsAsyncTask extends AsyncTask<URL, Integer, StringBuilder> {

        Double dest_lat,dest_long;


        private final String TAG = "Shift details";
        //private static final String DIRECTIONS_API_BASE = "https://maps.googleapis.com/maps/api/distancematrix";
        private static final String DIRECTIONS_API_BASE = "https://maps.googleapis.com/maps/api/directions";
        private static final String OUT_JSON = "/json";
        private static final String GOOGLE_DISTANCE_API_KEY="AIzaSyAIg_fF-OgtiX-Ny5Bmo3srkx3TAs3XEYk";
        StringBuilder mJsonResults ;
        // API KEY of the project Google Map Api For work

        @Override
        protected StringBuilder doInBackground(URL... params) {
            Log.i(TAG, "doInBackground of ApiDirectionsAsyncTask");

            HttpURLConnection mUrlConnection = null;

            for ( int i = 0; i < latList.size(); i++) {
                Log.e("latlistsize",""+latList.size());

                mJsonResults =new StringBuilder();
                try {
                    dest_lat = latList.get(i);
                    dest_long = longList.get(i);
                    StringBuilder sb = new StringBuilder(DIRECTIONS_API_BASE + OUT_JSON);
                    sb.append("?origin=" + URLEncoder.encode(latitude + "," + longitude, "utf8"));
                    sb.append("&destination=" + URLEncoder.encode(dest_lat + "," + dest_long, "utf8"));
                    sb.append("&key=" + GOOGLE_DISTANCE_API_KEY);
                    Log.e("finalresult", sb.toString());

                    // String sb = "https://maps.googleapis.com/maps/api/distancematrix/json?origin=" + currentlocationname + "&destination=" + shifthistorydata.getAddress() + "&key="+GOOGLE_DISTANCE_API_KEY;
                    URL url = new URL(sb.toString());
                    mUrlConnection = (HttpURLConnection) url.openConnection();
                    InputStreamReader in = new InputStreamReader(mUrlConnection.getInputStream());

                    // Load the results into a StringBuilder
                    int read;
                    char[] buff = new char[1024];
                    while ((read = in.read(buff)) != -1) {
                        mJsonResults.append(buff, 0, read);

                        Log.e("stringBuilder",""+mJsonResults.length());
                        //in.close();
                    }


                } catch (MalformedURLException e) {
                    Log.e(TAG, "Error processing Distance Matrix API URL");
                    return null;

                } catch (IOException e) {

                    System.out.println("Error connecting to Distance Matrix");
                    return null;
                }
//                finally {
//                    if (mUrlConnection != null) {
//                        mUrlConnection.disconnect();
//                    }
            }
            return mJsonResults;
        }

        @Override
        protected void onPostExecute(StringBuilder stringBuilder) {
            super.onPostExecute(stringBuilder);
            if (stringBuilder != null) {
                ArrayList<String>urls;
                urls= new ArrayList<>();
                String finalresult = stringBuilder.toString();
                Log.e("finalresult",""+finalresult);
                for(int j=0;j<latList.size();j++) {
                    urls.add(finalresult);
                }
                Log.e("urlsize",""+urls.size()+""+urls.get(5));
                try {
                    JSONArray arr = new JSONArray(urls);
                    for (int i = 0; i < arr.length(); ++i) {
                        Log.e("finalresults",""+arr.length());
                        JSONObject object = arr.getJSONObject(i);
                        JSONArray array = object.getJSONArray("routes");
                        JSONObject routes = array.getJSONObject(0);
                        JSONArray legs = routes.getJSONArray("legs");
                        JSONObject steps = legs.getJSONObject(0);
                        JSONObject distance = steps.getJSONObject("distance");
                        String parsedDistance = distance.getString("text");
                        JSONObject duration = steps.getJSONObject("duration");
                        String parsedduration = duration.getString("text");
                        Log.e("duration", parsedduration);
                        //complete_add.setText(parsedduration);
//                    TextView tv_destination = (TextView) findViewById(R.id.tv_destination);
//                    detail = parsedDistance + " " + parsedduration;
//                    tv_destination.setText(parsedDistance + " " + parsedduration);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
    }

    public boolean checkInternetConenction() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec
                =(ConnectivityManager)getActivity().getSystemService(getActivity().getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() ==
                android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            //Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;
        }else if (
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() ==
                                android.net.NetworkInfo.State.DISCONNECTED  ) {
            //Toast.makeText(this, " Not Connected ", Toast.LENGTH_LONG).show();
            myActivity.showOneErrorDialog("No Network Available", "Ok", getActivity());
            return false;
        }
        return false;
    }


    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null|| address.size()==0) {
                return null;
            }
            else {
                Log.e("address",""+address);
                Address location = address.get(0);
                Log.e("addresss",""+address.get(0));
                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }



}
