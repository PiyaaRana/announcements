package com.zoptal.announcements.activities;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zoptal.announcements.GoogleUtil.DataParser;
import com.zoptal.announcements.GoogleUtil.DownloadURL;
import com.zoptal.announcements.R;
import com.zoptal.announcements.adapter.PlacesLocationAdapter;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.modal.LocationModal;
import com.zoptal.announcements.modal.MyListModal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchAmenitiesAndCondolencesActivity extends AppCompatActivity {
    private PlacesLocationAdapter cadapter;
    private RecyclerView recyclerViewList;
    private List<MyListModal> myListModalList;
    private MyActivity myActivity;
    private ProgressBar progressBar;
    private RelativeLayout mainLayout;
    String search="stores+hotels+transport",searches="gifts",eventLocation;
    private int PROXIMITY_RADIUS = 10000;
    ArrayList<String>storeName,vicinities,result;
    ArrayList<Double>latList,longList;
    private double latitude=0.0,latitude_map;
    private double longitude=0.0,longitude_map;
    LatLng latLng;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_amenities_fragment);
       getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        myListModalList= new ArrayList<>();
        mainLayout= (RelativeLayout) findViewById(R.id.main_layout);
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.VISIBLE);     // To Hide ProgressBar
        recyclerViewList = (RecyclerView) findViewById(R.id.recycler_mylist);

        Intent intent= getIntent();
        if (intent != null) {
            latitude = intent.getDoubleExtra("lat", 0.0);
            longitude = intent.getDoubleExtra("lon", 0.0);
            //Log.e("eventssssss", ""+latitude+"  "+""+longitude );
            eventLocation= intent.getStringExtra("eventplace" );
            searches=intent.getStringExtra("searchtype");
            latLng=  getLocationFromAddress(this,eventLocation);
            latitude= latLng.latitude;
            longitude=latLng.longitude;

        }


        final Boolean abc1 = checkInternetConenction();
        if (abc1) {

            getNearbyBanks();
        }
        else {
            progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        }

    }

    public boolean checkInternetConenction() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec
                =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() ==
                android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            //Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;
        }else if (
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() ==
                                android.net.NetworkInfo.State.DISCONNECTED  ) {
            //Toast.makeText(this, " Not Connected ", Toast.LENGTH_LONG).show();
            myActivity.showOneErrorDialog("No Network Available", "Ok", this);
            return false;
        }
        return false;
    }


    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null|| address.size()==0) {
                return null;
            }
            else {
                Log.e("address",""+address);
                Address location = address.get(0);
                Log.e("addresss",""+address.get(0));
                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    private void getNearbyBanks(){
        String url = getUrl(latitude, longitude, search,searches);
        Object[] DataTransfer = new Object[2];
        DataTransfer[1] = url;
       GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData();
        getNearbyPlacesData.execute(DataTransfer);
        //Toast.makeText(getActivity(), "These are your Nearby Stores! ",
        // Toast.LENGTH_LONG).show();
    }

    private String getUrl(double latitude, double longitude, String nearbyPlace,String keyword) {
        StringBuilder googlePlacesUrl = new
                StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("type=" + nearbyPlace);
        googlePlacesUrl.append("&location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);

        googlePlacesUrl.append("&keyword=" + keyword);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&opennow=true");
        googlePlacesUrl.append("&key=" + "AIzaSyBLs1l4wEWnwCHNJ_TdiJ_TPAnDn_vQVS8");

        Log.e("googleplace",googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
    }

    public class GetNearbyPlacesData extends AsyncTask<Object, String, String> {

        String googlePlacesData;
        GoogleMap mMap;
        String url;
        ArrayList<LocationModal> myPlacesList;

        @Override
        protected String doInBackground(Object... params) {
            try {
                Log.d("GetNearbyStoresData", "doInBackground entered");

                url = (String) params[1];
                Log.e("googleurls",url);
                DownloadURL urlConnection = new DownloadURL();
                googlePlacesData = urlConnection.readUrl(url);
                Log.d("GooglePlacesReadTask", "doInBackground Exit");
            } catch (Exception e) {
                Log.d("GooglePlacesReadTask", e.toString());
            }
            return googlePlacesData;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("GooglePlacesReadTask", "onPostExecute Entered");
            Log.e("result",""+result);
            List<HashMap<String, String>> nearbyPlacesList = null;
            DataParser dataParser = new DataParser();
            nearbyPlacesList = dataParser.parse(result);
            if(nearbyPlacesList.size()>0|| nearbyPlacesList==null) {
                ShowNearbyPlaces(nearbyPlacesList);
            }
            else {
                progressBar.setVisibility(View.GONE);
                recyclerViewList.setVisibility(View.INVISIBLE);
                myActivity.showOneErrorDialog("No search results found.", "OK", SearchAmenitiesAndCondolencesActivity.this);
            }
            Log.d("GooglePlacesReadTask", "onPostExecute Exit");
        }

        private void ShowNearbyPlaces(List<HashMap<String, String>> nearbyPlacesList) {

            storeName = new ArrayList<>();
            vicinities = new ArrayList<>();
            latList= new ArrayList<>();
            longList= new ArrayList<>();
            result= new ArrayList<>();
            for (int i = 0; i < nearbyPlacesList.size(); i++) {

                Log.d("onPostExecute", "Entered into showing locations");
                MarkerOptions markerOptions = new MarkerOptions();
                HashMap<String, String> googlePlace = nearbyPlacesList.get(i);
                Log.e("googleplaces",""+googlePlace);
                double lat = Double.parseDouble(googlePlace.get("lat"));
                double lng = Double.parseDouble(googlePlace.get("lng"));
                String placeName = googlePlace.get("place_name");
                String vicinity = googlePlace.get("vicinity");
                storeName.add(placeName);
                vicinities.add(vicinity);
                latList.add(lat);
                longList.add(lng);

//                LatLng latLng = new LatLng(lat, lng);
//                Log.e("latlng",""+latLng);
//                markerOptions.position(latLng);
//                markerOptions.title(placeName + " : " + vicinity);
//                mMap.addMarker(markerOptions);
//                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
////move map camera
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//                mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
            }

            Log.e("lat_long_list","   "+latList+"  "+longList);

//for(int j=0;j<latList.size();j++) {
//   String duration= getUrls(latList.get(j),longList.get(j));
//    String results=getDuration(duration);
//    result.add(results);
//    Log.e("duration",""+results);
//}

            progressBar.setVisibility(View.GONE);


            cadapter =new PlacesLocationAdapter(getDataSet(),SearchAmenitiesAndCondolencesActivity.this);
            recyclerViewList.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchAmenitiesAndCondolencesActivity.this) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            recyclerViewList.setLayoutManager(mLayoutManager);
            //recyclerView
            // List.setNestedScrollingEnabled(false);
            recyclerViewList.setAdapter(cadapter);
//            if(latList.size()!=0) {
//
//                ApiDirectionsAsyncTask apiDirectionsAsyncTask = new ApiDirectionsAsyncTask();
//                apiDirectionsAsyncTask.execute();
//
//
//            }


        }
    }

    private ArrayList<LocationModal> getDataSet(){
        ArrayList results = new ArrayList<LocationModal>();
        for (int index = 0; index < storeName.size(); index++) {
            LocationModal obj = new LocationModal(storeName.get(index),vicinities.get(index)
            );
            results.add(index, obj);
        }
        return results;


    }

}
