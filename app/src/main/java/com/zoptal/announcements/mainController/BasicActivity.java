package com.zoptal.announcements.mainController;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.zoptal.announcements.R;
import com.zoptal.announcements.base.AppActivity;
import com.zoptal.announcements.fcm.MyFirebaseMessagingService;
import com.zoptal.announcements.fragments.CalendarFragment;
import com.zoptal.announcements.fragments.ContactsFragment;
import com.zoptal.announcements.fragments.CreateEventFragment;
import com.zoptal.announcements.fragments.HomeFragment;
import com.zoptal.announcements.fragments.InboxFragment;
import com.zoptal.announcements.fragments.MapFragment;
import com.zoptal.announcements.fragments.MyEventsFragment;
import com.zoptal.announcements.fragments.PastEventsFragment;
import com.zoptal.announcements.fragments.ProfileFragment;
import com.zoptal.announcements.fragments.SettingsFragment;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.TAGS;

public class BasicActivity extends AppActivity {
private LinearLayout layout_back,layout_menu,menuBack;
private Dialog menuDialog,logoutDialog;
private boolean doubleBackToExitPressedOnce;
public static final int RequestPermissionCode = 7;
    private String appToken="f6fa0fa5041479b76322145vgh1g410975af7766",tokenvalue,badgeCount;
    private RelativeLayout mainLayout;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_activity);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mThis=this;
        FirebaseApp.initializeApp(this);
        tokenvalue = FirebaseInstanceId.getInstance().getToken();
      //  Log.e("token",tokenvalue);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-event-name"));

        tokenvalue = FirebaseInstanceId.getInstance().getToken();


        //Log.e(TAGS, "token:" + tokenvalue);
        startService(
                new Intent(BasicActivity.this, MyFirebaseMessagingService.class).putExtra("badge", badgeCount)

        );
         findViews();
         replaceFragmentWithoutBackstack(new HomeFragment());
        AllowRuntimePermissions();
    }

    private void findViews(){
        layout_back=(LinearLayout)findViewById(R.id.ll_back);
        layout_menu=(LinearLayout)findViewById(R.id.ll_menu);
        menuBack=(LinearLayout)findViewById(R.id.menu_back);
        mainLayout=(RelativeLayout) findViewById(R.id.main_layout);
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar, params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        });
        layout_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogmenu();
            }
        });
        }

    private static BasicActivity mThis = null;

    public static BasicActivity getThis() {
        return mThis;
    }

    public void dialogmenu() {
        menuBack.setBackgroundColor(Color.parseColor("#f37261"));
        menuDialog = new Dialog(BasicActivity.this, android.R.style.Theme_Translucent) {
            @Override
            public boolean onTouchEvent(MotionEvent event) {
                // Tap anywhere to close dialog.
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                dismiss();
                //dialog.dismiss();
                return true;
            }
        };
        menuDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(event.getAction()==KeyEvent.ACTION_DOWN){
                    if(keyCode==KeyEvent.KEYCODE_BACK){
                        menuBack.setBackgroundColor(Color.parseColor("#000000"));
                    }
                }
                return false;
            }
        });
        menuDialog.setCanceledOnTouchOutside(true);
        menuDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        menuDialog.setCancelable(true);
        menuDialog.setContentView(R.layout.dialog_menu);
        TextView tv_home = (TextView) menuDialog.findViewById(R.id.tv_home);
        TextView tv_inbox = (TextView) menuDialog.findViewById(R.id.tv_inbox);
        TextView tv_calendar = (TextView) menuDialog.findViewById(R.id.tv_calendar);
        TextView tv_pastEvents = (TextView) menuDialog.findViewById(R.id.tv_pastevent);
        TextView tv_logout = (TextView) menuDialog.findViewById(R.id.tv_logout);
        TextView tv_contacts = (TextView) menuDialog.findViewById(R.id.tv_contacts);
        TextView tv_profile = (TextView) menuDialog.findViewById(R.id.tv_profile);
        TextView tv_settings = (TextView) menuDialog.findViewById(R.id.tv_settings);
        TextView tv_myEvents = (TextView) menuDialog.findViewById(R.id.tv_my_event);
        tv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                menuDialog.dismiss();
                replaceFragment(new HomeFragment());
            }
        });
        tv_inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                menuDialog.dismiss();
                replaceFragment(new InboxFragment());
            }
        });
        tv_calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                menuDialog.dismiss();
                replaceFragment(new CalendarFragment());
            }
        });
        tv_pastEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                menuDialog.dismiss();
                replaceFragment(new PastEventsFragment());
            }
        });
        tv_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                menuDialog.dismiss();
                ContactsFragment fragment6 = new ContactsFragment();
                Bundle bundle6 = new Bundle();
                bundle6.putString("inviteCheck", "false"); //key and value
                fragment6.setArguments(bundle6);
                replaceFragment(fragment6);
            }
        });
        tv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                menuDialog.dismiss();
                ProfileFragment fragment7 = new ProfileFragment();
                Bundle bundle7 = new Bundle();
                bundle7.putString("myprofile", "true"); //key and value
                bundle7.putString("otheruserid", "none");
                fragment7.setArguments(bundle7);
                replaceFragment(fragment7);

            }
        });
        tv_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                menuDialog.dismiss();
                replaceFragment(new SettingsFragment());
            }
        });
        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                menuDialog.dismiss();
                displayDialog();
            }
        });

        tv_myEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuBack.setBackgroundColor(Color.parseColor("#000000"));
                menuDialog.dismiss();
                replaceFragment(new MyEventsFragment());

            }
        });
        menuDialog.show();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String messageBadge = intent.getStringExtra("badge");
            Log.e("badgess", "" + messageBadge);
        }
    };
    public void displayDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure you want to log out?");
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        menuDialog.dismiss();
                        LogoutAPI();
                    }
                });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //dialog.dismiss();
                        menuDialog.dismiss();

                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @SuppressWarnings("ResourceType")
    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment topFragment = fragmentManager.findFragmentById(R.id.content_frame);
        if (topFragment != null) {
            fragmentTransaction.remove(topFragment);
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.addToBackStack(backStateName);
            fragmentTransaction.commitAllowingStateLoss();
        } else {
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.addToBackStack(backStateName);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    public void replaceFragmentWithoutBackstack(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment topFragment = fragmentManager.findFragmentById(R.id.content_frame);
        if (topFragment != null) {
            fragmentTransaction.remove(topFragment);
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.commitAllowingStateLoss();
        } else {
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("ParentMainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("ParentMainActivity", "nothing on backstack, calling super");
            // super.onBackPressed();
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    public void AllowRuntimePermissions() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (CheckingPermissionIsEnabledOrNot()) {

            }

            // If, If permission is not enabled then else condition will execute.
            else {
                //Calling method to enable permission.
                RequestMultiplePermission();
            }
        }

    }

    private void RequestMultiplePermission() {
        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(BasicActivity.this, new String[]
                {
                        CAMERA,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE,
                        READ_CONTACTS
                }, RequestPermissionCode);
    }

    // Calling override method.
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {

                    boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadExternalStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStoragePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadContactsPermission = grantResults[3] == PackageManager.PERMISSION_GRANTED;

                    if (CameraPermission && ReadExternalStoragePermission && WriteExternalStoragePermission && ReadContactsPermission) {

                        //Toast.makeText(BasicActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        //Toast.makeText(BasicActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    // Checking permission is enabled or not using function starts from here.
    public boolean CheckingPermissionIsEnabledOrNot() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int FourthPermissionResult= ContextCompat.checkSelfPermission(getApplicationContext(), READ_CONTACTS);
        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                FourthPermissionResult == PackageManager.PERMISSION_GRANTED;
    }
    private void LogoutAPI() {
          progressBar.setVisibility(View.VISIBLE);
        String refreshedToken = SharedPrefsUtils.getStringPreference(BasicActivity.this, "access_token");

        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        String access_token = SharedPrefsUtils.getStringPreference(this, "access_token");
        // prepare call in Retrofit 2.0
        try {
       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
            Call<LoginResponse> userCall = apiInterface.logout(appToken,access_token);
            userCall.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                   progressBar.setVisibility(View.GONE);
                    //Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        Toast.makeText(getApplicationContext(), "An error occurred, Please try again", Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().isResult()) {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        SharedPrefsUtils.setBooleanPreference(BasicActivity.this, "logged_in", false);
                        SharedPrefsUtils.setStringPreference(BasicActivity.this, "user_id", "");
                        SharedPrefsUtils.setStringPreference(BasicActivity.this, "username", "");
                        SharedPrefsUtils.setStringPreference(BasicActivity.this, "email", "");
                        SharedPrefsUtils.setStringPreference(BasicActivity.this, "phone","");
                        SharedPrefsUtils.setStringPreference(BasicActivity.this, "access_token", "");
                        startActivityWithFinish(LoginSignUpActivity.class);
                        }
                    else if (response.body().getMessage().equalsIgnoreCase("Access Denied.")) {
                        SharedPrefsUtils.setBooleanPreference(BasicActivity.this, "logged_in", false);
                        Intent intent = new Intent(BasicActivity.this, LoginSignUpActivity.class);
                        startActivity(intent);
                        finish();
                        return;
                    }
                     else {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //showAlert(response.body().getMessage());
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                 progressBar.setVisibility(View.GONE);
                    // Utils.disableEnableControls(true, mainLayout);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
