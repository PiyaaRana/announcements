package com.zoptal.announcements.mainController;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zoptal.announcements.R;
import com.zoptal.announcements.base.AppActivity;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgotPasswordActivity extends AppActivity {
    private EditText edt_email;
    private RelativeLayout mainLayout;
    private TextView txtview_submit,txtview_cancel;
    private String appToken="f6fa0fa5041479b76322145vgh1g410975af7766";
    private ProgressBar progressBar;
    final String emailRegex = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,40}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{1,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{1,4}" +
            ")+";
    private MyActivity myActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);
        findViews();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    private void findViews(){
        mainLayout=(RelativeLayout)findViewById(R.id.main_layout);
        mainLayout.setBackgroundResource(R.mipmap.bg);
        edt_email=(EditText)findViewById(R.id.edt_email);
        txtview_submit=(TextView)findViewById(R.id.tv_submit);
        txtview_cancel=(TextView)findViewById(R.id.tv_cancel);
        progressBar = new ProgressBar(this,null,android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar,params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar
        txtview_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             ForgotPasswordAPI();
            }
        });
        txtview_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityWithFinish(LoginActivity.class);
            }
        });

    }
    private void ForgotPasswordAPI() {
        if (edt_email.getText().length() == 0) {
            myActivity.showOneErrorDialog("Email address should not be empty.", "OK", ForgotPasswordActivity.this);
            edt_email.setFocusable(true);
            return;
        }

        if (!(edt_email.getText().toString().trim().matches(emailRegex))) {
            myActivity.showOneErrorDialog("Enter a valid email address.", "OK", ForgotPasswordActivity.this);
            edt_email.setFocusable(true);
            return;
        } else {
            progressBar.setVisibility(View.VISIBLE);
            // Utils.disableEnableControls(false, mainLayout);
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .addInterceptor(RestClient.getClient())
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(WebApiUrls.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                    .build();

            RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
            // prepare call in Retrofit 2.0
            try {
       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/
                Call<LoginResponse> userCall = apiInterface.forgot_password(appToken, edt_email.getText().toString().trim());
                userCall.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        progressBar.setVisibility(View.GONE);
                        //Utils.disableEnableControls(true, mainLayout);
                        if (response.body() == null) {
                            Toast.makeText(getApplicationContext(), "An error occurred, Please try again", Toast.LENGTH_SHORT).show();
                        }
                        if (response.body().isResult()) {
                            SharedPrefsUtils.setStringPreference(ForgotPasswordActivity.this, "password", "");
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            //showAlert(response.body().getMessage());
                        }

                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        // Utils.disableEnableControls(true, mainLayout);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}