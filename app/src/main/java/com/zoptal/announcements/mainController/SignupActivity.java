package com.zoptal.announcements.mainController;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.zoptal.announcements.R;
import com.zoptal.announcements.base.AppActivity;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.responseClasses.RegisterResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.os.Build.TAGS;

public class SignupActivity extends AppActivity {
    private EditText edt_username,edt_password,edt_contactno,edt_userEmail,edt_cnfrmPassword;
    private RelativeLayout mainLayout;
    private TextView txtview_signup,txtview_cancel;
    private String appToken="f6fa0fa5041479b76322145vgh1g410975af7766",tokenvalue;
    private ProgressBar progressBar;
    private static final String USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$";
    private MyActivity myActivity;
    final String emailRegex = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,40}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{1,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{1,4}" +
            ")+";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);
        tokenvalue = FirebaseInstanceId.getInstance().getToken();

        Log.e(TAGS, "token:" + tokenvalue);
        findViews();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    private void findViews(){
        mainLayout=(RelativeLayout)findViewById(R.id.main_layout);
        mainLayout.setBackgroundResource(R.mipmap.bg);
        edt_username=(EditText)findViewById(R.id.edt_username);
        edt_password=(EditText)findViewById(R.id.edt_password);
        edt_contactno=(EditText)findViewById(R.id.edt_contact);
        edt_userEmail=(EditText)findViewById(R.id.edt_email);
        txtview_signup=(TextView)findViewById(R.id.tv_signup);
        txtview_cancel=(TextView)findViewById(R.id.tv_cancel);
        edt_cnfrmPassword=(EditText) findViewById(R.id.edt_cnfrm_password);

        progressBar = new ProgressBar(this,null,android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar,params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar

        txtview_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkValidation();

            }
        });
        txtview_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityWithFinish(LoginSignUpActivity.class);
            }
        });

    }
    private void signUpAPI() {
        progressBar.setVisibility(View.VISIBLE);
       // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0
        try {
       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/

            Call<RegisterResponse> userCall = apiInterface.register(appToken,edt_username.getText().toString().trim(),edt_userEmail.getText().toString().trim(),edt_contactno.getText().toString().trim(), edt_password.getText().toString().trim(), "android", tokenvalue);
            userCall.enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    //Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        Toast.makeText(getApplicationContext(), "An error occurred, Please try again", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body().isResult()) {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        SharedPrefsUtils.setBooleanPreference(SignupActivity.this, "logged_in", true);
                        SharedPrefsUtils.setStringPreference(SignupActivity.this, "user_id", response.body().getRegisterDataModal().getUser_id() + "");
                        SharedPrefsUtils.setStringPreference(SignupActivity.this, "username", response.body().getRegisterDataModal().getUsername());
                        SharedPrefsUtils.setStringPreference(SignupActivity.this, "email", response.body().getRegisterDataModal().getEmail());
                        SharedPrefsUtils.setStringPreference(SignupActivity.this, "phone",response.body().getRegisterDataModal().getPhone_no());
                        SharedPrefsUtils.setStringPreference(SignupActivity.this, "access_token", response.body().getRegisterDataModal().getAccess_token());
                        startActivityWithFinish(BasicActivity.class);
                    } else {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<RegisterResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    // Utils.disableEnableControls(true, mainLayout);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkValidation() {
        if (edt_username.getText().length() == 0) {
            myActivity.showOneErrorDialog("Username should not be empty.", "OK", SignupActivity.this);
            edt_username.setFocusable(true);
            return;
        }

        if (!(edt_username.getText().toString().trim().matches(USERNAME_PATTERN))) {
            myActivity.showOneErrorDialog("Username should contain lower-case alphabets,special characters,numbers only.", "OK", SignupActivity.this);
            edt_username.setFocusable(true);
            return;
        }
        if (edt_password.getText().length() < 6) {
            myActivity.showOneErrorDialog("Password should be minimum 6 characters long.", "OK", SignupActivity.this);
            edt_password.setFocusable(true);
            return;
        }
        if (edt_cnfrmPassword.getText().length() < 6) {
            myActivity.showOneErrorDialog("Confirm password should be minimum 6 characters long.", "OK", SignupActivity.this);
            edt_password.setFocusable(true);
            return;
        }
        if(!(edt_cnfrmPassword.getText().toString().trim().equalsIgnoreCase(edt_password.getText().toString().trim()))){
            myActivity.showOneErrorDialog("Password doesn't match.", "OK", SignupActivity.this);
            return;
        }

        if (edt_contactno.getText().length() == 0) {
            myActivity.showOneErrorDialog("Mobile number should not be empty.", "OK", SignupActivity.this);
            edt_contactno.setFocusable(true);
            return;
        }

//        if (edt_contactno.getText().length()!= 10) {
//            myActivity.showOneErrorDialog("Enter a valid mobile number.", "OK", SignupActivity.this);
//            edt_contactno.setFocusable(true);
//            return;
//        }
        if (edt_userEmail.getText().length() == 0) {
            myActivity.showOneErrorDialog("Email address should not be empty.", "OK", SignupActivity.this);
            edt_userEmail.setFocusable(true);
            return;
        }

        if (!(edt_userEmail.getText().toString().trim().matches(emailRegex))) {
            myActivity.showOneErrorDialog("Enter a valid email address.", "OK", SignupActivity.this);
            edt_userEmail.setFocusable(true);
            return;
        }
        else {
            signUpAPI();
        }

    }

}