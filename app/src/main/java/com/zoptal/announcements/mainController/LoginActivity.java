package com.zoptal.announcements.mainController;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.zoptal.announcements.R;
import com.zoptal.announcements.base.AppActivity;
import com.zoptal.announcements.base.MyActivity;
import com.zoptal.announcements.responseClasses.LoginResponse;
import com.zoptal.announcements.retrofit.RetrofitAPIInnterface;
import com.zoptal.announcements.utils.RestClient;
import com.zoptal.announcements.utils.SharedPrefsUtils;
import com.zoptal.announcements.utils.WebApiUrls;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.os.Build.TAGS;

public class LoginActivity extends AppActivity {
    private EditText edt_username,edt_password;
    private RelativeLayout mainLayout;
    private TextView txtview_signin,txtview_cancel,txtview_forgotpswd;
    private MyActivity myActivity;
    private static final String USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$";
    private ProgressBar progressBar;
    private String appToken="f6fa0fa5041479b76322145vgh1g410975af7766",tokenvalue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        tokenvalue = FirebaseInstanceId.getInstance().getToken();

        Log.e(TAGS, "token:" + tokenvalue);

        findViews();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void findViews(){
        mainLayout=(RelativeLayout)findViewById(R.id.main_layout);
        mainLayout.setBackgroundResource(R.mipmap.bg);
        edt_username=(EditText)findViewById(R.id.edt_username);
        edt_password=(EditText)findViewById(R.id.edt_password);
        txtview_signin=(TextView)findViewById(R.id.tv_signin);
        txtview_cancel=(TextView)findViewById(R.id.tv_cancel);
        txtview_forgotpswd =(TextView)findViewById(R.id.tv_forgotpswd);

        progressBar = new ProgressBar(this,null,android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.addView(progressBar,params);
        progressBar.setVisibility(View.GONE);     // To Hide ProgressBar

        txtview_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             checkValidation();
            }
        });
        txtview_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            startActivityWithFinish(LoginSignUpActivity.class);
            }
        });
        txtview_forgotpswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(ForgotPasswordActivity.class);
            }
        });

    }

    private void checkValidation() {
        if (edt_username.getText().toString().trim().length() == 0) {
            myActivity.showOneErrorDialog("Username should not be empty.", "OK", LoginActivity.this);
            edt_username.setFocusable(true);
            return;
        }

        if (!(edt_username.getText().toString().trim().matches(USERNAME_PATTERN))) {
            myActivity.showOneErrorDialog("Username should contain lower-case alphabets,special characters,numbers only.", "OK", LoginActivity.this);
            edt_username.setFocusable(true);
            return;
        }
        if (edt_password.getText().toString().trim().length() < 6) {
            myActivity.showOneErrorDialog("Password should be minimum 6 characters long.", "OK", LoginActivity.this);
            edt_password.setFocusable(true);
            return;
        }
        else {
            signInAPI();
        }

    }
    private void signInAPI() {
        progressBar.setVisibility(View.VISIBLE);
        // Utils.disableEnableControls(false, mainLayout);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(RestClient.getClient())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApiUrls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();

        RetrofitAPIInnterface apiInterface = retrofit.create(RetrofitAPIInnterface.class);
        // prepare call in Retrofit 2.0
        try {
       /*     String android_id = Settings.Secure.getString(SignupActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/

            Call<LoginResponse> userCall = apiInterface.login(appToken,edt_username.getText().toString().trim(),edt_password.getText().toString().trim(), "android", tokenvalue);
            userCall.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    //Utils.disableEnableControls(true, mainLayout);
                    if (response.body() == null) {
                        Toast.makeText(getApplicationContext(), "An error occurred, Please try again", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response.body().isResult()) {
                        SharedPrefsUtils.setBooleanPreference(LoginActivity.this, "logged_in", true);
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        SharedPrefsUtils.setStringPreference(LoginActivity.this, "user_id", response.body().getRegisterDataModal().getUser_id() + "");
                        SharedPrefsUtils.setStringPreference(LoginActivity.this, "username", response.body().getRegisterDataModal().getUsername());
                        SharedPrefsUtils.setStringPreference(LoginActivity.this, "email", response.body().getRegisterDataModal().getEmail());
                        SharedPrefsUtils.setStringPreference(LoginActivity.this, "phone",response.body().getRegisterDataModal().getPhone_no());
                        SharedPrefsUtils.setStringPreference(LoginActivity.this, "access_token", response.body().getRegisterDataModal().getAccess_token());

                        startActivityWithFinish(BasicActivity.class);
                    } else {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //showAlert(response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    // Utils.disableEnableControls(true, mainLayout);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
