package com.zoptal.announcements.mainController;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoptal.announcements.R;
import com.zoptal.announcements.base.AppActivity;
import com.zoptal.announcements.utils.SharedPrefsUtils;

public class LoginSignUpActivity extends AppActivity{

    private TextView txtview_signin,txtview_signup;
    private RelativeLayout mainLayout;
    private Boolean loginStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_signup);
        loginStatus = SharedPrefsUtils.getBooleanPreference(LoginSignUpActivity.this, "logged_in", false);
        if(loginStatus){
            startActivityWithFinish(BasicActivity.class);
        }
        else {

        }

        mainLayout=(RelativeLayout)findViewById(R.id.main_layout);
        mainLayout.setBackgroundResource(R.mipmap.bg);
        txtview_signin=(TextView)findViewById(R.id.tv_signin);
        txtview_signup=(TextView)findViewById(R.id.tv_signup);

        txtview_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             startActivity(LoginActivity.class);
            }
        });

        txtview_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(SignupActivity.class);
            }
        });

    }
}
